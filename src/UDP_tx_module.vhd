----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		11:01:48 08/13/2010 
--
-- Module Name:    UDP_tx_module - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--
--	The module will read the RAM and build the outgoing UDP-data packet.
--
--	The data is then organised in RAM as shown:
--
--        RAM-@ | content (63 downto 0) in bytes: -- == don't care (N/A)|
--        ------|------'------'------'------'------'------'------'------|
--         00   |  --  '  --  '  --  '  --  '  --  '  --  '  --  '  --  |
--         01   |  "D" '  " " ' 6 by_'__tes_'_DST__'_MAC__'_addr_'_ess  |
--         02   |  "S" '  " " ' 6 by_'__tes_'_SRC__'_MAC__'_addr_'_ess  |
--         03   |  UDP_'_len  '  --  '  --  '  --  '  --  '  Ty__'__pe  |
--         04   |  IHL '  TOS 'Total_'_Len  '  Id_ ' _ent ' Frag_'_ment |
--         05   |  --  '  --  '  --  '  --  '  TTL ' Prot '  CRC_'_CRC  |
--         06   |  IP-_'_SRC__'_addr_'_ess  '  IP-_'_DST__'_addr_'_ess  |
--         07   | UDP-_'_head_'_er:__'_see__'_UDP__'_docu_'_ment_'_ation|
--         -----|------'------'------'------'------'------'------'------|
--         08   | data_'_data_'_data_'_data_'_data_'_data_'_data_'_data |
--         ..   |  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  |
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UDP_tx_module is
	port (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		tx_data_out			: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n		: in  std_logic; -- Input destination ready	-- check

--	Sending counter for outer module
		tx_counter			: out std_logic_vector(10 downto 0);

--	external stimuli
		send_frame_flag		: in  std_logic;	-- start frame signal

--	RAM read requests from UDP-module:
		r_flag_enet			: out std_logic;
		r_addr_enet			: out std_logic_vector(7 downto 0);
		r_fine_enet			: in  std_logic;
		r_data				: in  std_logic_vector(63 downto 0)	-- out read data
	);
end UDP_tx_module;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of UDP_tx_module is

--	constants for sending process
	constant tx_delay			: integer := 2;

--	counter for sending process
	signal tx_count			: integer range 0 to max_frame_length + tx_delay + 1 := 0;
	signal tx_frame_length	: integer range 0 to max_frame_length := max_frame_length;

	signal tx_enable		: std_logic;

	type sr9by8 is array (0 to 8) of std_logic_vector(7 downto 0);
	signal tx_data_sr_content	: sr9by8;	-- holds contents of tx_data in shift register

--  CRC calculation of the output packet:
--	CRC base value:  Version & TOS + no fragmentation + TTL & protocol
--                        x"45_00" +    x"40_00"      + x"40_11"
	constant IP_CRC_init_value	: std_logic_vector := x"c5_11";

--	shift register to hold full RAM slice by 16 bits for CRC
	type sr4by16 is array (0 to 3) of std_logic_vector(15 downto 0);
	signal CRC_sr_content	: sr4by16;

	signal IP_total_length	: unsigned(15 downto 0) := x"00_1c"; -- could be kept in sr
--  running not(CRC) being calculated
	signal IP_CRC_n			: std_logic_vector(15 downto 0) := not IP_CRC_init_value; -- to be kept in sr

	signal adder_in_a	: std_logic_VECTOR(15 downto 0) := x"00_00";
	signal adder_in_b	: std_logic_VECTOR(15 downto 0) := x"00_00";
	signal adder_in_c	: std_logic := '0';
	signal adder_out_c	: std_logic;
	signal adder_out_s	: std_logic_VECTOR(15 downto 0);
	signal carry		: std_logic := '0';

	component FullAdder_16bit is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         16-bit fulladder: selfexplanatory                                  --
--                                                                            --
--------------------------------------------------------------------------------
	port(
		a_in	: in std_logic_vector (15 downto 0);
		b_in	: in std_logic_vector (15 downto 0);
		c_in	: in std_logic;
		s_out	: out std_logic_vector (15 downto 0);
		c_out	: out std_logic
	);
	end component;

begin

	tx_counter <= std_logic_vector(to_unsigned(tx_count,11));

--	set control bits accordingly
	tx_enable <= not(tx_dst_rdy_in_n);


------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process data_out_reg_p                                            --
--                                                                            --
--   continuously data stream from shift register to output when transmitting --
--                                                                            --
--------------------------------------------------------------------------------
	data_out_reg_p : process(clk)
	begin
		if rising_edge(clk) then
			if tx_enable = '1' then
				tx_data_out <= tx_data_sr_content(8);-- after dly;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_tx_sof_from_tx_count                               --
--                                                                            --
--   set start_of_frame of transmitting properly                              --
--   tx_delay is required due to shift register implementation causing delay  --
--                                                                            --
--------------------------------------------------------------------------------
	manage_tx_sof_from_tx_count : process(clk)
	begin
		if rising_edge(clk) then
			if tx_enable = '1' then
				if tx_count = 1 + tx_delay then
					tx_sof_out_n <= '0';
				else
					tx_sof_out_n <= '1';
				end if;
			end if;
		end if;          
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_tx_eof_from_tx_count                               --
--                                                                            --
--   set end_of_frame of transmitting properly                                --
--   tx_delay is required due to shift register implementation causing delay  --
--                                                                            --
--------------------------------------------------------------------------------
	manage_tx_eof_from_tx_count : process(clk)
	begin
		if rising_edge(clk) then
	--		if tx_enable = '1' then
	--	max frame length = 1522 bytes (no jumbo frames)
				if (tx_count = tx_frame_length + tx_delay) then
					tx_eof_out_n <= '0';
				else
					tx_eof_out_n <= '1';
				end if;
	--		end if;
		end if;          
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_tx_src_rdy_out_from_tx_count                       --
--                                                                            --
--   set tx_src_rdy_out_n of transmitting properly                            --
--   tx_delay is required due to shift register implementation causing delay  --
--                                                                            --
--------------------------------------------------------------------------------
	manage_tx_src_rdy_out_from_tx_count : process(clk)
	begin
		if rising_edge(clk) then
			if tx_enable = '1' then
				if (0 + tx_delay < tx_count) and (tx_count <= tx_frame_length + tx_delay) then
					tx_src_rdy_out_n <= '0';
				else
					tx_src_rdy_out_n <= '1';
				end if;
			end if;
		end if;          
	end process;
		
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_tx_count_from_send_frame_flag                      --
--                                                                            --
--   data output is started on send_frame_flag                                --
--   if started once, the counter is increased up to tx_frame_length          --
--       (tx_frame_length is set by UDP-bytes in data output process          --
--                                                                            --
--------------------------------------------------------------------------------
	manage_tx_count_from_send_frame_flag : process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				tx_count <= 0;
			elsif tx_enable = '1' then
				if (send_frame_flag = '1') then
					tx_count <= 1;
				else
					if (0 < tx_count) and (tx_count < tx_frame_length + tx_delay) then
						tx_count <= tx_count + 1;
					else
						tx_count <= 0;
					end if;
				end if;
			else
				tx_count <= 0;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          CRC management starts here:                                       --
--                                                                            --
--   based on tx_count the CRC is simultaneously calculated with data read    --
--   from RAM by passing it into the decicated shift register                 --
--   at tx_count = 23 the not(IP_CRC_n) is written into output stream         --
--                                                                            --
--   adder_in_a is running not(crc) (accumulator-like)                        --
--   adder_in_b is being taken from dedicated shift register                  --
--   adder_in_c is running carry bit                                          --
--                                                                            --
--------------------------------------------------------------------------------
	crc_calculator: FullAdder_16bit
	port map (
		a_in	=> adder_in_a,
		b_in	=> adder_in_b,
		c_in	=> adder_in_c,
		s_out	=> adder_out_s,
		c_out	=> adder_out_c
	);

	with tx_count select
		adder_in_a <=
			x"00_00" when 0,
			IP_CRC_init_value when 1,
			IP_CRC_n when others;

	with tx_count select
		adder_in_c <=
			'0' when 1,
			carry when others;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_IP_CRC_from_tx_count                               --
--                                                                            --
--   adder_in_b is being taken from dedicated shift register                  --
--   it is being initialised based on tx_enable                               --
--                                                                            --
--------------------------------------------------------------------------------
	manage_IP_CRC_from_tx_count : process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				IP_CRC_n <= (others => '0');
				adder_in_b <= (others => '0');
				carry <= '0';
			elsif tx_enable = '1' then
				carry <= adder_out_c;
				IP_CRC_n <= adder_out_s;

				adder_in_b <= crc_sr_content(3);
			else
				carry <= '0';
				adder_in_b <= (others => '0');
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process create_output_stream                                      --
--                                                                            --
--   main process dealing data output                                         --
--   data are taken from RAM or assigned statically to shift register         --
--   if needed for calcluation, data are assigned to CRC-shift register, too  --
--                                                                            --
--   reading from RAM is pipelined so flag and data assigned with 2 clk shift --
--                                                                            --
--------------------------------------------------------------------------------
	create_output_stream: process (clk) is
		variable UDP_bytes	: unsigned(15 downto 0) := x"00_00";
		variable ram_addr	: unsigned(7 downto 0) := x"00";
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				UDP_bytes := x"00_00";
				ram_addr := x"01";
				r_addr_enet <= x"01";
				r_flag_enet <= '0';
				
				crc_sr_content <= (others => (others => '0'));
			elsif tx_enable = '1' then
				-- keep up shifting data register
				tx_data_sr_content <= x"00" & tx_data_sr_content (0 to 7);
				-- keep up shifting CRC register
				crc_sr_content <= x"00_00" & crc_sr_content(0 to 2);
				
				case tx_count is
--  destination MAC:
--	if you want to read this from RAM, too, then you'd have to change quite the
--	whole thing as ram-@ settings have to be modified in the view of delaying
--	tx_count 2 steps
--
--	example code:
--
--					when 1 =>
--						r_addr_enet <= x"01";
--						r_flag_enet <= '1';
--					when 3 =>	(which is now when 1 =>)
--						r_flag_enet <= '1';
--						tx_data_sr_content(7) <= r_data(47 downto 40);
--						tx_data_sr_content(6) <= r_data(39 downto 32);
--						tx_data_sr_content(5) <= r_data(31 downto 24);
--						tx_data_sr_content(4) <= r_data(23 downto 16);
--						tx_data_sr_content(3) <= r_data(15 downto  8);
--						tx_data_sr_content(2) <= r_data( 7 downto  0);
--
--	you'll have to apply this delay in all when statements
--	
					when 1 =>
						-- needs to be assigned in each case accordingly!
						r_flag_enet <= '0';

						tx_data_sr_content(7) <= x"00";
						tx_data_sr_content(6) <= x"26";
						tx_data_sr_content(5) <= x"18";
						tx_data_sr_content(4) <= x"f0";
						tx_data_sr_content(3) <= x"55";
						tx_data_sr_content(2) <= x"e0";
-- get IP-SRC and IP-DST addresses from RAM for IP_CRC calculation
					when 2 =>	-- select RAM-@ first
						r_addr_enet <= x"06";
						r_flag_enet <= '1';
					when 4 =>	-- take data from RAM
						r_flag_enet <= '1';

						crc_sr_content(3) <= r_data(63 downto 48); -- dst-IP-@ I & II
						crc_sr_content(2) <= r_data(47 downto 32); -- dst-IP-@ III & IV
						crc_sr_content(1) <= r_data(31 downto 16); -- src-IP-@ I & II
						crc_sr_content(0) <= r_data(15 downto  0); -- src-IP-@ III & IV
-- send source MAC from RAM
					when 5 =>	-- select RAM-@ first
						-- former DST @ is now SRC - change to x"02" to reswap
						r_addr_enet <= x"01";
						r_flag_enet <= '1';
					when 7 =>	-- take data from RAM
						r_flag_enet <= '1';

						tx_data_sr_content(7) <= r_data(47 downto 40);
						tx_data_sr_content(6) <= r_data(39 downto 32);
						tx_data_sr_content(5) <= r_data(31 downto 24);
						tx_data_sr_content(4) <= r_data(23 downto 16);
						tx_data_sr_content(3) <= r_data(15 downto  8);
						tx_data_sr_content(2) <= r_data( 7 downto  0);
-- send TYPE field, get UDP-length from RAM
					when 11 =>	-- select RAM-@ first
						r_addr_enet <= x"03";
						r_flag_enet <= '1';
					when 13 =>	-- take data from RAM
						r_flag_enet <= '1';

						UDP_bytes := unsigned(r_data(63 downto 48));
						IP_total_length <= UDP_bytes + x"00_14";-- 20 IP Offset
						tx_frame_length <= to_integer(x"22" + UDP_bytes);-- 14 Ethernet Offset + 20 IP Offset
					
						tx_data_sr_content(7) <= x"08";	-- IP Version 4 (IPv4)
						tx_data_sr_content(6) <= x"00";	-- IP Version 4 (IPv4)

						r_addr_enet <= x"04"; -- for next reading cycle at 15

						crc_sr_content(0) <= std_logic_vector(UDP_bytes + x"00_14");-- 20 IP Offset
-- send IP header I/III
					when 15 =>
						r_flag_enet <= '1';

						tx_data_sr_content(7) <= x"45";	-- IP: Version = 4, I(P)H(eader)L(ength) = 5 (*32 Bit = 20 Byte)
						tx_data_sr_content(6) <= x"00";	-- T(ype) O(f) S(ervice) - no need
						tx_data_sr_content(5) <= std_logic_vector(IP_total_length(15 downto 8));	-- upper Byte Total Length: incl. header, in bytes
						tx_data_sr_content(4) <= std_logic_vector(IP_total_length( 7 downto 0));	-- lower Byte Total Length
						tx_data_sr_content(3) <= r_data(31 downto 24);--x"12";--IP_identification(15 downto 8);	-- upper Byte Identification: explicit ID of datagramm
						tx_data_sr_content(2) <= r_data(23 downto 16);--x"34";--IP_identification(7 downto 0);	-- lower Byte Identification
						tx_data_sr_content(1) <= x"40";	-- upper Byte fragmentation handling (better don't change)
						tx_data_sr_content(0) <= x"00";	-- lower Byte fragmentation handling

						crc_sr_content(1) <= r_data(31 downto 16);
-- IP header part II/III
					when 23 =>
						r_flag_enet <= '0';

						tx_data_sr_content(7) <= x"40";	-- TTL = 64
						tx_data_sr_content(6) <= x"11";	-- Protokoll: x06 = TCP, x11 = UDP
						tx_data_sr_content(5) <= not IP_CRC_n(15 downto 8);	-- upper byte header Checksum
						tx_data_sr_content(4) <= not IP_CRC_n(7 downto 0);	-- lower byte header Checksum
-- IP header part III/III
					when 25 =>	-- select RAM-@ first
						r_addr_enet <= x"06";
						r_flag_enet <= '1';

					when 27 =>	-- take data from RAM
						r_flag_enet <= '1';

-- IP addresses: depending the order in the RAM, they might need to be reswapped
						tx_data_sr_content(3) <= r_data(63 downto 56);	-- 1st byte SRC-IP-@
						tx_data_sr_content(2) <= r_data(55 downto 48);	-- 2nd byte SRC-IP-@
						tx_data_sr_content(1) <= r_data(47 downto 40);	-- 3rd byte SRC-IP-@
						tx_data_sr_content(0) <= r_data(39 downto 32);	-- 4th byte SRC-IP-@

						tx_data_sr_content(7) <= r_data(31 downto 24);	-- 1st byte DST-IP-@
						tx_data_sr_content(6) <= r_data(23 downto 16);	-- 2nd byte DST-IP-@
						tx_data_sr_content(5) <= r_data(15 downto  8);	-- 3rd byte DST-IP-@
						tx_data_sr_content(4) <= r_data( 7 downto  0);	-- 4th byte DST-IP-@

-- UDP header part I/I
					when 35 =>
						r_flag_enet <= '0';

						tx_data_sr_content(7) <= x"AB";	-- upper byte scr-port
						tx_data_sr_content(6) <= x"CD";	-- lower byte scr-port
						tx_data_sr_content(5) <= x"EF";	-- upper byte dst-port
						tx_data_sr_content(4) <= x"01";	-- lower byte dst-port
--	UDP LENGTH TO BE CALCULATED
						tx_data_sr_content(3) <= std_logic_vector(UDP_bytes(15 downto 8));	-- upper byte lengthfield in bytes
						tx_data_sr_content(2) <= std_logic_vector(UDP_bytes( 7 downto 0));	-- lower byte lengthfield in bytes
						tx_data_sr_content(1) <= x"00";	-- upper byte checksum, can be left 0
						tx_data_sr_content(0) <= x"00";	-- lower byte checksum
						ram_addr := ram_data_start;
					when others =>
--	finally the UPD data
						if tx_count >= 40 then
							case tx_count mod 8 is
								when 1 =>	-- select RAM-@ first
								 -- must select ram-@ 2 clk-cycles before taking data from ram
									r_flag_enet <= '1';	-- for muxing ram requests correctly
									r_addr_enet <= std_logic_vector(ram_addr);
								when 3 =>	-- take data from RAM
									r_flag_enet <= '1';

									tx_data_sr_content(7) <= r_data(63 downto 56);
									tx_data_sr_content(6) <= r_data(55 downto 48);
									tx_data_sr_content(5) <= r_data(47 downto 40);
									tx_data_sr_content(4) <= r_data(39 downto 32);
									tx_data_sr_content(3) <= r_data(31 downto 24);
									tx_data_sr_content(2) <= r_data(23 downto 16);
									tx_data_sr_content(1) <= r_data(15 downto  8);
									tx_data_sr_content(0) <= r_data( 7 downto  0);

								when 5 =>
									ram_addr := ram_addr + x"01";

									r_flag_enet <= '0';
								when others =>
									r_flag_enet <= '0';
							end case;
						else
							r_flag_enet <= '0';
						end if;
				end case;
			else
				r_flag_enet <= '0';
			end if;
		end if;
	end process;

end Behavioral;

