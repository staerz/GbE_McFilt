----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			IKTP TU Dresden	
-- 
-- Create Date:		15:59:00 14/04/2010 
--
-- Module Name:		Halfadder - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		1 Bit Half Adder
--
-- Dependencies:	Using 
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HalfAdder is
	port (
		in1, in2:	in std_logic;
		sum, carry:	out std_logic
	);
end HalfAdder;

architecture RTL of HalfAdder is
	
begin
	sum <= in1 xor in2;
	carry <= in1 and in2;
end RTL;