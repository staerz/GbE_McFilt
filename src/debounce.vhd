----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:42:43 12/01/2009 
-- Design Name: 
-- Module Name:    debounce - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debounce is
    Port ( sig_in : in std_logic;
           clk : in std_logic;
           sig_out : out std_logic);
end debounce;

architecture Behavioral of debounce is


--  Provides a one-shot pulse from a non-clock input, with reset
signal Q1, Q2, Q3 : std_logic;
 
begin

	process(clk)
	begin
		if rising_edge(clk) then
			Q1 <= sig_in;
			Q2 <= Q1;
			Q3 <= Q2;
		end if;
	end process;

	sig_out <= Q1 and Q2 and (not Q3);
end Behavioral;

