----------------------------------------------------------------------------------
-- Engineer:		Rahul Vora, University of New Mexico
-- Editor:			Steffen St�rz, Technical University of Dresden
-- 
-- Create Date:		10:48:41 02/01/2010
--
-- to be found at:	http://www.cosmiac.org/tutorial_6.html
--
-- Module Name:		lcd - Behavior
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		Level-0-LCD driver using the 4-bit interface of this board
--					Manages LCD init and char transition as well as positionning
--
--					LCD can be displayed at LEDs (times might be to short to see)
--
-- Dependencies:	see entity LCD I/O port discription
--					clk : clock with 50MHz required
--					driven by LCD_driver normally
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lcd is
	port(
		reset : in std_logic;	--	reset to clear display
		clk : in std_logic;	--	clock signal: 50MHz required
		symbol : in std_logic_vector(7 downto 0);--character;	--	char to be displayed at LCD
		lcdx : integer range 0 to 16;	--	X-Position on the LCD
		lcdy : in integer range 0 to 2;	--	Y-Position on the LCD
		write_flag : in std_logic;	--	flag for writing mode
		pos_flag : in std_logic;	--	flag for positionning mode
		SF_D : out std_logic_vector(3 downto 0);	--	output data to LCD
		LCD_E, LCD_RS, LCD_RW : out std_logic;	--	control commands to LCD
		LED : out std_logic_vector(7 downto 0)	--	visual effect to show LCD activity
	);
end lcd;

architecture behavior of lcd is

--	time constant definition based on clock:
--	twenty_nanosec := 1 for 50 MHz
--	set it to 2 for 100 MHz clock or to 4 for 200 MHz
	constant twenty_nanosec : integer := 1;

--	type definition for LCD setup state machine
	type tx_sequence is (high_setup, high_hold, oneus, low_setup, low_hold, fortyus, done);
	signal tx_state : tx_sequence := done;
	signal tx_byte : std_logic_vector(7 downto 0);
	signal tx_init : std_logic := '0';

--	type definition for LCD init sequence state machine
	type init_sequence is (idle, fifteenms, one, two, three, four, five, six, seven, eight, done);
	signal init_state : init_sequence := idle;
	signal init_init, init_done : std_logic := '0';

	constant fourty_microsec : integer := 2000*twenty_nanosec;

--	counter variables
	signal i1 : integer range 0 to 750001*twenty_nanosec := 0;
	signal i2 : integer range 0 to 2001*twenty_nanosec := 0;
	signal i3 : integer range 0 to 82001*twenty_nanosec := 0;

--	data transfer to LCD
	signal SF_D0, SF_D1 : std_logic_vector(3 downto 0);
--	data control for LCD
	signal LCD_E0, LCD_E1 : std_logic;
	signal mux : std_logic;

--	type definition for LCD control & data transfer state machine
	type display_state is (init, function_set, entry_set, set_display, clr_display, pause, set_addr, char, done);
	signal cur_state : display_state := init;

--	char and positionning data
--	signal upper_4bit	: std_logic_vector(3 downto 0);
--	signal lower_4bit	: std_logic_vector(3 downto 0);
	signal upper_pos_bits	: std_logic_vector(3 downto 0);
	signal lower_pos_bits	: std_logic_vector(3 downto 0);

begin
	LED <= tx_byte;	--for diagnostic purposes
	LCD_RW <= '0';	--write only

	--The following "with" statements simplify the process of adding and removing states.

	--when to transmit a command/data and when not to
	with cur_state select
		tx_init <= '0' when init | pause | done,
			'1' when others;

	--control the bus
	with cur_state select
		mux <= '1' when init,
			'0' when others;

	--control the initialization sequence
	with cur_state select
		init_init <= '1' when init,
			'0' when others;
	
	--register select
	with cur_state select
		LCD_RS <= '0' when function_set|entry_set|set_display|clr_display|set_addr,
			'1' when others;
	
	with lcdy select
		upper_pos_bits <=
			"1100" when 2,
			"1000" when others;-- == 1 as default value
	
	with lcdx select
		lower_pos_bits <=
			CONV_STD_LOGIC_VECTOR(lcdx-1, 4) when 1 to 16,
			"0000" when others; -- == 1 as default value

--		no need anymore as symbol now comes as std_logic_vector directly
--		for information purpose only (ASCII look up table...)
--	with symbol select
--		upper_4bit <= 
--			"0010" when ' ' | '!'       | '#' | '$' | '%' | '&' | ''' | '(' | ')' | '*' | '+' | ',' | '-' | '.' | '/' | '"', 
--			"0011" when '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | ':' | ';' | '<' | '=' | '>' | '?',
--			"0100" when '@' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O',
--			"0101" when 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | '['       | ']' | '^' | '_',
--			"0110" when '`' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o',
--			"0111" when 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z' | '{' | '|' | '}' | '~',
--			"0000" when others;
--
--	with symbol select
--		lower_4bit <= 
--			"0000" when ' ' | '0' | '@' | 'P' | '`' | 'p',
--			"0001" when '!' | '1' | 'A' | 'Q' | 'a' | 'q',
--			"0010" when       '2' | 'B' | 'R' | 'b' | 'r' | '"',
--			"0011" when '#' | '3' | 'C' | 'S' | 'c' | 's',
--			"0100" when '$' | '4' | 'D' | 'T' | 'd' | 't',
--			"0101" when '%' | '5' | 'E' | 'U' | 'e' | 'u',
--			"0110" when '&' | '6' | 'F' | 'V' | 'f' | 'v',
--			"0111" when ''' | '7' | 'G' | 'W' | 'g' | 'w',
--			"1000" when '(' | '8' | 'H' | 'X' | 'h' | 'x',
--			"1001" when ')' | '9' | 'I' | 'Y' | 'i' | 'y',
--			"1010" when '*' | ':' | 'J' | 'Z' | 'j' | 'z',
--			"1011" when '+' | ';' | 'K' | '[' | 'k' | '{',
--			"1100" when ',' | '<' | 'L'       | 'l' | '|',
--			"1101" when '-' | '=' | 'M' | ']' | 'm' | '}',
--			"1110" when '.' | '>' | 'N' | '^' | 'n' | '~',
--			"1111" when '/' | '?' | 'O' | '_' | 'o',
--			"0000" when others;

------------------------------  <-  80 chars  ->  --------------------------------
--                                                                              --
--         Selection which byte to transmit to LCD (see datasheet)              --
--                                                                              --
----------------------------------------------------------------------------------
	with cur_state select
		tx_byte <=
			"00101000" when function_set,	-- set 4 bit mode
			"00000110" when entry_set,		-- move curser right after writing
--			"00001100" when set_display,	-- display on, cursor off, blink off
			"00001111" when set_display,	-- display on, cursor on, blink on
			"00000001" when clr_display,	-- clears display and returns home
			upper_pos_bits & lower_pos_bits when set_addr,	-- set cursor position
--			upper_4bit & lower_4bit when char,	-- give char as ascii-(bit)-value
--			char_to_byte(symbol) when char,
			symbol when char,
			"00000000" when others;
		
	--main state machine
	display: process(clk, reset)
	begin
		if(reset='1') then
			cur_state <= function_set;
		elsif(rising_edge(clk)) then
			case cur_state is
				--refer to intialize state machine below
				when init =>
					if(init_done = '1') then
						cur_state <= function_set;
					else
						cur_state <= init;
					end if;

				--every other state but pause uses the transmit state machine
				when function_set =>
					if(i2 = fourty_microsec) then
						cur_state <= entry_set;
					else
						cur_state <= function_set;
					end if;	
				
				when entry_set =>
					if(i2 = fourty_microsec) then
						cur_state <= set_display;
					else
						cur_state <= entry_set;
					end if;
				
				when set_display =>
					if(i2 = fourty_microsec) then
						cur_state <= clr_display;
					else
						cur_state <= set_display;
					end if;
				
				when clr_display =>
					i3 <= 0;
					if(i2 = fourty_microsec) then
						cur_state <= pause;
					else
						cur_state <= clr_display;
					end if;

				when pause =>
					if(i3 = 41*fourty_microsec) then
						cur_state <= set_addr;
						i3 <= 0;
					else
						cur_state <= pause;
						i3 <= i3 + 1;
					end if;

				when set_addr =>
					if(i2 = fourty_microsec) then
						cur_state <= done;
					else
						cur_state <= set_addr;
					end if;

				when char =>
					if(i2 = fourty_microsec) then
						cur_state <= done;
					else
						cur_state <= char;
					end if;

				when done =>
					if pos_flag = '1' then
						cur_state <= set_addr;
					elsif (write_flag = '1') then
						cur_state <= char;
					else
						cur_state <= done;
					end if;

			end case;
		end if;
	end process display;

	with mux select
		SF_D <= SF_D0 when '0', --transmit
			SF_D1 when others;	--initialize
	with mux select
		LCD_E <= LCD_E0 when '0', --transmit
			LCD_E1 when others; --initialize

	--specified by datasheet
	transmit : process(clk, reset, tx_init)
	begin
		if(reset='1') then
			tx_state <= done;
		elsif(clk='1' and clk'event) then
			i2 <= i2 + 1;
			case tx_state is
				when high_setup => --40ns
					LCD_E0 <= '0';
					SF_D0 <= tx_byte(7 downto 4);
					if(i2 = 3*twenty_nanosec) then
						tx_state <= high_hold;
						i2 <= 0;
					else
						tx_state <= high_setup;
					end if;

				when high_hold => --230ns
					LCD_E0 <= '1';
					SF_D0 <= tx_byte(7 downto 4);
					if(i2 = 13*twenty_nanosec) then
						tx_state <= oneus;
						i2 <= 0;
					else
						tx_state <= high_hold;
					end if;

				when oneus =>
					LCD_E0 <= '0';
					if(i2 = 51*twenty_nanosec) then
						tx_state <= low_setup;
						i2 <= 0;
					else
						tx_state <= oneus;
					end if;

				when low_setup =>
					LCD_E0 <= '0';
					SF_D0 <= tx_byte(3 downto 0);
					if(i2 = 3*twenty_nanosec) then
						tx_state <= low_hold;
						i2 <= 0;
					else
						tx_state <= low_setup;
					end if;

				when low_hold =>
					LCD_E0 <= '1';
					SF_D0 <= tx_byte(3 downto 0);
					if(i2 = 13*twenty_nanosec) then
						tx_state <= fortyus;
						i2 <= 0;
					else
						tx_state <= low_hold;
					end if;

				when fortyus =>
					LCD_E0 <= '0';
					if(i2 = fourty_microsec) then
						tx_state <= done;
						i2 <= 0;
					else
						tx_state <= fortyus;
					end if;

				when done =>
					LCD_E0 <= '0';
					if(tx_init = '1') then
						tx_state <= high_setup;
					else
						tx_state <= done;
					end if;
					i2 <= 0;

			end case;
		end if;
	end process transmit;
					
	--specified by datasheet
	power_on_initialize: process(clk, reset, init_init) --power on initialization sequence
	begin
		if(reset='1') then
			init_state <= idle;
			init_done <= '0';
		elsif(clk='1' and clk'event) then
			i1 <= i1 + 1;
			case init_state is
				when idle =>	
					init_done <= '0';
					if(init_init = '1') then
						init_state <= fifteenms;
						i1 <= 0;
					else
						init_state <= idle;
					end if;
				
				when fifteenms =>
					init_done <= '0';
					if(i1 = 750001*twenty_nanosec) then
						init_state <= one;
						i1 <= 0;
					else
						init_state <= fifteenms;
					end if;

				when one =>
					SF_D1 <= "0011";
					LCD_E1 <= '1';
					init_done <= '0';
					if(i1 = 12*twenty_nanosec) then
						init_state<=two;
						i1 <= 0;
					else
						init_state<=one;
					end if;

				when two =>
					LCD_E1 <= '0';
					init_done <= '0';
					if(i1 = 205001*twenty_nanosec) then
						init_state<=three;
						i1 <= 0;
					else
						init_state<=two;
					end if;

				when three =>
					SF_D1 <= "0011";
					LCD_E1 <= '1';
					init_done <= '0';
					if(i1 = 12*twenty_nanosec) then	
						init_state<=four;
						i1 <= 0;
					else
						init_state<=three;
					end if;

				when four =>
					LCD_E1 <= '0';
					init_done <= '0';
					if(i1 = 5001*twenty_nanosec) then
						init_state<=five;
						i1 <= 0;
					else
						init_state<=four;
					end if;

				when five =>
					SF_D1 <= "0011";
					LCD_E1 <= '1';
					init_done <= '0';
					if(i1 = 12*twenty_nanosec) then
						init_state<=six;
						i1 <= 0;
					else
						init_state<=five;
					end if;

				when six =>
					LCD_E1 <= '0';
					init_done <= '0';
					if(i1 = fourty_microsec) then
						init_state<=seven;
						i1 <= 0;
					else
						init_state<=six;
					end if;

				when seven =>
					SF_D1 <= "0010";
					LCD_E1 <= '1';
					init_done <= '0';
					if(i1 = 12*twenty_nanosec) then
						init_state<=eight;
						i1 <= 0;
					else
						init_state<=seven;
					end if;

				when eight =>
					LCD_E1 <= '0';
					init_done <= '0';
					if(i1 = fourty_microsec) then
						init_state<=done;
						i1 <= 0;
					else
						init_state<=eight;
					end if;
				when done =>
					init_state <= done;
					init_done <= '1';
			end case;
		end if;
	end process power_on_initialize;

end behavior;