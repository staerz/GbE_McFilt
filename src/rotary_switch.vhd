----------------------------------------------------------------------------------
-- Create Date:		10:48:41 12/07/2009
--
-- Module Name:		write_8leds - Behavioral 
-- Description:		Making the transition of Byte-In to Bit-Out for LEDs
--
-- Dependencies:	None
--
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- 
-- Create Date:		16:56:27 12/14/2009 
-- Design Name:
-- Module Name:		rotary_switch - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		Decodes the rotary switch direct output to status variables
--					turned_right and turned_left
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rotary_switch is
	port(
		clk : in std_logic;						-- system clock
		rotary : in bit_vector (0 to 1);		-- direct rotary switch input
		turned_right : out std_logic;			-- status right output
		turned_left : out std_logic;			-- status left output
		status_LEDs : out bit_vector(0 to 7)	-- monitoring LEDs output
	);
end rotary_switch;

architecture Behavioral of rotary_switch is

	type state is (initial, state1, state2, turned);

	signal right, next_right : state;	-- processing turning right
	signal left, next_left   : state;	-- processing turning left

begin

	process (clk, rotary)--, right, count)
	begin
----------------------------------------------------------------------------------
--	initial state: rotary switch not turned
----------------------------------------------------------------------------------
		turned_right <= '0';
		turned_left <= '0';
----------------------------------------------------------------------------------
--	temporal rotary switch behavior when turning right:
--
--	initial position -> state 1 -> state 2 -> state 3 -> initial position
--
--	       "00"      ->   "01"  ->   "11"  ->   "10"  ->       "00"
--
--	right: initial   ->  state1 ->  state2 ->  turned ->    initial
--
--	temporal rotary switch behavior when turning left:
--
--	       "00"      ->   "10"  ->   "11"  ->   "01"  ->       "00"
--
--	left : initial   ->  state1 ->  state2 ->  turned ->    initial
--
----------------------------------------------------------------------------------
		case rotary is
----------------------------------------------------------------------------------
--	cases in order of turning right:
--		read from top to bottom for right, read from bottom to top for left
----------------------------------------------------------------------------------
			when "00" =>
				if right = turned then
					status_LEDs(5 to 7) <= "111";
					turned_right <= '1';
				else
					status_LEDs(5 to 7) <= "000";
				end if;
				next_right <= initial;

				if left = turned then
					turned_left <= '1';
				end if;
				next_left <= initial;
			when "01" =>
				next_right <= state1;
				status_LEDs(5 to 7) <= "001";

				if left = state2 or left = turned then
					next_left <= turned;
				else
					next_left <= initial;
				end if;
			when "11" =>
				if right = state1 or right = state2 then
					next_right <= state2;
					status_LEDs(5 to 7) <= "010";
				else
					next_right <= initial;
					status_LEDs(5 to 7) <= "110";
				end if;

				if left = state1 or left = state2 then
					next_left <= state2;
				else
					next_left <= initial;
				end if;
			when "10" =>
				if right = state2 or right = turned then
					next_right <= turned;
					status_LEDs(5 to 7) <= "100";
				else
					next_right <= initial;
					status_LEDs(5 to 7) <= "010";
				end if;

				next_left <= state1;
		end case;
	end process;


----------------------------------------------------------------------------------
--	stepping
----------------------------------------------------------------------------------
	timing : process (clk) is
	begin
		if	(rising_edge(clk)) then
			left <= next_left;
			right <= next_right;
		end if;
	end process timing; 

----------------------------------------------------------------------------------
--	monitoring algorithm activity
----------------------------------------------------------------------------------
	monitoring_right : process (next_right) is
	begin
		case next_right is
			when initial =>
				status_LEDs(2 to 4) <= "000";
			when state1 =>
				status_LEDs(2 to 4) <= "001";
			when state2 =>
				status_LEDs(2 to 4) <= "010";
			when turned =>
				status_LEDs(2 to 4) <= "100";
		end case;
	end process monitoring_right;
	
----------------------------------------------------------------------------------
--	monitoring rotary switch activity
----------------------------------------------------------------------------------
	status_LEDs(0 to 1) <= rotary(0 to 1);

end Behavioral;