library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity recalibration is
	generic (
		coeff_bytes		: positive
	);
	port (
--	controls
		clk				: in  std_logic;
		rst				: in  std_logic;

		calib_start		: in  std_logic;
		calib_enable	: out std_logic;
		calib_valid		: out std_logic;
--	RAM management
		r_addr_calib	: out std_logic_vector(7 downto 0);
		r_flag_calib	: out std_logic;
		r_data_calib	: in  std_logic_vector(63 downto 0);
--	coefficients upload
		cena			: out  std_logic;
		cdin			: out  std_logic
	);
end recalibration;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of recalibration is

----- BEGIN SIMULATION
--
--	signal global_count	: integer range 0 to 1000 := 0;
--
---- port definitions
----	controls
--	signal rst			: std_logic;
--	signal clk			: std_logic;
--	signal calib_start	: std_logic;
--	signal calib_enable	: std_logic;
--	signal calib_valid	: std_logic;
----	RAM management
--	signal r_addr_calib	: std_logic_vector(7 downto 0);
--	signal r_flag_calib	: std_logic;
--	signal r_data_calib	: std_logic_vector(63 downto 0);
----	coefficients upload
--	signal cena			:  std_logic; -- Enable: Shift cdin on cclk
--	signal cdin			:  std_logic; -- data (bit by bit)
--
----- END SIMULATION

	type t_state is (none, count, pre_read, ram_read, cbyte, pre_done, done);
	signal state			: t_state := none;

	signal calib_pre_done	: std_logic;
	signal calib_count		: unsigned(5 downto 0) := (others => '0');

begin

	state_machine: process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				state <= none;
			else
				case state is
					when none =>
						if calib_start = '1' then
							state <= pre_read;
						else
							state <= none;
						end if;
					when pre_read =>
						state <= ram_read;
					when ram_read =>
						state <= count;
					when count =>
						if calib_count mod 8 = 6 then
							state <= cbyte;
						else
							state <= count;
						end if;
					when cbyte =>
						if calib_pre_done = '1' then
							state <= pre_done;
						elsif calib_count = 63 then
							state <= pre_read;
						else
							state <= count;
						end if;
					when pre_done =>
						state <= done;
					when done =>
						if calib_start = '1' then
							state <= pre_read;
						else
							state <= done;
						end if;
				end case;
			end if;
		end if;
	end process;

	calib_counting: process(clk) is
	begin
		if rising_edge(clk) then
			case state is
				when none | done => calib_count <= (others => '0');
				when others => calib_count <= calib_count + 1;
			end case;
		end if;
	end process;

	calib_flags: process(clk) is
	begin
		if rising_edge(clk) then
			case state is
				when none =>
					calib_enable <= '0';
					calib_valid <= '0';
				when done =>
					calib_enable <= '0';
					calib_valid <= '1';
				when others =>
					calib_enable <= '1';
					calib_valid <= '0';
			end case;
		end if;
	end process;

	bytes_counting: block
	signal byte_count	: signed(log2ceil(coeff_bytes) downto 0);
	begin
		process(clk) is
		begin
			if rising_edge(clk) then
				case state is
	-- for testing (SIMULATION):
	--				when none | done	=> byte_count <= to_signed(16-2, byte_count'length);
					when none | done	=> byte_count <= to_signed(coeff_bytes - 2, byte_count'length);
					when cbyte	=> byte_count <= byte_count - to_signed(1, byte_count'length);
					when others	=> byte_count <= byte_count;
				end case;
			end if;
		end process;

		calib_pre_done <= byte_count(byte_count'left);
	end block;
	
	cena_set: process(clk) is
	begin
		if rising_edge(clk) then
			case state is
				when none => cena <= '0';
				when ram_read => cena <= '1';
				when done => cena <= '0';
				when others => null;
			end case;
		end if;
	end process;
	
	recalibration: block
	signal ram_addr	: unsigned(7 downto 0);
	signal calib_sr	: std_logic_vector(63 downto 0);
	begin
		process(clk) is
		begin
			if rising_edge(clk) then
				--	keep up shifting data into cdin
				calib_sr <= calib_sr(calib_sr'left-1 downto calib_sr'right) & '0';
				case state is
					when none | done =>
						calib_sr <= (others => '-');
						ram_addr <= ram_data_start;
					when ram_read =>
						calib_sr <= r_data_calib;
						ram_addr <= ram_addr + 1;
					when others => null;
				end case;
			end if;
		end process;

		cdin <= calib_sr(calib_sr'left);
		r_addr_calib <= std_logic_vector(ram_addr);
	end block;

	with state select r_flag_calib <=
		'1' when pre_read,
		'0' when others;


----- BEGIN SIMULATION
---- Clock process definitions
--clk_n_count_process :process 
--begin
--	clk <= '1';
--	global_count <= global_count + 1;
--	wait for 5ns;
--	clk <= '0';
--	wait for 5ns;
--end process;
--
--with global_count select rst <=
--	'1' when 1,
--	'0' when others;
--
--with global_count select calib_start <=
--	'1' when 9,-- | 200,
--	'0' when others;
--   
--with ram_addr select r_data_calib <=
--	x"f123456789abcdef" when x"08",
--	x"f987654321fedcbf" when x"09",
--	x"f1348743150613ff" when x"0a",
--	x"f34190870ae805cf" when others;
----- END SIMULATION

end Behavioral;