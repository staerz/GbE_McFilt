----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		13:46:15 08/30/2010
--
-- Module Name:		hilo_dectect - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:		detect high-low change on sig_in
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity hilo_detect is
    Port (
		clk		: in  std_logic;
		sig_in	: in  std_logic;
		sig_out	: out std_logic
	);
end hilo_detect;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of hilo_detect is

signal Q1, Q2 : std_logic;
 
begin

	process(clk)
	begin
		if rising_edge(clk) then
			Q1 <= sig_in;
			Q2 <= Q1;
		end if;
	end process;

	sig_out <= not(Q1) and Q2;

end Behavioral;

