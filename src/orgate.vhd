----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			IKTP TU Dresden	
-- 
-- Create Date:		15:59:00 14/04/2010 
--
-- Module Name:		Orgate - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		Simple or gate for Fulladder
--
-- Dependencies:	Using 
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity orgate is
	port(
		a, b : in std_logic;
		res : out std_logic
	);
end orgate;

architecture Behavioral of orgate is

begin
	res <= a or b;
end Behavioral;

