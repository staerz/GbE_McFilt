--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:06:01 09/08/2010
-- Design Name:   
-- Module Name:   E:/Xilinx_Projects/Steffen/GbE_Calc_optimise/McFilt_Manager_sim.vhd
-- Project Name:  GbE_Calc_optimise
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: McFilt_manager
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY McFilt_Manager_sim IS
END McFilt_Manager_sim;
 
ARCHITECTURE behavior OF McFilt_Manager_sim IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	component McFilt_manager is
	port (
--	clocking
		clk_125MHz		: in  std_logic;
		clk_mcfilt		: in  std_logic;
		rst				: in  std_logic;

--	communication McFilt-manager -> McFilt
		mcfilt_enable	: out std_logic;
		mcfilt_reset	: out std_logic;

--	RAM read requests of McFilt manager:
		r_flag_mcfilt	: out std_logic;
		r_addr_mcfilt	: out std_logic_vector(7 downto 0);
		r_fine_mcfilt	: in  std_logic;
		r_data			: in  std_logic_vector(63 downto 0);

--	RAM write requests of McFilt manager:
		w_flag_mcfilt	: out std_logic;
		w_addr_mcfilt	: out std_logic_vector(7 downto 0);
		w_data_mcfilt	: out std_logic_vector(63 downto 0);
		w_fine_mcfilt	: in  std_logic;

--	external stimuli
		send_frame_flag	: out std_logic;

--	communication McFilt manager <-> UDP module
		calc_start		: in  std_logic;
		calc_enable		: out std_logic;

--	Energy to be put into McFilt
		E				: out std_logic_vector(15 downto 0);
--	Result from McFilt
		S				: in  std_logic_vector(47 downto 0);
		inval			: in  std_logic;
		c_valid			: in  std_logic
	);
	end component;
    
	signal inval	: std_logic;
	
	component mcfilt is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Calculation routine: DEPTH-stage filter of E to S                  --
--                                                                            --
--------------------------------------------------------------------------------
	port (
--	calculation controls
		clk			: in  std_logic;
		rst			: in  std_logic;
		en			: in  std_logic;
--	E: energy samples in
--	S: sums out
		E			: in  std_logic_vector(15 downto 0);
		S			: out std_logic_vector(47 downto 0);
		inval		: out std_logic;
		c_valid		: out std_logic;
--	coefficients upload
		clk_calib	: in  std_logic;
		cena		: in  std_logic; -- Enable: Shift cdin on cclk
		cdin		: in  std_logic; -- data (bit by bit)
		cdout		: out std_logic
	);
	end component;


	component ranging is
	port (
--	controls
--		rst : in  std_logic;
--		clk : in  std_logic;
--		en  : in  std_logic;

--	unranged input
		Input  : in  std_logic_vector(47 downto 0);
--	invalidity flag of input
		inval  : in  std_logic;
--	ranged output
		Output : out std_logic_vector(15 downto 0)
	);
	end component;

	component RAM_management is
	port (
--	controls
		clk				: in  std_logic;
		rst				: in  std_logic;

--	RAM requests of calibration: read only
		r_flag_calib	: in std_logic;
		r_addr_calib	: in std_logic_vector(7 downto 0);
		r_fine_calib	: out std_logic;

--	RAM read requests of UDP-module:
		r_flag_enet		: in std_logic;
		r_addr_enet		: in std_logic_vector(7 downto 0);
		r_fine_enet		: out std_logic;

--	RAM write requests of UDP-module:
		w_flag_enet		: in std_logic;
		w_addr_enet		: in std_logic_vector(7 downto 0);
		w_data_enet		: in std_logic_vector(63 downto 0);
		w_fine_enet		: out std_logic;

--	RAM read requests of McFilt:
		r_flag_mcfilt	: in std_logic;
		r_addr_mcfilt	: in std_logic_vector(7 downto 0);
		r_fine_mcfilt	: out std_logic;

--	RAM write requests of McFilt:
		w_flag_mcfilt	: in std_logic;
		w_addr_mcfilt	: in std_logic_vector(7 downto 0);
		w_data_mcfilt	: in std_logic_vector(63 downto 0);
		w_fine_mcfilt	: out std_logic;

--	RAM requests of LCD module: read only
		r_flag_lcd		: in std_logic;	-- in read demand
		r_addr_lcd		: in std_logic_vector(7 downto 0);	-- in read @
		r_fine_lcd		: out std_logic; -- out read accepted

--	RAM data output
		r_data			: out std_logic_vector(63 downto 0)
	);
	end component RAM_management;

   --Inputs
   signal clk_125MHz : std_logic := '0';
   signal clk_mcfilt : std_logic := '0';
   signal rst : std_logic := '0';
   signal r_fine_mcfilt : std_logic := '0';
   signal r_data : std_logic_vector(63 downto 0) := (others => '0');
   signal w_fine_mcfilt : std_logic := '0';
   signal calc_start : std_logic := '0';
   signal S_out : std_logic_vector(15 downto 0) := (others => '0');
   signal S : std_logic_vector(47 downto 0) := (others => '0');
   signal c_valid : std_logic := '0';

 	--Outputs
   signal mcfilt_enable : std_logic;
   signal mcfilt_reset : std_logic;
   signal r_flag_mcfilt : std_logic;
   signal r_addr_mcfilt : std_logic_vector(7 downto 0);
   signal w_flag_mcfilt : std_logic;
   signal w_addr_mcfilt : std_logic_vector(7 downto 0);
   signal w_data_mcfilt : std_logic_vector(63 downto 0);
   signal send_frame_flag : std_logic;
   signal calc_enable : std_logic;
   signal E : std_logic_vector(15 downto 0);

	-- RAM management
	signal w_flag_enet		: std_logic := '0';
	signal w_addr_enet		: std_logic_vector(7 downto 0) := x"00";
	signal w_data_enet		: std_logic_vector(63 downto 0);
	signal w_fine_enet		: std_logic;

   -- Clock period definitions
   constant clk_125MHz_period : time := 8 ns;
   constant clk_mcfilt_period : time := 3 ns;
 
BEGIN
 
	McFilt_manage: McFilt_manager
	port map (
--	clocking
		clk_125MHz		=> clk_125MHz,
		clk_mcfilt		=> clk_mcfilt,
		rst				=> rst,

--	communication McFilt-manager -> McFilt
		mcfilt_enable	=> mcfilt_enable,
		mcfilt_reset	=> mcfilt_reset,

--	RAM read requests of McFilt manager:
		r_flag_mcfilt	=> r_flag_mcfilt,
		r_addr_mcfilt	=> r_addr_mcfilt,
		r_fine_mcfilt	=> r_fine_mcfilt,
		r_data			=> r_data,

--	RAM write requests of McFilt manager:
		w_flag_mcfilt	=> w_flag_mcfilt,
		w_addr_mcfilt	=> w_addr_mcfilt,
		w_data_mcfilt	=> w_data_mcfilt,
		w_fine_mcfilt	=> w_fine_mcfilt,

--	external stimuli
		send_frame_flag	=> send_frame_flag,

--	communication McFilt manager <-> UDP module
		calc_start		=> calc_start,
		calc_enable		=> calc_enable,

--	Energy to be put into McFilt
	    E				=> E,
--	Result from McFilt
		S				=> S,
		inval			=> inval,
		c_valid			=> c_valid
	);

	E_calculation: mcfilt
	port map (
--	calculation controls
		clk			=> clk_mcfilt,
		rst 		=> mcfilt_reset,
		en  		=> mcfilt_enable,
--	E: energy samples in
--	S: sums out
		E   		=> E,
		S   		=> S,
		inval		=> inval,
		c_valid		=> c_valid,
--	coefficients upload
		clk_calib	=> clk_125MHz,
		cena		=> '0',--cena, -- Enable: Shift cdin on cclk
		cdin		=> '0',--cdin, -- data (bit by bit)
		cdout		=> open--cdout
	);

----	delay_ranging: block
----	signal S_reg : std_logic_vector(47 downto 0);
----	signal iinval : std_logic;
----	begin
----		process (clk_mcfilt)
----		begin
----			if rising_edge(clk_mcfilt) then
------				if mcfilt_reset_i = '1' then
------				else
----					S_reg <= S;
----					iinval <= inval;
------				end if;
----			end if;
----		end process;
----		
--		makeranging: ranging
--		port map (
--	--	controls
--	--		rst => calc_reset,
--	--		clk => rtx_clk,
--	--		en  => mcfilt_enable,
--
--	--	unranged input
----			Input  => S_reg,--S,
--			Input  => S,
--	--	invalidity flag of input 3 clk cycles before (due to mcfilt)
----			inval  => iinval,--inval,
--			inval  => inval,
--	--	ranged output
--			Output => S_out
--		);
----	end block;

	manage_RAM: RAM_management
	port map (
--	controls
		clk				=> clk_125MHz,
		rst				=> rst,

--	RAM requests of calibration
		r_flag_calib	=> '0',
		r_addr_calib	=> (others => '0'),
		r_fine_calib	=> open,

--	RAM read requests of UDP module
		r_flag_enet		=> '0',
		r_addr_enet		=> (others => '0'),
		r_fine_enet		=> open,

--	RAM write requests of UDP module
		w_flag_enet		=> w_flag_enet,
		w_addr_enet		=> w_addr_enet,
		w_data_enet		=> w_data_enet,
		w_fine_enet		=> w_fine_enet,

--	RAM read requests of McFilt
		r_flag_mcfilt	=> r_flag_mcfilt,
		r_addr_mcfilt	=> r_addr_mcfilt,
		r_fine_mcfilt	=> r_fine_mcfilt,

--	RAM write requests of McFilt
		w_flag_mcfilt	=> w_flag_mcfilt,
		w_addr_mcfilt	=> w_addr_mcfilt,
		w_data_mcfilt	=> w_data_mcfilt,
		w_fine_mcfilt	=> w_fine_mcfilt,

--	RAM requests of LCD module
		r_flag_lcd		=> '0',
		r_addr_lcd		=> (others => '0'),
		r_fine_lcd		=> open,

--	RAM data output
		r_data			=> r_data
	);

   -- Clock process definitions
   clk_125MHz_process :process
   begin
		clk_125MHz <= '0';
		wait for clk_125MHz_period/2;
		clk_125MHz <= '1';
		wait for clk_125MHz_period/2;
   end process;
 
   clk_mcfilt_process :process
   begin
		clk_mcfilt <= '0';
		wait for clk_mcfilt_period/2;
		clk_mcfilt <= '1';
		wait for clk_mcfilt_period/2;
   end process;
 

	-- Stimulus process
	stim_proc: process
	begin		
		-- hold reset state for 100 ms.
		rst <= '1';
--		wait for 100 ms;	

		wait for clk_125MHz_period*10.5;
		rst <= '0';
		
		-- insert stimulus here 
		
		-- Some Input Data
		w_flag_enet <= '1';
		w_addr_enet <= x"08";
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_data_enet <= (others => '0');
		w_addr_enet <= x"00";
		w_flag_enet <= '0';
		-- Start Calc
		calc_start <= '1';
		wait for clk_125MHz_period;
		calc_start <= '0';
		
		wait until calc_enable = '0';
		wait for clk_125MHz_period*20;

		-- again Some Input Data
		w_flag_enet <= '1';
		w_addr_enet <= x"08";
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4023_800C_88DE_C84B";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4030_8034_0000_C61C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C003_8034_CA52_C456";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4012_C01C_843C_40A3";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"4007_802A_C3D7_C5CB";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"800A_400A_C2DE_8A35";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C023_4003_C53C_446F";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"C01C_402E_C29D_81E5";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400A_801B_89BE_421C";
		wait for clk_125MHz_period;
		w_addr_enet <= std_logic_vector(unsigned(w_addr_enet) + 1);
		w_data_enet <= x"400B_8035_CA3C_C2B7";
		wait for clk_125MHz_period;
		w_data_enet <= (others => '0');
		w_addr_enet <= x"00";
		w_flag_enet <= '0';

		-- Start Calc
		calc_start <= '1';
		wait for clk_125MHz_period;
		calc_start <= '0';

		wait;
	end process;

--	-- Emulate McFilt
--	mcfilt_proc: process(clk_mcfilt)
--	begin
--		if rising_edge(clk_mcfilt) then
--			S_out <= E;
--			c_valid <= mcfilt_enable;
--		end if;
--	end process;
END;
