----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		11:01:48 08/13/2010 
--
-- Module Name:		UDP_rx_module - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--
--	The module will treat incomming UDP-data packets and save them to RAM.
--	Only well formatted packets are accepted:
--		calibration packets have to wear calib_signature - see my_definitions
--		calculation packets have to wear mcfilt_signature - see my_definitions
--
--	The data is then organised in RAM as shown:
--
--        RAM-@ | content (63 downto 0) in bytes: -- == don't care (N/A)|
--        ------|------'------'------'------'------'------'------'------|
--         00   |  --  '  --  '  --  '  --  '  --  '  --  '  --  '  --  |
--         01   |  "D" '  " " ' 6 by_'__tes_'_DST__'_MAC__'_addr_'_ess  |
--         02   |  "S" '  " " ' 6 by_'__tes_'_SRC__'_MAC__'_addr_'_ess  |
--         03   |  UDP_'_len  '  --  '  --  '  --  '  --  '  Ty__'__pe  |
--         04   |  IHL '  TOS 'Total_'_Len  '  Id_ ' _ent ' Frag_'_ment |
--         05   |  --  '  --  '  --  '  --  '  TTL ' Prot '  CRC_'_CRC  |
--         06   |  IP-_'_SRC__'_addr_'_ess  '  IP-_'_DST__'_addr_'_ess  |
--         07   | UDP-_'_head_'_er:__'_see__'_UDP__'_docu_'_ment_'_ation|
--         -----|------'------'------'------'------'------'------'------|
--         08   | data_'_data_'_data_'_data_'_data_'_data_'_data_'_data |
--         ..   |  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  |
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UDP_rx_module is
	port (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		rx_data_in			: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n			: in  std_logic; -- Input start of frame
		rx_eof_in_n			: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready

--	Receiving counter for outer module
		rx_counter			: out std_logic_vector(10 downto 0);

--	communication UDP module <-> LCD module
		capture_src_flag	: out std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: out std_logic;	-- DST MAC @ was found
		mac_readout_flag	: in  std_logic;	-- MACs have been read out

--	communication UDP module <-> Calibration module
		calib_start			: out std_logic;
		calib_enable		: in  std_logic;

--	communication UDP module <-> McFilt module
		calc_start			: out std_logic;
		calc_enable			: in  std_logic;

--	RAM write requests from UDP-module:
		w_flag_enet			: out std_logic;
		w_addr_enet			: out std_logic_vector(7 downto 0);
		w_data_enet			: out std_logic_vector(63 downto 0);
		w_fine_enet			: in  std_logic
	);
end UDP_rx_module;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of UDP_rx_module is

--	constants for receiving process
	constant rx_delay		: integer := 8;

--	counter for receive process
	signal rx_count			: integer range 0 to max_frame_length + rx_delay + 1 := 0;
	signal rx_frame_length	: integer range 0 to max_frame_length := max_frame_length;

	signal rx_enable		: std_logic;
	signal rx_sof			: std_logic;-- start of frame
	signal rx_eof			: std_logic;-- end of frame

	type sr9by8 is array (0 to 8) of std_logic_vector(7 downto 0);
	signal rx_data_sr_content	: sr9by8;	-- holds contents of rx_data in shift register

	signal calib_packet		: std_logic := '0';
	signal mcfilt_packet	: std_logic := '0';
	
	signal calib_done		: std_logic := '0';
	signal mcfilt_done		: std_logic := '0';
	
	signal packet_in_RAM	: std_logic := '0';

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         char (ASCII) to std_logic_vector conversion                        --
--                                                                            --
--------------------------------------------------------------------------------
	function char_to_byte(char: character) return std_logic_vector is
	begin
		return std_logic_vector(to_unsigned(character'pos(char),8));
	end char_to_byte;

--	registered detection of signal conversion from high to low
	component hilo_detect is
		Port (
			clk		: in  std_logic;
			sig_in	: in  std_logic;
			sig_out	: out std_logic
		);
	end component;
begin

	rx_counter <= std_logic_vector(to_unsigned(rx_count,11));

--	set control bits accordingly
	rx_enable <= not(rx_src_rdy_in_n);
	rx_eof <= not(rx_eof_in_n);
	rx_sof <= not(rx_sof_in_n);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process rx_data_sr_p                                              --
--                                                                            --
--   take data_in into shift register before saving 8 bytes at once to RAM    --
--   keeps on shifting the register when in receiving mode                    --
--   0 < rx_count needed as rx_enable will be 0 at the last <8 bytes to send  --
--                                                                            --
--------------------------------------------------------------------------------
	rx_data_sr_p : process(clk)
	begin
		if rising_edge(clk) then
			if (rx_enable = '1') or (0 < rx_count) then
				rx_data_sr_content <= rx_data_in & rx_data_sr_content (0 to 7);
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_rx_count_from_rx_sof                               --
--                                                                            --
--   assigning rx_count properly from rx_sof (receive start of frame) signal  --
--   and packet_in_RAM: no new packet will be received if the current is not  --
--   processed yet                                                            --
--   rx_delay is required for handling the last <8 bytes correctly at sending --
--                                                                            --
--------------------------------------------------------------------------------
	manage_rx_count_from_rx_sof : process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				rx_count <= 0;
			else	
				if (rx_sof = '1') and (packet_in_RAM = '0') then
					rx_count <= 1;
				else
					if (0 < rx_count) and (rx_count < rx_frame_length + rx_delay) then
						rx_count <= rx_count + 1;
					else
						rx_count <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process manage_rx_frame_length_from_rx_eof                        --
--                                                                            --
--   assigning rx_frame_length from rx_eof (receive end of frame) signal      --
--                                                                            --
--------------------------------------------------------------------------------
	manage_rx_frame_length_from_rx_eof : process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				rx_frame_length <= max_frame_length;
			elsif rx_enable = '1' then
				if rx_eof = '1' then
					rx_frame_length <= rx_count;
				else
					rx_frame_length <= max_frame_length;
				end if;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process capture_macs_from_stream                                  --
--                                                                            --
--   indicates when MAC-destination and -source addresses have passed         --
--   for LCD module indicating them                                           --
--                                                                            --
--------------------------------------------------------------------------------
	capture_macs_from_stream: process (clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				capture_src_flag <= '0';
				capture_dst_flag <= '0';
			else
				if mac_readout_flag = '1' then -- mac has already been read out, so reset flags
					capture_src_flag <= '0';
					capture_dst_flag <= '0';
				else
					case rx_count is
						when 6 => capture_dst_flag <= '1';
						when 12 => capture_src_flag <= '1';
						when others =>
					end case;
				end if;
			end if;
		end if;
	end process;

--	generate calib_done from falling calib_enable
	calib_done_detect: hilo_detect
	Port map (
		clk		=> clk,
		sig_in	=> calib_enable,
		sig_out	=> calib_done
	);

--	generalte mcfilt_done from falling calc_enable
	mcfilt_done_detect: hilo_detect
	Port map (
		clk		=> clk,
		sig_in	=> calc_enable,
		sig_out	=> mcfilt_done
	);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process analyse_rx_stream                                         --
--                                                                            --
--   analyses incomming stream whether it is a calibration or data packet     --
--                                                                            --
--   up to now the separation is done due to the identification flag          --
--   in the IP-header which would have to be changed to 1st byte of UDP-data  --
--                                                                            --
--------------------------------------------------------------------------------
	analyse_rx_stream: process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') or (mcfilt_done = '1') or (calib_done = '1') then
				calib_packet <= '0';
				mcfilt_packet <= '0';
				
				packet_in_RAM <= '0';	-- open for new packet (-> manage rx_count)
			elsif (rx_count = 22) then -- IP header part I/III
				case rx_data_sr_content(2) is
					when calib_signature =>
						calib_packet <= '1';
						mcfilt_packet <= '0';

						packet_in_RAM <= '1';
					when mcfilt_signature =>
						calib_packet <= '0';
						mcfilt_packet <= '1';

						packet_in_RAM <= '1';
					when others =>
						calib_packet <= '0';
						mcfilt_packet <= '0';

						packet_in_RAM <= '0';
				end case;
			else
				calib_packet <= calib_packet;
				mcfilt_packet <= mcfilt_packet;

				packet_in_RAM <= packet_in_RAM;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process mark_calibration_when_ready                               --
--                                                                            --
--   as soon as the first calibration data is wirtten to the RAM the process  --
--   will pull up the indication signal calib_start                           --
--   it can start already during the incoming of the packet                   --
--   as the calibration is a bit-by-bit process so very slow                  --
--                                                                            --
--------------------------------------------------------------------------------
	mark_calibration_when_ready: process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				calib_start <= '0';
			elsif (rx_count = 64) then -- first bigbyte already written to RAM
				if (calib_packet = '1') then
			-- pull up signal: start recalibration immediately
					calib_start <= '1';
				else
					calib_start <= '0';
				end if;
			else
				calib_start <= '0';
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process mark_calculation_when_ready                               --
--                                                                            --
--   as soon as the first calculation data is wirtten to the RAM the process  --
--   will pull up the indication signal calc_start                            --
--   it can only pull up the signal after the complete reception due to the   --
--   fast treatment of data by mcfilt                                         --
--                                                                            --
--------------------------------------------------------------------------------
	mark_calculation_when_ready: process(clk) is
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				calc_start <= '0';
			elsif (rx_count = rx_frame_length + rx_delay) then -- full packet written to RAM
				if (mcfilt_packet = '1') then
			-- pull up signal: start calculation immediately
					calc_start <= '1';
				else
					calc_start <= '0';
				end if;
			else
				calc_start <= '0';
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          Process capture_from_stream                                       --
--                                                                            --
--   main process dealing data input                                          --
--   data are taken from shift register and saved into RAM                    --
--   refere to header of this file for RAM organisation overview              --
--                                                                            --
--------------------------------------------------------------------------------
	capture_from_stream: process (clk) is
		variable ram_addr		: unsigned(7 downto 0) := x"00";
		variable IP_length		: unsigned(15 downto 0) := x"00_00";
		variable type_field		: std_logic_vector(15 downto 0) := x"00_00";
		variable IP_detected	: boolean := false;
		variable UDP_detected	: boolean := false;

	begin
		if rising_edge(clk) then
			if (rst = '1') then
				ram_addr := x"00";
				w_flag_enet <= '0';
				w_addr_enet <= x"00";
				IP_length := x"00_00";
				type_field := x"00_00";
				IP_detected := false;
				UDP_detected := false;
			elsif rx_count > 0 then
			-- cannot check rx_enable as it will be of down for the last <8 bytes
			
				case rx_count is -- we suppose a UDP-paket
					when 6 => -- DST-@
						w_addr_enet <= x"01";
						w_flag_enet <= '1';

						w_data_enet(63 downto 56) <= char_to_byte('D');
						w_data_enet(55 downto 48) <= char_to_byte(':');
						w_data_enet(47 downto 40) <= rx_data_sr_content(5);
						w_data_enet(39 downto 32) <= rx_data_sr_content(4);
						w_data_enet(31 downto 24) <= rx_data_sr_content(3);
						w_data_enet(23 downto 16) <= rx_data_sr_content(2);
						w_data_enet(15 downto  8) <= rx_data_sr_content(1);
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0);

						IP_length := x"00_00";
					when 12 => -- SRC-@
						w_addr_enet <= x"02";
						w_flag_enet <= '1';

						w_data_enet(63 downto 56) <= char_to_byte('S');
						w_data_enet(55 downto 48) <= char_to_byte(':');
						w_data_enet(47 downto 40) <= rx_data_sr_content(5);
						w_data_enet(39 downto 32) <= rx_data_sr_content(4);
						w_data_enet(31 downto 24) <= rx_data_sr_content(3);
						w_data_enet(23 downto 16) <= rx_data_sr_content(2);
						w_data_enet(15 downto  8) <= rx_data_sr_content(1);
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0);
					when 14 => -- Type field
						type_field(15 downto 8) := rx_data_sr_content(1);
						type_field( 7 downto 0) := rx_data_sr_content(0);
						case type_field is
							when x"81_00" => -- VLAN-tag
								IP_detected := false;
							when x"08_00" => -- IPv4
								IP_detected := true;
							when others => -- who knows...
								IP_detected := false;
						end case;
					when 22 => -- IP header part I/III
						w_addr_enet <= x"04";
						w_flag_enet <= '1';

						w_data_enet(63 downto 56) <= rx_data_sr_content(7); -- IHL
						w_data_enet(55 downto 48) <= rx_data_sr_content(6); -- TOS
						
						IP_length(15 downto 8) := unsigned(rx_data_sr_content(5)); -- Total Length  I/II
						IP_length( 7 downto 0) := unsigned(rx_data_sr_content(4)); -- Total Length  I/II
						
						w_data_enet(47 downto 40) <= std_logic_vector(IP_length(15 downto 8));
						w_data_enet(39 downto 32) <= std_logic_vector(IP_length( 7 downto 0));
						
						w_data_enet(31 downto 24) <= rx_data_sr_content(3); -- ID  I/II
						w_data_enet(23 downto 16) <= rx_data_sr_content(2); -- ID II/II
						w_data_enet(15 downto  8) <= rx_data_sr_content(1); -- Frag  I/II
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0); -- Frag II/II
					when 26 => -- IP header part II/III
						w_addr_enet <= x"05";
						w_flag_enet <= '1';

						w_data_enet(63 downto 32) <= (others => '-');
						w_data_enet(31 downto 24) <= rx_data_sr_content(3); -- TTL
						w_data_enet(23 downto 16) <= rx_data_sr_content(2); -- Protocol
						w_data_enet(15 downto  8) <= rx_data_sr_content(1); -- CRC  I/II
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0); -- CRC II/II
						case rx_data_sr_content(2) is
							when x"11" => -- UDP: all right and proceed
								UDP_detected := true;
							when others =>
								UDP_detected := false;
						end case;
					when 28 => -- save UDP length and type field
						w_addr_enet <= x"03";
						w_flag_enet <= '1';
						if (IP_detected and UDP_detected) then -- give correct UDP_data_length
							w_data_enet(63 downto 48) <= std_logic_vector(IP_length - x"00_14");
						else -- no UDP_packet: no valid length
							w_data_enet(63 downto 48) <= x"00_08";
						end if;
						w_data_enet(47 downto 16) <= (others => '-');
						w_data_enet(15 downto  0) <= type_field;
					when 34 => -- IP header part III/III
						w_addr_enet <= x"06";
						w_flag_enet <= '1';

						w_data_enet(63 downto 56) <= rx_data_sr_content(7); -- dst-IP-@ I
						w_data_enet(55 downto 48) <= rx_data_sr_content(6); -- dst-IP-@ II
						w_data_enet(47 downto 40) <= rx_data_sr_content(5); -- dst-IP-@ III
						w_data_enet(39 downto 32) <= rx_data_sr_content(4); -- dst-IP-@ IV
						w_data_enet(31 downto 24) <= rx_data_sr_content(3); -- src-IP-@ I
						w_data_enet(23 downto 16) <= rx_data_sr_content(2); -- src-IP-@ II
						w_data_enet(15 downto  8) <= rx_data_sr_content(1); -- src-IP-@ III
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0); -- src-IP-@ IV
					when 42 => -- UDP header I/I
						w_addr_enet <= x"07";
						w_flag_enet <= '1';

						w_data_enet(63 downto 56) <= rx_data_sr_content(7); -- src-UDP-@ I
						w_data_enet(55 downto 48) <= rx_data_sr_content(6); -- src-UDP-@ II
						w_data_enet(47 downto 40) <= rx_data_sr_content(5); -- dst-UDP-@ I
						w_data_enet(39 downto 32) <= rx_data_sr_content(4); -- dst-UDP-@ II
						w_data_enet(31 downto 24) <= rx_data_sr_content(3); -- UDP-length I
						w_data_enet(23 downto 16) <= rx_data_sr_content(2); -- UDP-length II
						w_data_enet(15 downto  8) <= rx_data_sr_content(1); -- UDP-CRC I
						w_data_enet( 7 downto  0) <= rx_data_sr_content(0); -- UDP-CRC II
						
						ram_addr := x"08";
						
					when 50 to max_frame_length => -- UDP data
						--	have 8 further bytes entered the shift register?
						if (rx_count - 50) mod 8 = 0 then
							w_addr_enet <= std_logic_vector(ram_addr);
							w_flag_enet <= '1';
							
							w_data_enet(63 downto 56) <= rx_data_sr_content(7);
							w_data_enet(55 downto 48) <= rx_data_sr_content(6);
							w_data_enet(47 downto 40) <= rx_data_sr_content(5);
							w_data_enet(39 downto 32) <= rx_data_sr_content(4);
							w_data_enet(31 downto 24) <= rx_data_sr_content(3);
							w_data_enet(23 downto 16) <= rx_data_sr_content(2);
							w_data_enet(15 downto  8) <= rx_data_sr_content(1);
							w_data_enet( 7 downto  0) <= rx_data_sr_content(0);
							
							ram_addr := ram_addr + x"01";
						else
							w_flag_enet <= '0';
						end if;
					when others =>
						w_flag_enet <= '0';
				end case;
			else
				w_flag_enet <= '0';
			end if;
		end if;
	end process;
end Behavioral;
