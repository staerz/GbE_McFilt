----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		11:01:48 08/13/2010 
--
-- Module Name:		UDP_manager - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--	The module will treat UDP-data transfer from and to RAM.
--
--	Only well formatted incomming packets are accepted:
--		calibration packets have to wear calib_signature - see my_definitions
--		calculation packets have to wear mcfilt_signature - see my_definitions
--	Depending on the kind of packet either calib_start or calc_start is pulled
--	up for 1 clk so the following module can treat RAM data
--
--	Data sending will be done on send_frame_flag only. The number of bytes
--	to be sent is calculated from UDP_bytes (UDP_len) read from the corresponding
--	RAM entry (see table below).
--
--	The data is organised in RAM as shown:
--
--        RAM-@ | content (63 downto 0) in bytes: -- == don't care (N/A)|
--        ------|------'------'------'------'------'------'------'------|
--         00   |  --  '  --  '  --  '  --  '  --  '  --  '  --  '  --  |
--         01   |  "D" '  " " ' 6 by_'__tes_'_DST__'_MAC__'_addr_'_ess  |
--         02   |  "S" '  " " ' 6 by_'__tes_'_SRC__'_MAC__'_addr_'_ess  |
--         03   |  UDP_'_len  '  --  '  --  '  --  '  --  '  Ty__'__pe  |
--         04   |  IHL '  TOS 'Total_'_Len  '  Id_ ' _ent ' Frag_'_ment |
--         05   |  --  '  --  '  --  '  --  '  TTL ' Prot '  CRC_'_CRC  |
--         06   |  IP-_'_SRC__'_addr_'_ess  '  IP-_'_DST__'_addr_'_ess  |
--         07   | UDP-_'_head_'_er:__'_see__'_UDP__'_docu_'_ment_'_ation|
--         -----|------'------'------'------'------'------'------'------|
--         08   | data_'_data_'_data_'_data_'_data_'_data_'_data_'_data |
--         ..   |  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  |
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UDP_manager is
	port (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		rx_data_in			: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n			: in  std_logic; -- Input start of frame
		rx_eof_in_n			: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready
		tx_data_out			: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n	: in  std_logic; -- Input destination ready	-- check

--	communication ports UDP module <-> LCD module
		capture_src_flag	: out std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: out std_logic;	-- DST MAC @ was found
		mac_readout_flag	: in std_logic;	-- MACs have been read out

--	RAM read requests of UDP-module:
		r_flag_enet			: out std_logic;
		r_addr_enet			: out std_logic_vector(7 downto 0);
		r_fine_enet			: in std_logic;
		r_data				: in std_logic_vector(63 downto 0);	-- out read data

--	RAM write requests of UDP-module:
		w_flag_enet			: out std_logic;
		w_addr_enet			: out std_logic_vector(7 downto 0);
		w_data_enet			: out std_logic_vector(63 downto 0);
		w_fine_enet			: in std_logic;
		
--	communication UDP module <-> Calibration module
		calib_start			: out std_logic;
		calib_enable		: in  std_logic;

--	communication UDP module <-> McFilt module
		calc_start			: out std_logic;
		calc_enable			: in  std_logic;

--	external stimuli
		send_frame_flag		: in std_logic
	);
end UDP_manager;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;


architecture Behavioral of UDP_manager is

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--      signals of receive and transmit process                               --
--                                                                            --
--------------------------------------------------------------------------------
   
	component UDP_rx_module is
	port  (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		rx_data_in			: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n			: in  std_logic; -- Input start of frame
		rx_eof_in_n			: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready

--	Receiving counter for outer module
		rx_counter			: out std_logic_vector(10 downto 0);

--	communication UDP module <-> LCD module
		capture_src_flag	: out std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: out std_logic;	-- DST MAC @ was found
		mac_readout_flag	: in  std_logic;	-- MACs have been read out

--	communication UDP module <-> Calibration module
		calib_start			: out std_logic;
		calib_enable		: in  std_logic;

--	communication UDP module <-> McFilt module
		calc_start			: out std_logic;
		calc_enable			: in  std_logic;

--	RAM write requests from UDP-module:
		w_flag_enet			: out std_logic;
		w_addr_enet			: out std_logic_vector(7 downto 0);
		w_data_enet			: out std_logic_vector(63 downto 0);
		w_fine_enet			: in  std_logic
	);
	end component;

	component UDP_tx_module is
	port (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		tx_data_out			: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n		: in  std_logic; -- Input destination ready	-- check

--	Sending counter for outer module
		tx_counter			: out std_logic_vector(10 downto 0);

--	external stimuli
		send_frame_flag		: in std_logic;	-- start frame signal

--	RAM read requests of UDP-module:
		r_flag_enet			: out std_logic;
		r_addr_enet			: out std_logic_vector(7 downto 0);
		r_fine_enet			: in std_logic;
		r_data				: in std_logic_vector(63 downto 0)
	);
	end component;

begin

	UDP_rx : UDP_rx_module
	port map (
--	Ethernet clocking
		clk					=> clk,
		rst					=> rst,

--	Ethernet data + controls
		rx_data_in			=> rx_data_in,	-- Input data
		rx_sof_in_n			=> rx_sof_in_n,	-- Input start of frame
		rx_eof_in_n			=> rx_eof_in_n,	-- Input end of frame
		rx_src_rdy_in_n 	=> rx_src_rdy_in_n,	-- Input source ready

--	Receiving counter for outer module
		rx_counter			=> open,

--	communication UDP module <-> LCD module
		capture_src_flag	=> capture_src_flag,
		capture_dst_flag	=> capture_dst_flag,
		mac_readout_flag	=> mac_readout_flag,

--	communication UDP module <-> Calibration module
		calib_start			=> calib_start,
		calib_enable		=> calib_enable,

--	communication UDP module <-> McFilt module
		calc_start			=> calc_start,
		calc_enable			=> calc_enable,

--	communication UDP module <-> Calibration module

--	RAM write requests from UDP-module:
		w_flag_enet			=> w_flag_enet,
		w_addr_enet			=> w_addr_enet,
		w_data_enet			=> w_data_enet,
		w_fine_enet			=> w_fine_enet
	);

	UDP_tx : UDP_tx_module
	port map (
--	Ethernet clocking
		clk					=> clk,
		rst					=> rst,

--	Ethernet data + controls
		tx_data_out			=> tx_data_out,	-- Modified output data
		tx_sof_out_n		=> tx_sof_out_n,	-- Output start of frame						
		tx_eof_out_n		=> tx_eof_out_n,	-- Output end of frame							
		tx_src_rdy_out_n	=> tx_src_rdy_out_n,	-- Output source ready
		tx_dst_rdy_in_n		=> tx_dst_rdy_in_n,	-- Input destination ready

--	Sending counter for outer module
		tx_counter			=> open,

--	external stimuli
		send_frame_flag		=> send_frame_flag,-- start frame signal

--	RAM read requests from UDP-module:
		r_flag_enet			=> r_flag_enet,
		r_addr_enet			=> r_addr_enet,
		r_fine_enet			=> r_fine_enet,
		r_data				=> r_data
	);

end Behavioral;

