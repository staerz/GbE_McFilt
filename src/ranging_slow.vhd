----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			Technical University of Dresden, Germany
-- 					Institute of Physics of Nuclei and Particle Physics
-- 
-- Create Date:		10:05 21/07/2010 
--
-- Module Name:		ranging - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.5
--
-- Description:		This module creates a 16 bit range specific output from a
--					xx bit input
--	basic idea:
--	16 valid bits for output stream:
--	2 bits range encryption, 1 bit sign, 13 bits value (positive or negative)
--	ranges (for positive values only):
--		00: msb of value is within (13+0*shift) bit range -> no shift required
--		01: msb of value is within (13+1*shift) bit range -> shift once for SHIFT
--		10: msb of value is within (13+2*shift) bit range -> shift twice for SHIFT
--		11: msb of value is within (13+3*shift) bit range -> shift 3times for SHIFT
--	exceptions for "out of range":
--		- negative shifted values are physically not expected, but if they occure:
--			range == 01, sign == 1, value == any lower 13 bits of input
--		- values greater than max shift range are physically not expected, but if:
--			range == 10, sign == 1, value == any lower 13 bits of input
--		- invalid data will we tagged as the following:
--			range == 11, sign == 1, value == any lower 13 bits of input
--
-- Dependencies:	none
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library my_definitions;
use my_definitions.mytypes.all;

entity ranging_slow is
	port (
--	controls
		clk 	: in  std_logic;
		rst 	: in  std_logic;
		en  	: in  std_logic;

--	unranged input
		Input	: in  std_logic_vector(47 downto LOWESTBIT);
--	invalidity flag of input
		inval	: in  std_logic;
--	ranged output
		Output	: out std_logic_vector(15 downto 0)
	);
end ranging_slow;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of ranging_slow is

	signal sign : std_logic;
	signal notinrange : std_logic;
	signal intherange : std_logic_vector(1 downto 0);
	signal outtherange : std_logic_vector(1 downto 0);
	signal prefix : std_logic_vector(3*SHIFT downto 1);
	signal prefix3dt2 : std_logic_vector(SHIFT downto 1);
	signal preval : std_logic_vector(12 downto 0);

begin

	sign <= Input(Input'left);
	prefix <= Input(LOWESTBIT+12+3*SHIFT downto LOWESTBIT+13+0*SHIFT);
	-- high level decision if in range or not
	notinrange <= '0' when 0 = unsigned(not(Input(Input'left downto LOWESTBIT+13))) -- not too negative
			or 0 = unsigned(Input(Input'left downto LOWESTBIT+13+3*SHIFT)) -- not too positive
			else '1';
			
	prefix3dt2 <= prefix(3*SHIFT downto 1+2*SHIFT);

	intherange <=
		"00" when sign = '1' else
		"11" when 0 /= unsigned(prefix3dt2) else
		"10" when 0  = unsigned(prefix3dt2) and
				  0 /= unsigned(prefix(2*SHIFT downto 1+1*SHIFT)) else
		"01" when 0  = unsigned(prefix(3*SHIFT downto 1+1*SHIFT)) and
				  0 /= unsigned(prefix(1*SHIFT downto 1+0*SHIFT)) else
		"00";

	outtherange <= (not sign & sign) or (inval & inval);

--	process(clk)
--	begin
--		if rising_edge(clk) then
--			if rst = '1' then
--				Output(12 downto 0) <= (others => '-');
--				Output(15 downto 13) <= (others => '1');
--			elsif en = '1' then
--				case intherange is
--					when "11" => output(12 downto 0) <= Input(LOWESTBIT+12+3*SHIFT downto LOWESTBIT+3*SHIFT);
--					when "10" => output(12 downto 0) <= Input(LOWESTBIT+12+2*SHIFT downto LOWESTBIT+2*SHIFT);
--					when "01" => output(12 downto 0) <= Input(LOWESTBIT+12+1*SHIFT downto LOWESTBIT+1*SHIFT);
--					when others => output(12 downto 0) <= Input(LOWESTBIT+12+0*SHIFT downto LOWESTBIT+0*SHIFT);
--				end case;
--				
--				if (notinrange nor inval) = '1' then
--					Output(15 downto 13) <=	intherange & sign;
--				else
--					Output(15 downto 13) <= outtherange & '1';
--				end if;
--			end if;
--		end if;
--	end process;

	with intherange select preval <=
		Input(LOWESTBIT+12+3*SHIFT downto LOWESTBIT+3*SHIFT) when "11",
		Input(LOWESTBIT+12+2*SHIFT downto LOWESTBIT+2*SHIFT) when "10",
		Input(LOWESTBIT+12+1*SHIFT downto LOWESTBIT+1*SHIFT) when "01",
		Input(LOWESTBIT+12+0*SHIFT downto LOWESTBIT+0*SHIFT) when others;

	with notinrange nor inval select Output(15 downto 13) <=
		intherange & sign when '1',
		outtherange & '1' when others;
	
	Output(12 downto 0) <= preval;

end Behavioral;

