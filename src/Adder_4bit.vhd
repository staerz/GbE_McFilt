----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			IKTP TU Dresden	
-- 
-- Create Date:		15:59:00 14/04/2010 
--
-- Module Name:		Adder_4bit - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		4 Bit Full Adder with Carry Out
--
-- Dependencies:	Using 
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder_4bit is
	port(
		a_in	: in std_logic_vector (3 downto 0);
		b_in	: in std_logic_vector (3 downto 0);
		c_in	: in std_logic;
		s_out	: out std_logic_vector (3 downto 0);
		c_out	: out std_logic
	);
end Adder_4bit;

architecture Behavioral of Adder_4bit is

	component FullAdder is
--	component realising the FullAdder
		port(
			in1, in2, incarry : in std_logic;
			sum, carry : out std_logic
		);
	end component;

	signal carry : std_logic_vector (3 downto 1);

begin

--	composition of 4-Bit-Adder using 4 single Bit-FullAdders
	bit1 : FullAdder
	port map (
		a_in(0), b_in(0), c_in, s_out(0), carry(1)
	);
	
	bit2 : Fulladder
	port map (
		a_in(1), b_in(1), carry(1), s_out(1), carry(2)
	);

	bit3 : Fulladder
	port map (
		a_in(2), b_in(2), carry(2), s_out(2), carry(3)
	);

	bit4 : Fulladder
	port map (
		a_in(3), b_in(3), carry(3), s_out(3), c_out
	);


end Behavioral;