library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package mytypes is

--BEGIN function declarations

-- now already in myports as log2ceilp!
	-- external: calculate # of bits to encode number
	function log2ceil(arg : positive) return natural;
	-- external: select max of arg1 and arg2
	function sel_abits(arg1, arg2 : positive) return positive;
	
	-- internal: set # of bytes of calibration packet
	function sel_coeff_bytes return signed;
--END function declarations

--BEGIN clocking declarations: filter frequency
	constant clk_mult	: positive := 7; -- clock multiplicator for PLL with 100MHz clk input
	constant clk_div	: positive := 2; -- clock divider for PLL with 100MHz clk input
	-- filter frequency = 100MHz * clk_mult / clk_div
--END clocking declarations

--BEGIN LCD declarations
--	constant one_sec : integer := 50000000; -- 50 MHz: 50M == 1s
--	constant one_sec : integer := 125000000; -- 125 MHz: 125M == 1 Second
	constant milisec : integer := 50000; -- 50MHz: 50k == 1 ms
----	SIMULATION:
--	constant milisec : integer := 5;
--	constant one_microsec : integer := 50;

--	constant DISPLAYDELAY : positive := milisec;
	constant DISPLAYDELAY : positive := 125*milisec;
--END LCD declaration

--BEGIN Filter declarations
	-- fundamental filter settings
	constant CHANNELS	: positive := 4; -- number of channels for 1 DSP-set
	constant DEPTH		: positive := 5; -- number of DSPs in 1 DSP-set (= filter depth)
	-- output formatting (ranging)
	constant SHIFT		: positive := 2; -- 3 in normal case
	constant LOWESTBIT	: positive := 1; -- LSB of Sum of Filter result: 0 < LSB < 47-15-3*SHIFT = 23

	constant PED_SIZE	: positive := 16; -- multiple of 8! -- bit width of pedestal value in LUT
	constant A_I_SIZE	: positive := 24; -- multiple of 8! -- bit width of ai coefficients in LUT

	constant DSPDELAY	: positive := 3; -- delay caused by the DSP multiplication
	
	constant GAIN_COUNT : positive := 4; -- number of possible valid gain values plus 1 for invalid gain
	
	constant GAIN_BIT	: positive := 15; -- bit position of upper gain bit in energy (in input value)
	constant X_GAIN_BIT	: positive := 14; -- bit position of upper gain bit in energy (in calculation)
	constant E_BIT		: positive := 11; -- max 11

	-- McFilt Manager
	constant E_delay	: positive := 1; -- number of registers introduced on fifo input side
	constant S_delay	: positive := 1; -- number of registers introduced on fifo output side

	-- derived constants
	constant CHANNEL_WIDTH	: positive := log2ceil(CHANNELS); -- # of bits to encrypt CHANNELS bitwise
	constant GAIN_WIDTH	: positive := log2ceil(GAIN_COUNT); -- # of bits to address gain values
	constant GAIN_BUT	: positive := GAIN_BIT - GAIN_WIDTH + 1; -- bit position of lower gain bit (in input value)
	constant X_GAIN_BUT	: positive := X_GAIN_BIT - GAIN_WIDTH + 1; -- bit position of lower gain bit (in calculation)
--END Filter declarations

--BEGIN UDP manager declarations
	constant calib_signature	: std_logic_vector(7 downto 0) := x"FF";
	constant mcfilt_signature	: std_logic_vector(7 downto 0) := x"12";
	constant max_frame_length	: integer := 1600;
--END UDP manager declarations

--BEGIN RAM management declarations
	constant ram_data_start		: unsigned(7 downto 0) := x"08";--@ in RAM where data begins
	constant ram_coeff_bytes	: signed(10 downto 0) := sel_coeff_bytes; -- # of calibration bytes
	constant ram_E_samples		: signed(10 downto 0) := to_signed(40,11);-- # of samples for filter
--END RAM management declarations

--BEGIN simulation declarations
	type tEnergies is array(1 to 10) of std_logic_vector(15 downto 0);
	type tDATA is array(0 to CHANNELS-1) of tEnergies;
	--predefined samples: 4 channels � 10 samples
	constant DATA_init_values : tDATA := 
	(
		(x"4023", x"4030", x"C003", x"4012", x"4007", x"800a", x"c023", x"C01c", x"400a", x"400b"),
		(x"800c", x"8034", x"8034", x"c01c", x"802a", x"400a", x"4003", x"402e", x"801b", x"8035"),
--		(x"800c", x"8034", x"8034", x"c01c", x"802a", x"400a", x"4003", x"402e", x"801b", x"8035"),
--		(x"800c", x"8034", x"8034", x"c01c", x"802a", x"400a", x"4003", x"402e", x"801b", x"8035"),
		(x"88de", --x"412a",
				  x"0000",
						   x"ca52", x"843c", x"c3d7", x"c2de", x"c53c", x"c29d", x"89be", x"ca3c"),
		(x"c84b", x"c61c", x"C456", x"40a3", x"c5cb", x"8a35", x"446f", x"81e5", x"421c", x"c2b7")
	);

	type tIPheader is array(1 to 42) of std_logic_vector(7 downto 0);
	constant IPheader_data : tIPheader :=
	(
		x"AA", x"BB", x"CC", x"DD", x"EE", x"FF",-- DST MAC
		x"00", x"26", x"18", x"F0", x"55", x"E0",-- SRC MAC
		x"08", x"00",-- Protocol: IP
		x"45", x"00", x"00", x"6C",--Version, IHL, TOS, Total Length
		x"12", x"12", x"40", x"00",--Ident, Flags, Fragment
		x"40", x"11", x"B0", x"26",--TTL, Protocol: UDP, Checksum
		x"C0", x"A8", x"7B", x"7B",--IP SRC
		x"C0", x"A8", x"7B", x"7C",--IP DST
		x"00", x"00", x"00", x"00", x"00", x"58", x"DC", x"53"--UDP header: SRC, DST, Length, Checksum
	);

	constant IPheader_calib : tIPheader :=
	(
		x"AA", x"BB", x"CC", x"DD", x"EE", x"FF",-- DST MAC
		x"00", x"26", x"18", x"F0", x"55", x"E0",-- SRC MAC
		x"08", x"00",-- Protocol: IP
		x"45", x"00", x"02", x"3C",--Version, IHL, TOS, Total Length
		x"12", x"ff", x"40", x"00",--Ident, Flags, Fragment
		x"40", x"11", x"ad", x"69",--TTL, Protocol: UDP, Checksum
		x"C0", x"A8", x"7B", x"7B",--IP SRC
		x"C0", x"A8", x"7B", x"7C",--IP DST
		x"00", x"00", x"00", x"00", x"02", x"28", x"4f", x"bc"--UDP header: SRC, DST, Length, Checksum
	);

--------------------------------------------------------------------------------
--
--	bitwise definition of coefftable:
--
--	----------------------------------------------------
--	|9 ..8|77|7 ..6|66|6 ..4|44|4 ..3|33|2 ..1|11|1 ..0| -> tens
--	|5 ..0|98|7 ..4|32|1 ..8|76|5 ..2|10|9 ..6|54|3 ..1| -> ones
--	|-----|--|-----|--|-----|--|-----|--|-----|--|-----|
--	| ped |sc|..a5.|sc|..a4.|sc|..a3.|sc|..a2.|sc|..a1.| -> sc=scale
--	|15..0|98|13..0|76|13..0|54|13..0|32|13..0|10|13..0| -> bit #
--	----------------------------------------------------
--
--------------------------------------------------------------------------------

	type tfullcoeffset is array (1 to 3) of std_logic_vector(PED_SIZE + DEPTH*A_I_SIZE - 1 downto 0);
	type tfullcoefftable is array (0 to CHANNELS-1) of tfullcoeffset;

	--predefined coefficients: 4 channels � 3 gains (gain 4 = everything 0)
	constant coeff_init_values : tfullcoefftable :=
	(
	 (
--	  pedestal  &    a1     &     a2    &   a3      &    a4     &     a5
		x"FFFF" & x"000004" & x"ffFFFF" & x"000012" & x"000055" & x"000083",--channel 0, gain 1
		x"FFFE" & x"000003" & x"ffFFFD" & x"0000d2" & x"000035" & x"0000c3",--channel 0, gain 2
		x"FFFD" & x"000002" & x"ffFFFE" & x"000072" & x"000015" & x"000013" --channel 0, gain 3
	),
	(
		x"0000" & x"000006" & x"ffFFCF" & x"000042" & x"000025" & x"000021",--channel 1, gain 1
		x"0001" & x"000005" & x"ffFFAF" & x"000012" & x"000045" & x"000023",--channel 1, gain 2
		x"0002" & x"000001" & x"ffFFBF" & x"000008" & x"000035" & x"000013" --channel 1, gain 3
	),
	(
		x"0003" & x"000045" & x"ffFF1F" & x"000012" & x"0000c5" & x"00002e",--channel 2, gain 1
		x"0042" & x"000034" & x"ffFF2F" & x"000023" & x"0000a5" & x"00002d",--channel 2, gain 2
		x"0004" & x"000023" & x"ffFF3F" & x"000021" & x"0000b5" & x"00002b" --channel 2, gain 3
	),
	(
		x"0046" & x"000045" & x"ffFF4F" & x"000066" & x"000012" & x"00001a",--channel 3, gain 1
		x"0076" & x"000024" & x"ffFFC3" & x"000052" & x"000035" & x"00001b",--channel 3, gain 2
		x"0023" & x"000011" & x"ffFF26" & x"000041" & x"000045" & x"00001c" --channel 3, gain 3
	 )
	);
--END simulation declarations

end mytypes;

package body mytypes is
-- now already in myports as log2ceilp
--	calculate the number of bits required to encryp arg binarily
	function log2ceil(arg : positive) return natural is
		variable tmp : positive;
		variable log : natural;
	begin
		tmp := 1;
		log := 0;
		-- satisfy synthesis tools: prevent warnings about loop iterations
		if arg > 1 then
			while arg > tmp loop
				tmp := tmp * 2;
				log := log + 1;
			end loop;
		end if;

		return log;
	end;

--	select the maximum of 2 arguments
	function sel_abits(arg1, arg2: positive) return positive is
	begin
		if arg1 < arg2 then
			return arg2;
		else
			return arg1;
		end if;
	end;

--	set the number of bytes used for the storage of the coefficients
	function sel_coeff_bytes return signed is
	begin
		assert CHANNELS * GAIN_COUNT < 64
		report "Too many CHANNELS or GAINs. Only CHANNELS * GAIN_COUNT < 64 allowed!"
		severity failure;

		if CHANNELS * GAIN_COUNT < 32 then
			return to_signed((PED_SIZE + A_I_SIZE*DEPTH)/8 * 32,11);
		elsif CHANNELS * GAIN_COUNT < 64 then
			return to_signed((PED_SIZE + A_I_SIZE*DEPTH)/8 * 64,11);
		else
			return to_signed(0,11);
		end if;
	end;

end mytypes;