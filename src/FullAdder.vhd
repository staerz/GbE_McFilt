----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			IKTP TU Dresden	
-- 
-- Create Date:		15:59:00 14/04/2010 
--
-- Module Name:		Fulladder - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		1 Bit Full Adder with Carry Out
--
-- Dependencies:	Using 
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder is
	port(
		in1, in2, incarry : in std_logic;
		sum, carry : out std_logic
	);
end FullAdder;

architecture STD of FullAdder is
	signal w_sum, w_carry1, w_carry2 : std_logic;
	
	component halfadder
		port (
			in1, in2:	in std_logic;
			sum, carry:	out std_logic
		);
	end component;
	
	component orgate
		port(
			a, b : in std_logic;
			res : out std_logic
		);
	end component;

begin
	module1 : halfadder
		port map ( in1, in2, w_sum, w_carry1 );
	
	module2 : halfadder
		port map ( w_sum, incarry, sum, w_carry2 );

	module3 : orgate
		port map ( w_carry1, w_carry2, carry );

end STD;