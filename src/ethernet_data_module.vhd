----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		10:48:41 12/07/2010
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--   - Saves incomming ethernet data stream to RAM:
--      UDP/IP-header (see UDP/IP-documentation) and UDP-data
--      CRC of IP-HEADER is calculated (it is mandatory)
--      CRC of UDP PACKET is NOT calculated (it's not mandatory, so constant 0)
--        RAM-@ | content (63 downto 0) in bytes: -- == don't care      |
--        ------|------'------'------'------'------'------'------'------|
--         00   |  --  '  --  '  --  '  --  '  --  '  --  '  --  '  --  |
--         01   |  "D" '  " " ' 6 by_'__tes_'_DST__'_MAC__'_addr_'_ess  |
--         02   |  "S" '  " " ' 6 by_'__tes_'_SRC__'_MAC__'_addr_'_ess  |
--         03   |  UDP_'_len  '  --  '  --  '  --  '  --  '  Ty__'__pe  |
--         04   |  IHL '  TOS 'Total_'_Len  '  Id_ ' _ent ' Frag_'_ment |
--         05   |  --  '  --  '  --  '  --  '  TTL ' Prot '  CRC_'_CRC  |
--         06   |  IP-_'_SRC__'_addr_'_ess  '  IP-_'_DST__'_addr_'_ess  |
--         07   | UDP-_'_head_'_er:__'_see__'_UDP__'_docu_'_ment_'_ation|
--         -----|------'------'------'------'------'------'------'------|
--         08   | data_'_data_'_data_'_data_'_data_'_data_'_data_'_data |
--         ..   |  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  '  ..  |
--
--   - sends UDP/IP packet from RAM to output: on send_frame_flag only
--      RAM-content is evaluated:
--        header taken from RAM:
--          DST-MAC (SRC-MAC is fixed so far)
--          UDP-length: evaluated to be more than 0
--          DST-IP-address; SCR-IP-address
--   - receive and send use 8 stage shift register to minimise RAM access
--     (only each 8th clock cycle RAM is accessed when sending data part)
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ethernet_data_module is
	port (
--	global user reset
		user_reset			: out std_logic; -- global user reset

--	CLOCKING:
	--	clock running
		clk_locked			: in std_logic; -- high = clock running

	--	Ethernet clocking
		rtx_clk				: in  std_logic; -- Input CLK from TRIMAC Reciever
		rtx_rst				: in  std_logic; -- Synchronous reset signal

	--	LCD clocking
		clk_50MHz			: in  std_logic; -- Input CLK 50MHz

	--	calculation clocking
		clk_mcfilt			: in  std_logic; -- Input high speed CLK (approx. 400MHz)

--	Ethernet data + controls
		rx_data_in			: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n			: in  std_logic; -- Input start of frame
		rx_eof_in_n			: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready
		tx_data_out			: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n		: in  std_logic; -- Input destination ready	-- check

--	LCD interface
		lcd_e				: out std_logic;	-- LCD enable
		lcd_rs				: out std_logic;	-- LCD control
		lcd_rw				: out std_logic;	-- LCD control
		lcd_data			: inout std_logic_vector(7 downto 4);	-- LCD data

--	LEDs
		leds_nwces			: out std_logic_vector(4 downto 0);
		leds8				: out std_logic_vector(0 to 7);

--	user push buttons
		switches_nwces		: in std_logic_vector(4 downto 0)
	);
end ethernet_data_module;

library my_definitions;
use my_definitions.mytypes.all;
use my_definitions.myports.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture arch1 of ethernet_data_module is

	signal reset				: std_logic := '0';

--	component debounce is
--------------------------------  <-  80 chars  ->  ------------------------------
----                                                                            --
----         Debouncing module                                                  --
----                                                                            --
----  Required Input:	signal to be debounced + clock signal                     --
----                                                                            --
----  Given Output:	debounced signal (1 cycle later)                          --
----                                                                            --
----------------------------------------------------------------------------------
--	Port(
--		sig_in	: in std_logic;
--		clk		: in std_logic;
--		sig_out	: out std_logic
--	);
--	end component;

--	communication ports LCD <-> Enet module
	signal capture_src_flag	: std_logic;	-- SRC MAC @ was found
	signal capture_dst_flag	: std_logic;	-- DST MAC @ was found
	signal mac_readout_flag		: std_logic;	-- MACs have been read out

	signal calib_start			: std_logic;
	signal calib_enable			: std_logic;

	signal calc_start			: std_logic;
	signal calc_enable			: std_logic;

	signal send_frame_flag		: std_logic;	-- start frame signal

	component UDP_manager is
	port (
--	Ethernet clocking
		clk					: in  std_logic; -- Input CLK from TRIMAC Reciever
		rst					: in  std_logic; -- Synchronous reset signal

--	Ethernet data + controls
		rx_data_in		: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n		: in  std_logic; -- Input start of frame
		rx_eof_in_n		: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready
		tx_data_out		: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n	: in  std_logic; -- Input destination ready	-- check

--	communication ports UDP module <-> LCD module
		capture_src_flag	: out std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: out std_logic;	-- DST MAC @ was found
		mac_readout_flag	: in std_logic;	-- MACs have been read out

--	RAM read requests of UDP-module:
		r_flag_enet			: out std_logic;
		r_addr_enet			: out std_logic_vector(7 downto 0);
		r_fine_enet			: in std_logic;
		r_data				: in std_logic_vector(63 downto 0);	-- out read data

--	RAM write requests of UDP-module:
		w_flag_enet			: out std_logic;
		w_addr_enet			: out std_logic_vector(7 downto 0);
		w_data_enet			: out std_logic_vector(63 downto 0);
		w_fine_enet			: in std_logic;
		
--	communication UDP module <-> Calibration module
		calib_start			: out std_logic;
		calib_enable		: in  std_logic;

--	communication UDP module <-> McFilt module
		calc_start			: out std_logic;
		calc_enable			: in  std_logic;

--	external stimuli
		send_frame_flag		: in std_logic
	);
	end component;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         energy calculation process variables                               --
--                                                                            --
--------------------------------------------------------------------------------
	signal mcfilt_enable	: std_logic := '1';
	signal mcfilt_reset 	: std_logic := '0';

--	Energy to be put into calculation
	signal E				: std_logic_vector(15 downto 0) := (others => '-');
--	Result from calculation
	signal S				: std_logic_vector(47 downto LOWESTBIT);
	signal inval			: std_logic;
	signal c_valid			: std_logic;
	
	component McFilt_manager is
	generic (
		E_samples		: signed(10 downto 0);
		LOWESTBIT		: positive
	);
	port (
--	clocking
		clk_125MHz		: in  std_logic;
		clk_mcfilt		: in  std_logic;
		rst				: in  std_logic;

--	communication McFilt-manager -> McFilt
		mcfilt_enable	: out std_logic;
		mcfilt_reset	: out std_logic;

--	RAM read requests of McFilt manager:
		r_flag_mcfilt	: out std_logic;
		r_addr_mcfilt	: out std_logic_vector(7 downto 0);
		r_fine_mcfilt	: in  std_logic;
		r_data			: in  std_logic_vector(63 downto 0);

--	RAM write requests of McFilt manager:
		w_flag_mcfilt	: out std_logic;
		w_addr_mcfilt	: out std_logic_vector(7 downto 0);
		w_data_mcfilt	: out std_logic_vector(63 downto 0);
		w_fine_mcfilt	: in  std_logic;

--	external stimuli
		send_frame_flag	: out std_logic;

--	communication McFilt manager <-> UDP module
		calc_start		: in  std_logic;
		calc_enable		: out std_logic;

--	Energy to be put into McFilt
		E				: out std_logic_vector(15 downto 0);
--	Result from McFilt
		S				: in  std_logic_vector(47 downto LOWESTBIT);
		inval			: in  std_logic;
		c_valid			: in  std_logic
	);
	end component;

	signal cdout : std_logic;

	component mcfilt is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Calculation routine: DEPTH-stage filter of E to S                  --
--                                                                            --
--------------------------------------------------------------------------------
	generic (
		LOWESTBIT	: positive
	);
	port (
--	calculation controls
		clk			: in  std_logic;
		rst			: in  std_logic;
		en			: in  std_logic;
--	E: energy samples in
--	S: sums out
		E			: in  std_logic_vector(15 downto 0);
		S			: out std_logic_vector(47 downto LOWESTBIT);
		inval		: out std_logic;
		c_valid		: out std_logic;
--	coefficients upload
		clk_calib	: in  std_logic;
		cena		: in  std_logic; -- Enable: Shift cdin on cclk
		cdin		: in  std_logic; -- data (bit by bit)
		cdout		: out std_logic
	);
	end component;

-- RAM organisation: distance from @ of VLAN-tag to @ of 1st data bigbyte:
	-- idea: have real data in RAM from x"08" on => ram_data_start := x"08";
	-- but be able to change to send full headers (Ethernet/UDP/IP) as data, too
	-- 											 => ram_data_start := x"01";
	-- THIS NEEDS TO BE TAKEN INTO ACCOUNT AT UDP-BYTES!!
	-- DEFAULT IS x"08" - THEN ACCORDINGLY UDP-BYTES IS 8 PLUS # DATA BYTES

--------------------
-- 
--	RAM management
--
-----------------------
--	RAM requests of calibration: read only
	signal r_flag_calib		: std_logic;
	signal r_addr_calib		: std_logic_vector(7 downto 0);
	signal r_fine_calib		: std_logic;

--	RAM read requests of UDP module:
	signal r_flag_enet		: std_logic := '0';
	signal r_addr_enet		: std_logic_vector(7 downto 0) := x"00";
	signal r_fine_enet		: std_logic;
	signal r_data			: std_logic_vector(63 downto 0);

--	RAM requests from LCD module: read only
	signal r_flag_lcd		: std_logic;	-- in read demand
	signal r_addr_lcd		: std_logic_vector(7 downto 0);	-- in read @
	signal r_fine_lcd		: std_logic; -- out read permission
	signal r_data_lcd		: std_logic_vector(63 downto 0);	-- out read data

--	RAM read requests of McFilt module:
	signal r_flag_mcfilt	: std_logic;	-- in read demand
	signal r_addr_mcfilt	: std_logic_vector(7 downto 0);	-- in read @
	signal r_fine_mcfilt	: std_logic; -- out read permission

--	RAM write requests of UDP module:
	signal w_flag_enet		: std_logic := '0';
	signal w_addr_enet		: std_logic_vector(7 downto 0) := x"00";
	signal w_data_enet		: std_logic_vector(63 downto 0);
	signal w_fine_enet		: std_logic;

--	RAM write requests of McFilt module:
	signal w_flag_mcfilt	: std_logic := '0';
	signal w_addr_mcfilt	: std_logic_vector(7 downto 0) := x"00";
	signal w_data_mcfilt	: std_logic_vector(63 downto 0);
	signal w_fine_mcfilt	: std_logic;

	component RAM_management_bus is
		generic (
	-- number of concurrent writing modules to manage
			write_cnt	: positive;
	-- number of concurrent reading modules to manage
			read_cnt	: positive
		);
		port (
	--	controls
			clk			: in  std_logic;
			rst			: in  std_logic;

	--	write ports
			w_flag_bus	: in  std_logic_vector(write_cnt-1 downto 0);
			w_addr_bus	: in		  t_w_addr(write_cnt-1 downto 0);
			w_data_bus	: in		  t_w_data(write_cnt-1 downto 0);
			w_fine_bus	: out std_logic_vector(write_cnt-1 downto 0);

	--	read ports
			r_flag_bus	: in  std_logic_vector(read_cnt-1 downto 0);
			r_addr_bus	: in		  t_r_addr(read_cnt-1 downto 0);
			r_fine_bus	: out std_logic_vector(read_cnt-1 downto 0);

	--	RAM data output
			r_data		: out std_logic_vector(r_data_width-1 downto 0)
		);
	end component;

--	component RAM_management is
--	port (
----	controls
--		clk				: in  std_logic;
--		rst				: in  std_logic;
--
----	RAM requests of calibration: read only
--		r_flag_calib	: in std_logic;
--		r_addr_calib	: in std_logic_vector(7 downto 0);
--		r_fine_calib	: out std_logic;
--
----	RAM read requests of UDP-module:
--		r_flag_enet		: in std_logic;
--		r_addr_enet		: in std_logic_vector(7 downto 0);
--		r_fine_enet		: out std_logic;
--
----	RAM write requests of UDP-module:
--		w_flag_enet		: in std_logic;
--		w_addr_enet		: in std_logic_vector(7 downto 0);
--		w_data_enet		: in std_logic_vector(63 downto 0);
--		w_fine_enet		: out std_logic;
--
----	RAM read requests of McFilt:
--		r_flag_mcfilt	: in std_logic;
--		r_addr_mcfilt	: in std_logic_vector(7 downto 0);
--		r_fine_mcfilt	: out std_logic;
--
----	RAM write requests of McFilt:
--		w_flag_mcfilt	: in std_logic;
--		w_addr_mcfilt	: in std_logic_vector(7 downto 0);
--		w_data_mcfilt	: in std_logic_vector(63 downto 0);
--		w_fine_mcfilt	: out std_logic;
--
----	RAM requests of LCD module: read only
--		r_flag_lcd		: in std_logic;	-- in read demand
--		r_addr_lcd		: in std_logic_vector(7 downto 0);	-- in read @
--		r_fine_lcd		: out std_logic; -- out read accepted
--
----	RAM data output
--		r_data			: out std_logic_vector(63 downto 0)
--	);
--	end component RAM_management;

--	recalibration of filter coefficients a_i
	-- contols
	signal calib_valid			: std_logic := '0';	-- indicate valid calibration

	-- coeeficients upload
	signal cena : std_logic := '0';
	signal cdin : std_logic := '0';

	component recalibration is
	generic (
		coeff_bytes		: signed(10 downto 0)
	);
	port (
--	controls
		clk				: in  std_logic;
		rst				: in  std_logic;

		calib_start		: in  std_logic;
		calib_enable	: out std_logic;
		calib_valid		: out std_logic;
--	RAM management
		r_addr_calib	: out std_logic_vector(7 downto 0);
		r_flag_calib	: out std_logic;
		r_data_calib	: in  std_logic_vector(63 downto 0);
--	coefficients upload
		cena			: out  std_logic;
		cdin			: out  std_logic
	);
	end component;

---	signals for LCD_top_driver:

--	constant one_sec : integer := 50000000; -- 50 MHz: 50M == 1s
--	constant one_sec : integer := 125000000; -- 125 MHz: 125M == 1 Second
	constant one_milisec : integer := 50000; -- 50MHz: 50k == 1 ms
--	constant one_microsec : integer := 50;

	signal timer	: integer := 0;
	signal seconds	: unsigned(4 downto 0) := (others => '0');

	component LCD_top_driver is
	port(
--	input controls
		clk					: in  std_logic;
		clk_50MHz			: in  std_logic;
		rst					: in  std_logic;

--	LCD control pins on target board
		lcd_e				: out std_logic;
		lcd_rs				: out std_logic;
		lcd_rw				: out std_logic;
		lcd_data			: inout std_logic_vector(7 downto 4);

--	RAM requests from LCD module: read only
		r_flag_lcd			: out std_logic;	-- in read demand
		r_addr_lcd			: out std_logic_vector(7 downto 0);	-- in read @
		r_fine_lcd			: in  std_logic; -- out read permission
		r_data_lcd			: in  std_logic_vector(63 downto 0);	-- out read data

--	communication ports LCD <-> Enet module
		capture_src_flag	: in  std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: in  std_logic;	-- DST MAC @ was found
		mac_readout_flag	: out std_logic	-- MACs have been read out
	);
	end component;



------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Beginning of source programming code                               --
--                                                                            --
--------------------------------------------------------------------------------
begin

	UDP: UDP_manager
	port map (
--	Ethernet clocking
		clk					=> rtx_clk,
		rst					=> rtx_rst,

--	Ethernet data + controls
		rx_data_in			=> rx_data_in,	-- Input data
		rx_sof_in_n			=> rx_sof_in_n,	-- Input start of frame
		rx_eof_in_n			=> rx_eof_in_n,	-- Input end of frame
		rx_src_rdy_in_n 	=> rx_src_rdy_in_n,	-- Input source ready
		tx_data_out			=> tx_data_out,	-- Modified output data
		tx_sof_out_n		=> tx_sof_out_n,	-- Output start of frame						
		tx_eof_out_n		=> tx_eof_out_n,	-- Output end of frame							
		tx_src_rdy_out_n	=> tx_src_rdy_out_n,	-- Output source ready
		tx_dst_rdy_in_n	=> tx_dst_rdy_in_n,	-- Input destination ready

--	communication ports UDP module <-> LCD module
		capture_src_flag	=> capture_src_flag,
		capture_dst_flag	=> capture_dst_flag,
		mac_readout_flag	=> mac_readout_flag,

--	RAM read requests of UDP-module:
		r_flag_enet			=> r_flag_enet,
		r_addr_enet			=> r_addr_enet,
		r_fine_enet			=> r_fine_enet,
		r_data				=> r_data,

--	RAM write requests of UDP-module:
		w_flag_enet			=> w_flag_enet,
		w_addr_enet			=> w_addr_enet,
		w_data_enet			=> w_data_enet,
		w_fine_enet			=> w_fine_enet,
		
--	communication UDP module <-> Calibration module
		calib_start			=> calib_start,
		calib_enable		=> calib_enable,

--	communication UDP module <-> McFilt module
		calc_start			=> calc_start,
		calc_enable			=> calc_enable,

--	external stimuli
		send_frame_flag		=> send_frame_flag
	);


	r_data_lcd <= r_data;

	manage_RAM: RAM_management_bus
	generic map (
		write_cnt	=> w_bus_cnt,
		read_cnt	=> r_bus_cnt
	)
	port map (
--	controls
		clk				=> rtx_clk,
		rst				=> rtx_rst,

--	read port mapping: busses have to be contiguous
--		r(3) = lcd
--		r(2) = calib
--		r(1) = enet
--		r(0) = mcfilt

		r_flag_bus(3)	=> r_flag_lcd,
		r_flag_bus(2)	=> r_flag_calib,
		r_flag_bus(1)	=> r_flag_enet,
		r_flag_bus(0)	=> r_flag_mcfilt,

		r_addr_bus(3)	=> r_addr_lcd,
		r_addr_bus(2)	=> r_addr_calib,
		r_addr_bus(1)	=> r_addr_enet,
		r_addr_bus(0)	=> r_addr_mcfilt,

		r_fine_bus(3)	=> r_fine_lcd,
		r_fine_bus(2)	=> r_fine_calib,
		r_fine_bus(1)	=> r_fine_enet,
		r_fine_bus(0)	=> r_fine_mcfilt,

--	write port mapping: busses have to be contiguous
--		w(1) = enet
--		w(0) = mcfilt

		w_flag_bus(1)	=> w_flag_enet,
		w_flag_bus(0)	=> w_flag_mcfilt,

		w_addr_bus(1)	=> w_addr_enet,
		w_addr_bus(0)	=> w_addr_mcfilt,

		w_data_bus(1)	=> w_data_enet,
		w_data_bus(0)	=> w_data_mcfilt,

		w_fine_bus(1)	=> w_fine_enet,
		w_fine_bus(0)	=> w_fine_mcfilt,

--	RAM data output
		r_data			=> r_data
	);

--	manage_RAM: RAM_management
--	port map (
----	controls
--		clk				=> rtx_clk,
--		rst				=> rtx_rst,
--
----	RAM requests of calibration
--		r_flag_calib	=> r_flag_calib,
--		r_addr_calib	=> r_addr_calib,
--		r_fine_calib	=> r_fine_calib,
--
----	RAM read requests of UDP module
--		r_flag_enet		=> r_flag_enet,
--		r_addr_enet		=> r_addr_enet,
--		r_fine_enet		=> r_fine_enet,
--
----	RAM write requests of UDP module
--		w_flag_enet		=> w_flag_enet,
--		w_addr_enet		=> w_addr_enet,
--		w_data_enet		=> w_data_enet,
--		w_fine_enet		=> w_fine_enet,
--
----	RAM read requests of McFilt
--		r_flag_mcfilt	=> r_flag_mcfilt,
--		r_addr_mcfilt	=> r_addr_mcfilt,
--		r_fine_mcfilt	=> r_fine_mcfilt,
--
----	RAM write requests of McFilt
--		w_flag_mcfilt	=> w_flag_mcfilt,
--		w_addr_mcfilt	=> w_addr_mcfilt,
--		w_data_mcfilt	=> w_data_mcfilt,
--		w_fine_mcfilt	=> w_fine_mcfilt,
--
----	RAM requests of LCD module
--		r_flag_lcd		=> r_flag_lcd,
--		r_addr_lcd		=> r_addr_lcd,
--		r_fine_lcd		=> r_fine_lcd,
--
----	RAM data output
--		r_data			=> r_data
--	);

	McFilt_manage: McFilt_manager
	generic map (
		E_samples		=> RAM_E_samples,
		LOWESTBIT		=> LOWESTBIT
	)
	port map (
--	clocking
		clk_125MHz		=> rtx_clk,
		clk_mcfilt		=> clk_mcfilt,
		rst				=> rtx_rst,

--	communication McFilt-manager -> McFilt
		mcfilt_enable	=> mcfilt_enable,
		mcfilt_reset	=> mcfilt_reset,

--	RAM read requests of McFilt manager:
		r_flag_mcfilt	=> r_flag_mcfilt,
		r_addr_mcfilt	=> r_addr_mcfilt,
		r_fine_mcfilt	=> r_fine_mcfilt,
		r_data			=> r_data,

--	RAM write requests of McFilt manager:
		w_flag_mcfilt	=> w_flag_mcfilt,
		w_addr_mcfilt	=> w_addr_mcfilt,
		w_data_mcfilt	=> w_data_mcfilt,
		w_fine_mcfilt	=> w_fine_mcfilt,

--	external stimuli
		send_frame_flag	=> send_frame_flag,

--	communication McFilt manager <-> UDP module
		calc_start		=> calc_start,
		calc_enable		=> calc_enable,

--	Energy to be put into McFilt
	    E				=> E,
--	Result from McFilt
		S				=> S,
		inval			=> inval,
		c_valid			=> c_valid
	);

	E_calculation: mcfilt
	generic map (
		LOWESTBIT	=> LOWESTBIT
	)
	port map (
--	calculation controls
		clk			=> clk_mcfilt,
		rst 		=> mcfilt_reset,
		en  		=> mcfilt_enable,
--	E: energy samples in
--	S: sums out
		E   		=> E,
		S   		=> S,
		inval		=> inval,
		c_valid		=> c_valid,
--	coefficients upload
		clk_calib	=> rtx_clk,
		cena		=> cena, -- Enable: Shift cdin on cclk
		cdin		=> cdin, -- data (bit by bit)
		cdout		=> cdout
	);

	recalib: recalibration
	generic map (
		coeff_bytes		=> ram_coeff_bytes
	)
	port map (
--	controls
		rst				=> rtx_rst,
		clk				=> rtx_clk,

		calib_start		=> calib_start,
		calib_enable	=> calib_enable,
		calib_valid		=> calib_valid,
--	RAM management
		r_addr_calib	=> r_addr_calib,
		r_flag_calib	=> r_flag_calib,
		r_data_calib	=> r_data,
--	coefficients upload
		cena			=> cena,
		cdin			=> cdin
	);

--------------------------------------------------------------------------------
--	handling data transfer to LCD panel @ 50MHz
--------------------------------------------------------------------------------
	timing: process (clk_50MHz) is
	begin
--------------------------------------------------------------------------------
--	act on clock signal only:
--------------------------------------------------------------------------------
		if	(rising_edge(clk_50MHz)) then
			if (reset = '1') then
				timer <= 0;
				seconds <= (others => '0');
			else
--------------------------------------------------------------------------------
--	let time pass (timer gives event for new text)
--------------------------------------------------------------------------------
				if timer > 125*one_milisec then
--------------------------------------------------------------------------------
--	time to write next text: activate timer flag to be readout in read_from_ram
--------------------------------------------------------------------------------
					seconds <= seconds + 1;
					timer <= 0;				--	rst timer
				else
					timer <= timer + 1;
				end if;
			end if;
		end if;
	end process timing;

	manage_LCD: LCD_top_driver
	port map(
--	input controls
		clk					=> rtx_clk,
		clk_50MHz			=> clk_50MHz,
		rst					=> reset,

--	LCD interface
		lcd_e				=> lcd_e,
		lcd_rs				=> lcd_rs,
		lcd_rw				=> lcd_rw,
		lcd_data			=> lcd_data,

--	RAM requests from LCD module: read only
		r_flag_lcd			=> r_flag_lcd,
		r_addr_lcd			=> r_addr_lcd,
		r_fine_lcd			=> r_fine_lcd,
		r_data_lcd			=> r_data_lcd,

--	communication ports LCD <-> Enet module
		capture_src_flag	=> capture_src_flag,
		capture_dst_flag	=> capture_dst_flag,
		mac_readout_flag	=> mac_readout_flag
	);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Visualising process: Push-Button LEDs are used to indicate time    --
--                                                                            --
--           ^              16                                                --
--         < o >   ==   08  04  02   == max. 31 seconds                       --
--           v              01                                                --
--                                                                            --
--------------------------------------------------------------------------------
	leds_nwces <= std_logic_vector(seconds);		
	leds8(0 to 3) <= capture_dst_flag & capture_src_flag & mac_readout_flag & r_addr_lcd(0);
--	leds8(4 to 7) <= "0000";--rx_src_rdy_n_0_i & tx_src_rdy_n_0_i & "10";
	leds8(7) <= '0';
	leds8(6) <= cdout;
	leds8(5) <= clk_locked;
	leds8(4) <= calib_valid;

--	reset_deb: debounce
--    Port map (
--		sig_in => switches_nwces(2),-- center button
--        clk => clk_50MHz,
--        sig_out => reset
--	);

	reset <= switches_nwces(2);
	user_reset <= reset;

--	start_Enet_output_deb: debounce
--    Port map (
--		sig_in => switches_nwces(4),-- north button
--        clk => rtx_clk,
--        sig_out => start_Enet
--	);
--	
----	send_frame_flag <= start_enet;

end arch1;