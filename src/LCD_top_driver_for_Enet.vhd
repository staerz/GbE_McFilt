library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LCD_top_driver is
	port(
--	input controls
		clk					: in  std_logic;
		clk_50MHz			: in  std_logic;
		rst					: in  std_logic;

--	LCD control pins on target board
		lcd_e					: out std_logic;
		lcd_rs				: out std_logic;
		lcd_rw				: out std_logic;
		lcd_data				: inout std_logic_vector(7 downto 4);

--	RAM requests from LCD module: read only
		r_flag_lcd			: out std_logic;	-- in read demand
		r_addr_lcd			: out std_logic_vector(7 downto 0);	-- in read @
		r_fine_lcd			: in  std_logic; -- out read permission
		r_data_lcd			: in  std_logic_vector(63 downto 0);	-- out read data

--	communication ports LCD <-> Enet module
		capture_src_flag	: in  std_logic;	-- SRC MAC @ was found
		capture_dst_flag	: in  std_logic;	-- DST MAC @ was found
		mac_readout_flag	: out std_logic	-- MACs have been read out
	);
end LCD_top_driver;

library IEEE;
--use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library my_definitions;
use my_definitions.mytypes.all;

architecture Behavioral of LCD_top_driver is

	signal timer	: integer := 0;

	signal timer_flag			: std_logic := '0';
	signal have_read_symbol		: std_logic := '0';
	signal save_macs			: std_logic := '0';	-- address to display is src == 0, dest == 1

------------------------------  <-  80 chars  ->  ------------------------------
--         LCD panel transfer variables                                       --
--------------------------------------------------------------------------------
--	flag to indicate LCD new writing instruction
	signal writing_mode	: std_logic := '0';
--	positionning
	signal posx		: integer range 0 to 16 := 0;
	signal posy		: integer range 0 to 2 := 0;
--	char (ASCII code in 8 bits)
	signal symbol	: std_logic_vector(7 downto 0);
--	string to be transmitted to LCD
	signal schtring	: string (1 to 16);

	component LCD_driver is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         LCD module driver                                                  --
--                                                                            --
--  Required Input:	see signal description                                    --
--                                                                            --
--  Given Output:	see signal description                                    --
--                                                                            --
--------------------------------------------------------------------------------
	port(
		reset	: in std_logic;	--	reset to clear display
		clk		: in std_logic;	--	clock signal: 50MHz required

		posx	: in integer range 0 to 16;	--	X-Position on the LCD
		posy	: in integer range 0 to 2;	--	Y-Position on the LCD
		text	: in string (1 to 16);	--	string to be displayed
		symbol	: in std_logic_vector(7 downto 0);	--	char to be displayed (low priority)
		flag	: in std_logic;	--	input flag to indicate new data transfer
		
		SF_D	: out std_logic_vector(3 downto 0);	--	output data to LCD
		LCD_E	: out std_logic;	--	control commands to LCD
		LCD_RS	: out std_logic;	--	control commands to LCD
		LCD_RW	: out std_logic;	--	control commands to LCD
		LED		: out std_logic_vector(7 downto 0)	--	visual effect to show LCD activity
	);
	end component;


------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         char (ASCII) to std_logic_vector conversion                        --
--                                                                            --
--------------------------------------------------------------------------------
	function char_to_byte(char: character) return std_logic_vector is
	begin
		return CONV_STD_LOGIC_vector(character'pos(char),8);
	end char_to_byte;

begin

	lcd: LCD_driver
	port map(
		reset => rst,
		clk => clk_50MHz,

		posx => posx,
		posy => posy,
		text => schtring,
		symbol => symbol,
		flag => writing_mode,
		
		SF_D => lcd_data(7 downto 4),
		LCD_E => LCD_E,
		LCD_RS => LCD_RS,
		LCD_RW => LCD_RW,
		LED => open
--		LED => ledbits
	);

--------------------------------------------------------------------------------
--	handling data transfer to LCD panel @ 50MHz
--------------------------------------------------------------------------------
	transfer_to_LCD: process (clk_50MHz) is
	begin
--------------------------------------------------------------------------------
--	act on clock signal only:
--------------------------------------------------------------------------------
		if	(rising_edge(clk_50MHz)) then
			if (rst = '1') then
				timer <= 0;
				writing_mode <= '0';
			else
--------------------------------------------------------------------------------
--	let time pass (timer gives event for new text)
--------------------------------------------------------------------------------
				if timer > DISPLAYDELAY then
--------------------------------------------------------------------------------
--	time to write next text: activate timer flag to be readout in read_from_ram
--------------------------------------------------------------------------------
					timer <= 0;				--	rst timer
					timer_flag <= '1';
					writing_mode <= '0';
				else
					timer <= timer + 1;
					if have_read_symbol = '1' then
						writing_mode <= '1';
						timer_flag <= '0';
					else
						writing_mode <= '0';
					end if;
				end if;
			end if;
		end if;
	end process transfer_to_LCD;

--------------------------------------------------------------------------------
--	handling data readout from RAM @ clk to monitor at LCD
--------------------------------------------------------------------------------
	read_from_RAM: process (clk) is
	variable x		: integer range 0 to 16;
	variable y		: integer range 0 to 2;
	variable hex	: std_logic_vector(3 downto 0);
	begin
		if (rising_edge(clk)) then
			schtring <= NUL & NUL & NUL & NUL & NUL & NUL & NUL & NUL
					  & NUL & NUL & NUL & NUL & NUL & NUL & NUL & NUL;
			r_addr_lcd <= (others => '0');
			if rst = '1' then
				r_flag_lcd <= '0';
--				r_addr_lcd <= (others => '0');
				mac_readout_flag <= '0';
				have_read_symbol <= '0';
				y := 0;
				x := 1;
			elsif (timer_flag = '1') then
--------------------------------------------------------------------------------
--	timer_flag activated in transfer_to_LCD enables read_from_RAM,
--	read_from_RAM then enables have_read_symbol when a symbol to be display
--	is read from RAM initiated by capture_src_flag or capture_dst_flag,
--	read_from_RAM will set have_read_symbol to rst timer_flag ransfer_to_LCD
--------------------------------------------------------------------------------
				if (have_read_symbol = '0') then
					if save_macs = '0' then	--check for source mac
						if capture_src_flag = '1' then
							r_addr_lcd <= x"02";
							y := 1;
							r_flag_lcd <= '1';
						else
							y := 0;
							r_flag_lcd <= '0';
							mac_readout_flag <= '0';
						end if;
					else	--	check for dest mac
						if capture_dst_flag = '1' then
							r_addr_lcd <= x"01";
							y := 2;
							r_flag_lcd <= '1';
						else
							y := 0;
							r_flag_lcd <= '0';
							mac_readout_flag <= '0';
						end if;
					end if;
					if (y > 0) then
	if r_fine_lcd = '1' then
						case x is
							when 1 => symbol <= r_data_lcd(63 downto 56);
							when 2 => symbol <= r_data_lcd(55 downto 48);
							when 3 | 4 => symbol <= char_to_byte(' ');
							when others =>
								hex := r_data_lcd(63-4*(x-1) downto 60-4*(x-1));
								if hex < x"A" then
									symbol <= x"0" & hex + x"30";
								else
									symbol <= x"0" & hex + x"37";
									-- char A = x"41" (d10) <=> hex = "A" (d65)
								end if;
						end case;
						posx <= x;
						posy <= y;
						if x < 16 then
							x := x + 1;
						else
							x := 1;
							save_macs <= not save_macs; -- check for other mac
							if (y = 2) then	-- finished displaying
								mac_readout_flag <= '1';-- check for new data
								y := 0;
							end if;
						end if;
						have_read_symbol <= '1';
	end if;
					end if;
				end if;
			else
				have_read_symbol <= '0';
			end if;
		end if;
	end process;

end Behavioral;

