--------------------------------------------------------------------------------
-- Copyright (c) 1995-2009 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 11.5
--  \   \         Application : xaw2vhdl
--  /   /         Filename : pll1.vhd
-- /___/   /\     Timestamp : 08/25/2010 16:07:02
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: xaw2vhdl-intstyle E:/Xilinx_Projects/Steffen/_source_files/pll1.xaw -st pll1.vhd
--Design Name: pll1
--Device: xc5vfx70t-1ff1136
--
-- Module pll1
-- Generated by Xilinx Architecture Wizard
-- Written for synthesis tool: XST
-- For block PLL_ADV_INST, Estimated PLL Jitter for CLKOUT0 = 0.113 ns
-- For block PLL_ADV_INST, Estimated PLL Jitter for CLKOUT1 = 0.170 ns

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clocking is
	port (
		CLKIN1_IN	: in  std_logic; 
		RST_IN		: in  std_logic; 
		CLKOUT0_OUT	: out std_logic; 
		CLKOUT1_OUT	: out std_logic; 
		CLKOUT2_OUT	: out std_logic; 
		LOCKED_OUT	: out std_logic
	);
end clocking;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture BEHAVIORAL of clocking is
	signal CLKFBOUT1_CLKFBIN	: std_logic;
	signal CLKFBOUT2_CLKFBIN	: std_logic;
	signal CLKIN1_BUF		: std_logic;
	signal CLKOUT0_BUF		: std_logic;
	signal CLKOUT1_BUF		: std_logic;
	signal CLKOUT2_BUF		: std_logic;
	signal GND_BIT			: std_logic;
	signal GND_BUS_5		: std_logic_vector (4 downto 0);
	signal GND_BUS_16		: std_logic_vector (15 downto 0);
	signal VCC_BIT			: std_logic;
	
	signal locked1_out		: std_logic;
	signal locked2_out		: std_logic;
begin
	GND_BIT <= '0';
	GND_BUS_5(4 downto 0) <= "00000";
	GND_BUS_16(15 downto 0) <= "0000000000000000";
	VCC_BIT <= '1';

	CLKIN0_BUFG_INST : IBUFG
	port map (
		I	=> CLKIN1_IN,
		O	=> CLKIN1_BUF
	);

	CLKOUT0_BUFG_INST : BUFG
	port map (
		I	=> CLKOUT0_BUF,
		O	=> CLKOUT0_OUT
	);

	CLKOUT1_BUFG_INST : BUFG
	port map (
		I	=> CLKOUT1_BUF,
		O	=> CLKOUT1_OUT
	);

	CLKOUT2_BUFG_INST : BUFG
	port map (
		I	=> CLKOUT2_BUF,
		O	=> CLKOUT2_OUT
	);

	PLL_ADV_INST1 : PLL_ADV
	generic map(
		BANDWIDTH			=> "OPTIMIZED",
		CLKIN1_PERIOD		=> 10.000,
		CLKIN2_PERIOD		=> 10.000,
		CLKOUT0_DIVIDE		=> 8,  --- 1000/ 8 = 125
		CLKOUT1_DIVIDE		=> 20, --- 1000/20 =  50
		CLKOUT0_PHASE		=> 0.000,
		CLKOUT1_PHASE		=> 0.000,
		CLKOUT0_DUTY_CYCLE	=> 0.500,
		CLKOUT1_DUTY_CYCLE	=> 0.500,
		COMPENSATION		=> "SYSTEM_SYNCHRONOUS",
		DIVCLK_DIVIDE		=> 1,
		CLKFBOUT_MULT		=> 10, --- 100*10 = 1000
		CLKFBOUT_PHASE		=> 0.0,
		REF_JITTER			=> 0.000000
	)
	port map (
		CLKFBIN				=> CLKFBOUT1_CLKFBIN,
		CLKINSEL			=> VCC_BIT,
		CLKIN1				=> CLKIN1_BUF,
		CLKIN2				=> GND_BIT,
		DADDR(4 downto 0)	=> GND_BUS_5(4 downto 0),
		DCLK				=> GND_BIT,
		DEN					=> GND_BIT,
		DI(15 downto 0)		=> GND_BUS_16(15 downto 0),
		DWE					=> GND_BIT,
		REL					=> GND_BIT,
		RST					=> RST_IN,
		CLKFBDCM			=> open,
		CLKFBOUT			=> CLKFBOUT1_CLKFBIN,
		CLKOUTDCM0			=> open,
		CLKOUTDCM1			=> open,
		CLKOUTDCM2			=> open,
		CLKOUTDCM3			=> open,
		CLKOUTDCM4			=> open,
		CLKOUTDCM5			=> open,
		CLKOUT0				=> CLKOUT0_BUF,
		CLKOUT1				=> CLKOUT1_BUF,
		CLKOUT2				=> open,
		CLKOUT3				=> open,
		CLKOUT4				=> open,
		CLKOUT5				=> open,
		DO					=> open,
		DRDY				=> open,
		LOCKED				=> LOCKED1_OUT
	);

	PLL_ADV_INST2 : PLL_ADV
	generic map(
		BANDWIDTH			=> "OPTIMIZED",
		CLKIN1_PERIOD		=> 10.000,
		CLKIN2_PERIOD		=> 10.000,
		CLKOUT0_DIVIDE		=> clk_div,
		CLKOUT0_PHASE		=> 0.000,
		CLKOUT0_DUTY_CYCLE	=> 0.500,
		COMPENSATION		=> "SYSTEM_SYNCHRONOUS",
		DIVCLK_DIVIDE		=> 1,
		CLKFBOUT_MULT		=> clk_mult,
		CLKFBOUT_PHASE		=> 0.0,
		REF_JITTER			=> 0.000000
	)
	port map (
		CLKFBIN				=> CLKFBOUT2_CLKFBIN,
		CLKINSEL			=> VCC_BIT,
		CLKIN1				=> CLKIN1_BUF,
		CLKIN2				=> GND_BIT,
		DADDR(4 downto 0)	=> GND_BUS_5(4 downto 0),
		DCLK				=> GND_BIT,
		DEN					=> GND_BIT,
		DI(15 downto 0)		=> GND_BUS_16(15 downto 0),
		DWE					=> GND_BIT,
		REL					=> GND_BIT,
		RST					=> RST_IN,
		CLKFBDCM			=> open,
		CLKFBOUT			=> CLKFBOUT2_CLKFBIN,
		CLKOUTDCM0			=> open,
		CLKOUTDCM1			=> open,
		CLKOUTDCM2			=> open,
		CLKOUTDCM3			=> open,
		CLKOUTDCM4			=> open,
		CLKOUTDCM5			=> open,
		CLKOUT0				=> CLKOUT2_BUF,
		CLKOUT1				=> open,
		CLKOUT2				=> open,
		CLKOUT3				=> open,
		CLKOUT4				=> open,
		CLKOUT5				=> open,
		DO					=> open,
		DRDY				=> open,
		LOCKED				=> LOCKED2_OUT
	);

	LOCKED_OUT <= locked1_out and locked2_out;

end BEHAVIORAL;