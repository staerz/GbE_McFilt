-------------------------------------------------------------------------------
-- Title      : Virtex-5 Ethernet MAC Example Design Wrapper
-- Project    : Virtex-5 Ethernet MAC Wrappers
-------------------------------------------------------------------------------
-- File       : v5_emac_v1_6_example_design.vhd
-------------------------------------------------------------------------------
-- Creator    : Xilinx, Inc.
-- Editor     : Steffen St�rz, IKTP TU Dresden
------------------------------------------------------------------------
-- Disclaimer :
--
-- Copyright (c) 2004-2008 by Xilinx, Inc. All rights reserved.
-- This text/file contains proprietary, confidential
-- information of Xilinx, Inc., is distributed under license
-- from Xilinx, Inc., and may be used, copied and/or
-- disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc. Xilinx hereby grants you
-- a license to use this text/file solely for design, simulation,
-- implementation and creation of design files limited
-- to Xilinx devices or technologies. Use with non-Xilinx
-- devices or technologies is expressly prohibited and
-- immediately terminates your license unless covered by
-- a separate agreement.
--
-- Xilinx is providing this design, code, or information
-- "as is" solely for use in developing programs and
-- solutions for Xilinx devices. By providing this design,
-- code, or information as one possible implementation of
-- this feature, application or standard, Xilinx is making no
-- representation that this implementation is free from any
-- claims of infringement. You are responsible for
-- obtaining any rights you may require for your implementation.
-- Xilinx expressly disclaims any warranty whatsoever with
-- respect to the adequacy of the implementation, including
-- but not limited to any warranties or representations that this
-- implementation is free from claims of infringement, implied
-- warranties of merchantability or fitness for a particular
-- purpose.
--
-- Xilinx products are not intended for use in life support
-- appliances, devices, or systems. Use in such applications are
-- expressly prohibited.
--
-- This copyright and support notice must be retained as part
-- of this text at all times. (c) Copyright 2004-2008 Xilinx, Inc.
-- All rights reserved.

-------------------------------------------------------------------------------
-- Description:  This is the VHDL example design for the Virtex-5 
--               Embedded Ethernet MAC.  It is intended that
--               this example design can be quickly adapted and downloaded onto
--               an FPGA to provide a real hardware test environment.
--
--               This level:
--
--               * instantiates the TEMAC local link file that instantiates 
--                 the TEMAC top level together with a RX and TX FIFO with a 
--                 local link interface;
--
--               * instantiates a simple client I/F side example design,
--                 providing an address swap and a simple
--                 loopback function;
--
--               * Instantiates IBUFs on the GTX_CLK, REFCLK and HOSTCLK inputs 
--                 if required;
--
--               Please refer to the Datasheet, Getting Started Guide, and
--               the Virtex-5 Embedded Tri-Mode Ethernet MAC User Gude for
--               further information.
--
--
--
--    ---------------------------------------------------------------------
--    | EXAMPLE DESIGN WRAPPER                                            |
--    |           --------------------------------------------------------|
--    |           |LOCAL LINK WRAPPER                                     |
--    |           |              -----------------------------------------|
--    |           |              |BLOCK LEVEL WRAPPER                     |
--    |           |              |    ---------------------               |
--    | --------  |  ----------  |    | ETHERNET MAC      |               |
--    | |      |  |  |        |  |    | WRAPPER           |  ---------    |
--    | |      |->|->|        |--|--->| Tx            Tx  |--|       |--->|
--    | | enet |  |  |        |  |    | client        PHY |  |       |    |
--    | | data |  |  | LOCAL  |  |    | I/F           I/F |  |       |    |  
--    | | mo-  |  |  |  LINK  |  |    |                   |  | PHY   |    |
--    | | dule |  |  |  FIFO  |  |    |                   |  | I/F   |    |
--    | |      |  |  |        |  |    |                   |  |       |    |
--    | |      |  |  |        |  |    | Rx            Rx  |  |       |    |
--    | |      |  |  |        |  |    | client        PHY |  |       |    |
--    | |      |<-|<-|        |<-|----| I/F           I/F |<-|       |<---|
--    | |      |  |  |        |  |    |                   |  ---------    |
--    | --------  |  ----------  |    ---------------------               |
--    |           |              -----------------------------------------|
--    |           --------------------------------------------------------|
--    ---------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library unisim;
use unisim.vcomponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



-------------------------------------------------------------------------------
-- The entity declaration for the example design.
-------------------------------------------------------------------------------
entity v5_emac_v1_6_example_design is
--	Ports defnition: I/O Ports (naming) the Virtex is using
--	The real 1:1 assignement of names to ports in done in the *.ucf-file
	Port (
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning of Cursor Push-Button LEDs and push buttons              --
--                                                                            --
--           ^                                       N                        --
--         < o >   ==                               WCE   ==   "NWCES"        --
--           v                                       S                        --
--                                                                            --
--------------------------------------------------------------------------------
		LEDs_NWCES : out std_logic_vector(4 downto 0);
		switches_nwces : in std_logic_vector(4 downto 0);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         System Clock using on board timer                                  --
--                                                                            --
--------------------------------------------------------------------------------
		clk	: in    std_logic;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning of I/O LEDs below LCD-Panel                              --
--                                                                            --
--------------------------------------------------------------------------------
		leds8	: out std_logic_vector (0 to 7);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning of LCD Panel operation                                   --
--                                                                            --
--------------------------------------------------------------------------------
		lcd_e		: out std_logic;	-- LCD enable
		lcd_rs		: out std_logic;	-- LCD control
		lcd_rw		: out std_logic;	-- LCD control
		lcd_data	: inout std_logic_vector(7 downto 4);	-- LCD data

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning of 8bit Dip Switch                                       --
--                                                                            --
--------------------------------------------------------------------------------
--		dips8	: in std_logic_vector (1 to 8);  -- not used in this example

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning rotary switch                                            --
--                                                                            --
--------------------------------------------------------------------------------
--		inca	: in std_logic;
--		incb	: in std_logic;
--		push	: in std_logic --that actually doesn't work, don't ask why not

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Assigning GMII Interface - EMAC0 for Ethernetof                    --
--                                                                            --
--------------------------------------------------------------------------------
------------------------------  <-  80 chars  ->  ------------------------------
--    Client Receiver Interface - EMAC0: refere to UG194, page 33             --
--     I have not been using this interface at all so far                     --
--------------------------------------------------------------------------------
		EMAC0CLIENTRXDVLD               : out std_logic;
		-->	The Ethernet MAC indicates to the client receipt of valid frame data.
		EMAC0CLIENTRXFRAMEDROP          : out std_logic;
		-->	This signal is asserted to notify the client that an incoming receive
		--	frames destination address does not match any addresses in the address
		--	filter. Signal functions even when the address filter is not enabled.
		EMAC0CLIENTRXSTATS              : out std_logic_vector(6 downto 0);
		-->	The statistics data on the last received data frame. The 27-bit raw
		--	RX statistics vector is multiplexed into a 7-bits-per-RX clock cycle
		--	output for statistics gathering, page 81
		EMAC0CLIENTRXSTATSVLD           : out std_logic;
		-->	Asserted by the Ethernet MAC after the end of receiving a frame
		--	to indicate a valid EMAC#CLIENTRXSTATS[6:0] output.
		EMAC0CLIENTRXSTATSBYTEVLD       : out std_logic;
		-->	Asserted if an Ethernet MAC frame byte (including destination
		--	address to FCS) is received. Valid on every RX clock cycle.

------------------------------  <-  80 chars  ->  ------------------------------
--    Client Transmitter Interface - EMAC0                                    --
--     I have not been using this interface at all so far                     --
--------------------------------------------------------------------------------
		CLIENTEMAC0TXIFGDELAY           : in  std_logic_vector(7 downto 0);
		-->	Configurable interframe gap (IFG) adjustment for full-duplex mode.
		EMAC0CLIENTTXSTATS              : out std_logic;
		-->	32bit Transmitter Statistics Vector, page 78
		EMAC0CLIENTTXSTATSVLD           : out std_logic;
		-->	validation of Statistics Vector, page 78
		EMAC0CLIENTTXSTATSBYTEVLD       : out std_logic;
		-->	Status Byte Valid: asserted if Ethernet MAC frame BYTE is transmitted
		--	(including dest. address to FCS). This is valid on every TX clock cycle.

		--	MAC Control Interface - EMAC0
		CLIENTEMAC0PAUSEREQ             : in  std_logic;
		-->	Asserted by client to transmit a pause frame, page 75
		CLIENTEMAC0PAUSEVAL             : in  std_logic_vector(15 downto 0);
		-->	The amount of pause time for the transmitter as defined 
		--	in the IEEE Std 802.3-2002 specification, page 75


		-- Clock Signals - EMAC0
--		GTX_CLK_0                       : in  std_logic; --> as signal only

		-- GMII Interface - EMAC0
		GMII_TXD_0                      : out std_logic_vector(7 downto 0);
		GMII_TX_EN_0                    : out std_logic;
		GMII_TX_ER_0                    : out std_logic;
		GMII_TX_CLK_0                   : out std_logic;

		GMII_RXD_0                      : in  std_logic_vector(7 downto 0);
		GMII_RX_DV_0                    : in  std_logic;
		GMII_RX_ER_0                    : in  std_logic;
		GMII_RX_CLK_0                   : in  std_logic;

		MII_TX_CLK_0                    : in  std_logic;
		GMII_COL_0                      : in  std_logic;
		GMII_CRS_0                      : in  std_logic;

		-- Reference clock for RGMII IODELAYs
		REFCLK                          : in  std_logic; 


		-- Asynchronous Reset
--		RESET                           : in  std_logic;

		-- PhyReset
		EthPhy_Reset_n					: out std_logic--;
		
----	for debugging purpose
--		signal_monitor_1				: out std_logic;
--		signal_monitor_2				: out std_logic;
--		signal_monitor_3				: out std_logic
   );
end v5_emac_v1_6_example_design;


architecture TOP_LEVEL of v5_emac_v1_6_example_design is

	signal clk_50MHz	: std_logic;	-- clock for LCD driver
	signal clk_mcfilt	: std_logic;	-- high speed clock for calculation
	signal clk_locked	: std_logic;	-- high = clocking runs

	COMPONENT clocking
	PORT(
		CLKIN1_IN		: IN std_logic;
		RST_IN			: IN std_logic;          
		CLKOUT0_OUT		: OUT std_logic;
		CLKOUT1_OUT		: OUT std_logic;
		CLKOUT2_OUT		: OUT std_logic;
		LOCKED_OUT		: OUT std_logic
		);
	END COMPONENT;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--      Ethernet Block managing signals                                       --
--                                                                            --
--------------------------------------------------------------------------------

    -- Global asynchronous reset
    signal reset_i               : std_logic;

    -- client interface clocking signals - EMAC0
    signal ll_clk_0_i            : std_logic;
--	signal semi_mux_clk			: std_logic;

    -- address swap transmitter connections - EMAC0
    signal tx_ll_data_0_i      : std_logic_vector(7 downto 0);
    signal tx_ll_sof_n_0_i     : std_logic;
    signal tx_ll_eof_n_0_i     : std_logic;
    signal tx_ll_src_rdy_n_0_i : std_logic;
    signal tx_ll_dst_rdy_n_0_i : std_logic;

   -- address swap receiver connections - EMAC0
    signal rx_ll_data_0_i           : std_logic_vector(7 downto 0);
    signal rx_ll_sof_n_0_i          : std_logic;
    signal rx_ll_eof_n_0_i          : std_logic;
    signal rx_ll_src_rdy_n_0_i      : std_logic;
    signal rx_ll_dst_rdy_n_0_i      : std_logic;

    -- create a synchronous reset in the transmitter clock domain
    signal ll_pre_reset_0_i          : std_logic_vector(5 downto 0);
    signal ll_reset_0_i              : std_logic;

    attribute async_reg : string;
    attribute async_reg of ll_pre_reset_0_i : signal is "true";


    -- Reference clock for RGMII IODELAYs
    signal refclk_ibufg_i            : std_logic;
    signal refclk_bufg_i             : std_logic;

    -- EMAC0 Clocking signals
	signal GTX_CLK_0                 : std_logic;

    -- GMII input clocks to wrappers
    signal tx_clk_0                  : std_logic;
    signal rx_clk_0_i                : std_logic;
    signal gmii_rx_clk_0_delay       : std_logic;

    -- IDELAY controller
    signal idelayctrl_reset_0_r      : std_logic_vector(12 downto 0);
    signal idelayctrl_reset_0_i      : std_logic;

    -- Setting attribute for RGMII/GMII IDELAY
    -- For more information on IDELAYCTRL and IDELAY, please refer to
    -- the Virtex-5 User Guide.
    attribute syn_noprune              : boolean;
    attribute syn_noprune of dlyctrl0  : label is true;

    
    -- GMII client clocks
    signal tx_client_clk_0_o         : std_logic;
    signal tx_client_clk_0           : std_logic;
    signal rx_client_clk_0_o         : std_logic;
    signal rx_client_clk_0           : std_logic;
    -- GMII PHY clocks
    signal tx_phy_clk_0_o            : std_logic;
    signal tx_phy_clk_0              : std_logic;

    -- Speed indication from EMAC wrappers
    signal speed_vector_0_i          : std_logic;
  

    attribute buffer_type : string;
    signal gtx_clk_0_i               : std_logic;
    attribute buffer_type of gtx_clk_0_i  : signal is "none";

  -- Component Declaration for the TEMAC wrapper with 
  -- Local Link FIFO.
	component v5_emac_v1_6_locallink is
	port(
		-- EMAC0 Clocking
		-- TX Client Clock output from EMAC0
		TX_CLIENT_CLK_OUT_0              : out std_logic;
		-- RX Client Clock output from EMAC0
		RX_CLIENT_CLK_OUT_0              : out std_logic;
		-- TX PHY Clock output from EMAC0
		TX_PHY_CLK_OUT_0                 : out std_logic;
		-- EMAC0 TX Client Clock input from BUFG
		TX_CLIENT_CLK_0                  : in  std_logic;
		-- EMAC0 RX Client Clock input from BUFG
		RX_CLIENT_CLK_0                  : in  std_logic;
		-- EMAC0 TX PHY Clock input from BUFG
		TX_PHY_CLK_0                     : in  std_logic;
		-- Speed indicator for EMAC0
		-- Used in clocking circuitry in example_design file.
		EMAC0SPEEDIS10100                : out std_logic;

		-- Local link Receiver Interface - EMAC0
		RX_LL_CLOCK_0                   : in  std_logic; 
		RX_LL_RESET_0                   : in  std_logic;
		RX_LL_DATA_0                    : out std_logic_vector(7 downto 0);
		RX_LL_SOF_N_0                   : out std_logic;
		RX_LL_EOF_N_0                   : out std_logic;
		RX_LL_SRC_RDY_N_0               : out std_logic;
		RX_LL_DST_RDY_N_0               : in  std_logic;
		RX_LL_FIFO_STATUS_0             : out std_logic_vector(3 downto 0);

		-- Local link Transmitter Interface - EMAC0
		TX_LL_CLOCK_0                   : in  std_logic;
		TX_LL_RESET_0                   : in  std_logic;
		TX_LL_DATA_0                    : in  std_logic_vector(7 downto 0);
		TX_LL_SOF_N_0                   : in  std_logic;
		TX_LL_EOF_N_0                   : in  std_logic;
		TX_LL_SRC_RDY_N_0               : in  std_logic;
		TX_LL_DST_RDY_N_0               : out std_logic;

		-- Client Receiver Interface - EMAC0
		EMAC0CLIENTRXDVLD               : out std_logic;
		EMAC0CLIENTRXFRAMEDROP          : out std_logic;
		EMAC0CLIENTRXSTATS              : out std_logic_vector(6 downto 0);
		EMAC0CLIENTRXSTATSVLD           : out std_logic;
		EMAC0CLIENTRXSTATSBYTEVLD       : out std_logic;

		-- Client Transmitter Interface - EMAC0
		CLIENTEMAC0TXIFGDELAY           : in  std_logic_vector(7 downto 0);
		EMAC0CLIENTTXSTATS              : out std_logic;
		EMAC0CLIENTTXSTATSVLD           : out std_logic;
		EMAC0CLIENTTXSTATSBYTEVLD       : out std_logic;

		-- MAC Control Interface - EMAC0
		CLIENTEMAC0PAUSEREQ             : in  std_logic;
		CLIENTEMAC0PAUSEVAL             : in  std_logic_vector(15 downto 0);


		-- Clock Signals - EMAC0
		GTX_CLK_0                       : in  std_logic;

		-- GMII Interface - EMAC0
		GMII_TXD_0                      : out std_logic_vector(7 downto 0);
		GMII_TX_EN_0                    : out std_logic;
		GMII_TX_ER_0                    : out std_logic;
		GMII_TX_CLK_0                   : out std_logic;
		GMII_RXD_0                      : in  std_logic_vector(7 downto 0);
		GMII_RX_DV_0                    : in  std_logic;
		GMII_RX_ER_0                    : in  std_logic;
		GMII_RX_CLK_0                   : in  std_logic;

		MII_TX_CLK_0                    : in  std_logic;
		GMII_COL_0                      : in  std_logic;
		GMII_CRS_0                      : in  std_logic;

		-- Asynchronous Reset
		RESET                           : in  std_logic
	);
	end component;
 
--	mein spass hier

	signal user_reset		: std_logic;	-- reset signal for top_level


	component ethernet_data_module
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Ethernet data processing module                                    --
--                                                                            --
--  Required Input:	see signal description                                    --
--                                                                            --
--  Given Output:	see signal description                                    --
--                                                                            --
--------------------------------------------------------------------------------
	port (
--	global user reset
		user_reset			: out std_logic; -- global user reset

--	CLOCKING:
	--	clock running
		clk_locked			: in std_logic; -- global user reset

	--	Ethernet clocking
		rtx_clk				: in  std_logic; -- Input CLK from TRIMAC Reciever
		rtx_rst				: in  std_logic; -- Synchronous reset signal

	--	LCD clocking
		clk_50MHz			: in  std_logic; -- Input CLK 50MHz

	--	calculation clocking
		clk_mcfilt			: in  std_logic; -- Input high speed CLK (approx. 400MHz)

--	Ethernet data + controls
		rx_data_in			: in  std_logic_vector(7 downto 0); -- Input data
		rx_sof_in_n			: in  std_logic; -- Input start of frame
		rx_eof_in_n			: in  std_logic; -- Input end of frame
		rx_src_rdy_in_n 	: in  std_logic; -- Input source ready
		tx_data_out			: out std_logic_vector(7 downto 0); -- Modified output data
		tx_sof_out_n		: out std_logic; -- Output start of frame						
		tx_eof_out_n		: out std_logic; -- Output end of frame							
		tx_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_dst_rdy_in_n		: in  std_logic; -- Input destination ready	-- check

--	LCD interface
		lcd_e				: out std_logic;	-- LCD enable
		lcd_rs				: out std_logic;	-- LCD control
		lcd_rw				: out std_logic;	-- LCD control
		lcd_data			: inout std_logic_vector(7 downto 4);	-- LCD data

--	LEDs
		leds_nwces			: out std_logic_vector(4 downto 0);
		leds8				: out std_logic_vector(0 to 7);

--	user push buttons
		switches_nwces		: in std_logic_vector(4 downto 0)
	);
	end component;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         Beginning of source programming code                               --
--                                                                            --
--------------------------------------------------------------------------------


begin
	Clocking_inst: clocking
	PORT MAP(
		CLKIN1_IN	=> clk,
		RST_IN		=> user_reset,
		CLKOUT0_OUT	=> GTX_CLK_0,--clk_125MHz,
		CLKOUT1_OUT	=> clk_50MHz,
		CLKOUT2_OUT	=> clk_mcfilt,
		LOCKED_OUT	=> clk_locked
	);

    ------------------------------------------------------------------------
    -- Instantiate the EMAC Wrapper with LL FIFO 
    -- (v5_emac_v1_6_locallink.v)
    ------------------------------------------------------------------------
    v5_emac_ll : v5_emac_v1_6_locallink
    port map (
      -- EMAC0 Clocking
      -- TX Client Clock output from EMAC0
      TX_CLIENT_CLK_OUT_0             => tx_client_clk_0_o,
      -- RX Client Clock output from EMAC0
      RX_CLIENT_CLK_OUT_0             => rx_client_clk_0_o,
      -- TX PHY Clock output from EMAC0
      TX_PHY_CLK_OUT_0                => tx_phy_clk_0_o,
      -- EMAC0 TX Client Clock input from BUFG
      TX_CLIENT_CLK_0                 => tx_client_clk_0,
      -- EMAC0 RX Client Clock input from BUFG
      RX_CLIENT_CLK_0                 => rx_client_clk_0,
      -- EMAC0 TX PHY Clock input from BUFG
      TX_PHY_CLK_0                    => tx_phy_clk_0, 
      -- Speed indicator for EMAC0
      -- Used in clocking circuitry.
      EMAC0SPEEDIS10100               => speed_vector_0_i,
      -- Local link Receiver Interface - EMAC0
      RX_LL_CLOCK_0                   => ll_clk_0_i,
      RX_LL_RESET_0                   => ll_reset_0_i,
      RX_LL_DATA_0                    => rx_ll_data_0_i,
      RX_LL_SOF_N_0                   => rx_ll_sof_n_0_i,
      RX_LL_EOF_N_0                   => rx_ll_eof_n_0_i,
      RX_LL_SRC_RDY_N_0               => rx_ll_src_rdy_n_0_i,
      RX_LL_DST_RDY_N_0               => rx_ll_dst_rdy_n_0_i,
      RX_LL_FIFO_STATUS_0             => open,

      -- Unused Receiver signals - EMAC0
      EMAC0CLIENTRXDVLD               => EMAC0CLIENTRXDVLD,
      EMAC0CLIENTRXFRAMEDROP          => EMAC0CLIENTRXFRAMEDROP,
      EMAC0CLIENTRXSTATS              => EMAC0CLIENTRXSTATS,
      EMAC0CLIENTRXSTATSVLD           => EMAC0CLIENTRXSTATSVLD,
      EMAC0CLIENTRXSTATSBYTEVLD       => EMAC0CLIENTRXSTATSBYTEVLD,

      -- Local link Transmitter Interface - EMAC0
      TX_LL_CLOCK_0                   => ll_clk_0_i,
      TX_LL_RESET_0                   => ll_reset_0_i,
      TX_LL_DATA_0                    => tx_ll_data_0_i,
      TX_LL_SOF_N_0                   => tx_ll_sof_n_0_i,
      TX_LL_EOF_N_0                   => tx_ll_eof_n_0_i,
      TX_LL_SRC_RDY_N_0               => tx_ll_src_rdy_n_0_i,
      TX_LL_DST_RDY_N_0               => tx_ll_dst_rdy_n_0_i,

      -- Unused Transmitter signals - EMAC0
      CLIENTEMAC0TXIFGDELAY           => CLIENTEMAC0TXIFGDELAY,
      EMAC0CLIENTTXSTATS              => EMAC0CLIENTTXSTATS,
      EMAC0CLIENTTXSTATSVLD           => EMAC0CLIENTTXSTATSVLD,
      EMAC0CLIENTTXSTATSBYTEVLD       => EMAC0CLIENTTXSTATSBYTEVLD,

      -- MAC Control Interface - EMAC0
      CLIENTEMAC0PAUSEREQ             => CLIENTEMAC0PAUSEREQ,
      CLIENTEMAC0PAUSEVAL             => CLIENTEMAC0PAUSEVAL,

 
      -- Clock Signals - EMAC0
      GTX_CLK_0                       => gtx_clk_0_i,
      -- GMII Interface - EMAC0
      GMII_TXD_0                      => GMII_TXD_0,
      GMII_TX_EN_0                    => GMII_TX_EN_0,
      GMII_TX_ER_0                    => GMII_TX_ER_0,
      GMII_TX_CLK_0                   => GMII_TX_CLK_0,
      GMII_RXD_0                      => GMII_RXD_0,
      GMII_RX_DV_0                    => GMII_RX_DV_0,
      GMII_RX_ER_0                    => GMII_RX_ER_0,
      GMII_RX_CLK_0                   => rx_clk_0_i,

 
      MII_TX_CLK_0                    => tx_clk_0,
      GMII_COL_0                      => GMII_COL_0,
      GMII_CRS_0                      => GMII_CRS_0,

        
        
      -- Asynchronous Reset
      RESET                           => reset_i
    );


    -- Please modify the value of the IOBDELAYs according to your design.
    -- For more information on IDELAYCTRL and IODELAY, please refer to
    -- the Virtex-5 User Guide.
    gmii_rxc0_delay : IODELAY
    generic map (
        IDELAY_TYPE    => "FIXED",
        IDELAY_VALUE   => 0,
        DELAY_SRC      => "I",
        SIGNAL_PATTERN => "CLOCK"
	)
    port map (
        IDATAIN    => GMII_RX_CLK_0,
        ODATAIN    => '0',
        DATAOUT    => gmii_rx_clk_0_delay,
        DATAIN     => '0',
        C          => '0',
        T          => '0',
        CE         => '0',
        INC        => '0',
        RST        => '0'
	);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--	Definition of Processes													  --
--                                                                            --
--------------------------------------------------------------------------------

--	Ethernet: Create synchronous reset in the transmitter clock domain.
    gen_ll_reset_emac0 : process (ll_clk_0_i, reset_i)
    begin
      if reset_i = '1' then
        ll_pre_reset_0_i <= (others => '1');
        ll_reset_0_i     <= '1';
      elsif ll_clk_0_i'event and ll_clk_0_i = '1' then
        ll_pre_reset_0_i(0)          <= '0';
        ll_pre_reset_0_i(5 downto 1) <= ll_pre_reset_0_i(4 downto 0);
        ll_reset_0_i                 <= ll_pre_reset_0_i(5);
      end if;
    end process gen_ll_reset_emac0;

    delay0rstgen :process (refclk_bufg_i, reset_i)
    begin
      if (reset_i = '1') then
        idelayctrl_reset_0_r(0)           <= '0';
        idelayctrl_reset_0_r(12 downto 1) <= (others => '1');
      elsif refclk_bufg_i'event and refclk_bufg_i = '1' then
        idelayctrl_reset_0_r(0)           <= '0';
        idelayctrl_reset_0_r(12 downto 1) <= idelayctrl_reset_0_r(11 downto 0);
      end if;
    end process delay0rstgen;


----	for debugging purpose
--	s_mon_1 : OBUF
--	generic map (
--		DRIVE => 12,
----		IOSTANDARD => "LVCMOS33",
--		SLEW => "SLOW")
--	port map (
--		O => signal_monitor_1,     -- Buffer output (connect directly to top-level port)
--		I => rx_ll_sof_n_0_i      -- Buffer input 
--	);
--
----	for debugging purpose
--	s_mon_2 : OBUF
--	generic map (
--		DRIVE => 12,
----		IOSTANDARD => "LVCMOS33",
--		SLEW => "SLOW")
--	port map (
--		O => signal_monitor_2,     -- Buffer output (connect directly to top-level port)
--		I => capture_source_flag      -- Buffer input 
--	);
--
----	for debugging purpose
--	s_mon_3 : OBUF
--	generic map (
--		DRIVE => 12,
----		IOSTANDARD => "LVCMOS33",
--		SLEW => "FAST")
--	port map (
--		O => signal_monitor_3,     -- Buffer output (connect directly to top-level port)
--		I => capture_source_flag      -- Buffer input 
--	);

------	for debugging purpose
----	s_mon_4 : OBUF
----	generic map (
----		DRIVE => 12,
------		IOSTANDARD => "LVCMOS33",
----		SLEW => "FAST")
----	port map (
----		O => signal_monitor_4,     -- Buffer output (connect directly to top-level port)
----		I => ll_clk_0_i      -- Buffer input 
----	);
	

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--		Concurrent Statments												  --
--                                                                            --
--------------------------------------------------------------------------------

--[Andi]
	EthPhy_Reset_n	<= NOT reset_i;
--[/Andi]

    ---------------------------------------------------------------------------
    -- Reset Input Buffer
    ---------------------------------------------------------------------------
	reset_i <= user_reset;
    -- EMAC0 Clocking
    ll_clk_0_i <= tx_client_clk_0;

    idelayctrl_reset_0_i <= idelayctrl_reset_0_r(12);
    rx_ll_dst_rdy_n_0_i     <= tx_ll_dst_rdy_n_0_i;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--		Buffers																  --
--                                                                            --
--------------------------------------------------------------------------------
	-- Instantiate IDELAYCTRL for the IDELAY in Fixed Tap Delay Mode
	dlyctrl0 : IDELAYCTRL port map (
		RDY    => open,
		REFCLK => refclk_bufg_i,
		RST    => idelayctrl_reset_0_i
	);

	-- Put the PHY clocks from the EMAC through BUFGs.
	-- Used to clock the PHY side of the EMAC wrappers.
	bufg_phy_tx_0 : BUFG port map (I => tx_phy_clk_0_o, O => tx_phy_clk_0);
	bufg_phy_rx_0 : BUFG port map (I => gmii_rx_clk_0_delay, O => rx_clk_0_i);

	-- Put the client clocks from the EMAC through BUFGs.
	-- Used to clock the client side of the EMAC wrappers.
	bufg_client_tx_0 : BUFG port map (I => tx_client_clk_0_o, O => tx_client_clk_0);
	bufg_client_rx_0 : BUFG port map (I => rx_client_clk_0_o, O => rx_client_clk_0);

	-- Enable PHYEMAC0MIITXCLK to continue toggling even when MII_TX_CLK_0 signal 
	-- stops toggling due when TEMAC changes to 1 Gbps mode
	bufg_tx_0 : BUFGMUX port map (
		I0 => gtx_clk_0_i,
		I1 => MII_TX_CLK_0,
		S  => speed_vector_0_i,
		O  => tx_clk_0
	); 

	------------------------------------------------------------------------
	-- REFCLK used for RGMII IODELAYCTRL primitive - Need to supply a 200MHz clock
	------------------------------------------------------------------------
	refclk_ibufg : IBUFG port map(I => REFCLK, O => refclk_ibufg_i);
	refclk_bufg  : BUFG  port map(I => refclk_ibufg_i, O => refclk_bufg_i);

	----------------------------------------------------------------------
	-- Stop the tools from automatically adding in a BUFG on the
	-- GTX_CLK_0 line.
	----------------------------------------------------------------------
	--    gtx_clk0_ibuf : IBUF port map (I => GTX_CLK_0, O => gtx_clk_0_i);
	gtx_clk_0_i <= GTX_CLK_0;

--	und wieder mein spass hier

---------------------------------------------------------------------
--  Instatiate the ethernet data module
---------------------------------------------------------------------
	Enet_data_module : ethernet_data_module
	port map (
--	global user reset
		user_reset			=> user_reset,

--	CLOCKING:
	--	clock running
		clk_locked			=> clk_locked,

	--	Ethernet clocking
		rtx_clk				=> ll_clk_0_i,
		rtx_rst				=> ll_reset_0_i,

	--	LCD clocking
		clk_50MHz			=> clk_50MHz,

	--	calculation clocking
		clk_mcfilt			=> clk_mcfilt,

--	Ethernet data + controls
		rx_data_in			=> rx_ll_data_0_i,
		rx_sof_in_n			=> rx_ll_sof_n_0_i,
		rx_eof_in_n			=> rx_ll_eof_n_0_i,
		rx_src_rdy_in_n		=> rx_ll_src_rdy_n_0_i,
		tx_data_out			=> tx_ll_data_0_i,
		tx_sof_out_n		=> tx_ll_sof_n_0_i,
		tx_eof_out_n		=> tx_ll_eof_n_0_i,
		tx_src_rdy_out_n	=> tx_ll_src_rdy_n_0_i,
		tx_dst_rdy_in_n		=> tx_ll_dst_rdy_n_0_i,

--	LCD interface
		lcd_e				=> lcd_e,
		lcd_rs				=> lcd_rs,
		lcd_rw				=> lcd_rw,
		lcd_data			=> lcd_data,

--	LEDs
		leds_nwces			=> leds_nwces,
		leds8				=> leds8,

--	user push buttons
		switches_nwces		=> switches_nwces
    );

end TOP_LEVEL;
