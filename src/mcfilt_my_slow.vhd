library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mcfilt is
	generic (
		LOWESTBIT	: positive
	);
	port (
--	calculation controls
		clk			: in  std_logic;
		rst			: in  std_logic;
		en			: in  std_logic;
--	E: energy samples in
--	S: sums out
		E			: in  std_logic_vector(15 downto 0);
		S			: out std_logic_vector(47 downto LOWESTBIT);
		inval		: out std_logic;
		c_valid		: out std_logic;
--	coefficients upload
		clk_calib	: in  std_logic;
		cena		: in  std_logic; -- Enable: Shift cdin on cclk
		cdin		: in  std_logic; -- data (bit by bit)
		cdout		: out std_logic
	);
end mcfilt;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture impl of mcfilt is

	component chain_rom is
	generic (
		WIDTH	: positive;             -- Data Width
		ABITS	: positive              -- Address Bits
	);
	port (
	-- Configuration
		cclk	: in  std_logic;         -- Clock
		cena	: in  std_logic;         -- Enable: Shift cdin on cclk
		cdin	: in  std_logic;         -- Chain Input
		cdout	: out std_logic;         -- Chain Output

	-- Data Output (Async without cclk and cena)
		addr	: in  std_logic_vector(ABITS-1 downto 0);
		dout	: out std_logic_vector(WIDTH-1 downto 0)
	);
	end component;

	subtype t_X is std_logic_vector(X_GAIN_BIT downto 0); -- input type: 14 bit = 2 bit gain + 12 bit ADC value
	subtype t_A is std_logic_vector(29 downto 0); -- input for DSP <=> Energies (pedestal reduced)
	subtype t_B is std_logic_vector(17 downto 0); -- input for DSP <=> coefficients
	subtype t_P is std_logic_vector(47 downto 0); -- output of DSP <=> products

	type arrX is array(natural range<>) of t_X;
	type arrA is array(natural range<>) of t_A;
	type arrB is array(natural range<>) of t_B;
	type arrP is array(natural range<>) of t_P;

	signal X : arrX(0 to DEPTH-1);
	signal A : arrA(0 to DEPTH-1);-- coefficients
	signal B : arrB(0 to DEPTH-1);-- energies
	signal P : arrP(0 to DEPTH-1);
	signal pedestal : std_logic_vector(PED_SIZE-1 downto 0);
-- will er nicht:
--	signal pedestal : std_logic_vector(E_BIT+1 downto 0);
	
	signal gain : std_logic_vector(GAIN_BIT downto GAIN_BUT);

	signal coeffio	: std_logic_vector(DEPTH downto 0); -- Chain Input: coeffio(0)
	
	constant abits  : positive := sel_abits(CHANNEL_WIDTH + GAIN_WIDTH, 5);

--	signals for validity check: incomming gain sets invalidityin .. corresponds to future output:
	signal invalidityin : std_logic;	-- invalidity of incomming energy
	constant MCFILTDELAY : positive := DSPDELAY + 2;
--	thus it needs to be stored for a while in a shift register
	signal invalidity : std_logic_vector(1 to CHANNELS*(DEPTH-1) + MCFILTDELAY) := (others => '1'); -- all invalid in the beginning
	signal calc_valid : std_logic_vector(1 to MCFILTDELAY) := (others => '0'); -- sr for en flag


	-- Rotating Channel Selector
	--   Implements a short combinational paths by looping through:
	--
	--     0, -1, CHANNELS-2, CHANNELS-1, ..., 1, 0, -1, CHANNELS-2, ...
	--
	--   Use chselof() for the static mapping of the channel number
	--   sequence to ChSel counter states:
	--
	--     0 -> 0, 1 -> -1, x -> CHANNELS-x
	--
	signal ChSel : std_logic_vector(log2ceil(CHANNELS)-1 downto 0);

	function chselof(constant ch : natural) return unsigned is
		variable  res : unsigned(log2ceil(CHANNELS)-1 downto 0);--unsigned(ChSel'range);
	begin
		case ch is
			when 0      => res := (others => '0');
			when 1      => res := (others => '1');
			when others => res := to_unsigned(CHANNELS-ch, res'length);
		end case;
		return  res;
	end chselof;

begin

	c_valid <= calc_valid(calc_valid'right);
	process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				calc_valid <= (others => '0');
			else
				calc_valid <= en & calc_valid(1 to calc_valid'right-1);
			end if;
		end if;
	end process;

--	handling validity check input: 
	invalidityin <= '0' when unsigned(gain) > 0 else '1';
--	optimised invalidS-calculation
	process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
--				invalidity <= (others => '0'); -- has to be '1' in real design
				invalidity <= (others => '1'); -- has to be '1' in real design
			elsif en = '1' then
				-- default: shift register
				invalidity <= invalidityin & invalidity(1 to invalidity'right-1);
				-- watch out for new invalid data: invalid enters DEPTH-1 more times
				for d in 2 to DEPTH loop
					invalidity((d-1)*CHANNELS+1) <= invalidityin or invalidity((d-1)*CHANNELS);
				end loop;
			end if;
		end if;
	end process;
	
	inval <= invalidity(invalidity'right);
		
-- Sum
--   - P(i)     is output of stage i and input to stage i-1
--   - P(0)     is full sum
--	S(S'left downto 16) <= P(0)(P(0)'left downto 16);

	S <= P(0)(47 downto LOWESTBIT);

		
-- Forwarding FIFOs
--   - X(i)   is input to stage i
--   - X(i+1) is X(i) delayed by CHANNELS-1 cycles
-- test: in process
	--X(0)(X_GAIN_BIT downto X_GAIN_BUT) <= E(GAIN_BIT downto GAIN_BUT);
	--X(0)(E_BIT+1 downto 0) <= std_logic_vector(signed('0' & E(E_BIT downto 0)) + signed(pedestal(E_BIT+1 downto 0)));

	gain <= E(GAIN_BIT downto GAIN_BUT);

	input_deal: block
	signal PPedestal : std_logic_vector(E_BIT+1 downto 0);
	signal X_gain : std_logic_vector(GAIN_BIT downto GAIN_BUT);
--	signal X_calc : signed(E_BIT+1 downto 0);
	signal X_calc : std_logic_vector(E_BIT downto 0);
	
	begin
		process(clk)
		begin
			if rising_edge(clk) then
--				X(0)(X_GAIN_BIT downto X_GAIN_BUT) <= gain;--E(GAIN_BIT downto GAIN_BUT);
				X_gain <= gain;
				X(0)(X_GAIN_BIT downto X_GAIN_BUT) <= X_gain;--E(GAIN_BIT downto GAIN_BUT);
		--		X_calc <= signed('0' & E(E_BIT downto 0));
				X_calc <= E(E_BIT downto 0);
				
--				X(0)(E_BIT+1 downto 0) <= std_logic_vector(signed('0' & E(E_BIT downto 0)) + signed(pedestal(E_BIT+1 downto 0)));
				PPedestal <= pedestal(E_BIT+1 downto 0);
--				X(0)(E_BIT+1 downto 0) <= std_logic_vector(signed('0' & E(E_BIT downto 0)) + signed(PPedestal));
				X(0)(E_BIT+1 downto 0) <= std_logic_vector(signed('0' & X_calc) + signed(PPedestal));
			end if;
		end process;
	end block;

	genFrwd: for i in 0 to DEPTH-2 generate
		genSingle: if CHANNELS < 2 generate
			X(i+1) <= X(i);
		end generate genSingle;
		
		genMulti: if CHANNELS >= 2 generate
--		signal fifo : arrX(0 to CHANNELS-2);
		signal fifo : arrX(0 to CHANNELS-2) := (others => (others => '0'));
		begin
			process(clk)
			begin
				if rising_edge(clk) then
					if en = '1' then
						fifo <= X(i) & fifo(fifo'low to fifo'high-1);
					end if;
				end if;
			end process;
			X(i+1) <= fifo(fifo'high);
		end generate genMulti;
	end generate;

-- Rotating Channel Selector
	blkChSel: block
	-- We need a counter that is usually one bit wider as we
	-- use the sign bit for overflow.
	signal ChCnt : signed(log2ceil(CHANNELS-1) downto 0);
	begin
		process(clk)
			begin
			if rising_edge(clk) then
				if rst = '1' then
					ChCnt <= (others => '0');
				elsif en = '1' then
					if ChCnt(ChCnt'left) = '1' then
						ChCnt <= to_signed(CHANNELS-2, ChCnt'length);
					else
						ChCnt <= ChCnt - 1;
					end if;
				end if;
			end if;
		end process;
		ChSel <= std_logic_vector(unsigned(ChCnt(ChSel'length-1 downto 0)));
	end block blkChSel;

	coeffio(0) <= cdin;

	genpeds: block
		signal addr		: std_logic_vector(abits-1 downto 0);
	begin
		pedestals: chain_rom
		generic map (
			WIDTH  => PED_SIZE,			-- Data Width
			ABITS  => abits				-- Address Bits
		)
		port map (
		-- Configuration
			cclk  => clk_calib,			-- Clock
			cena  => cena,				-- Enable: Shift cdin on cclk
			cdin  => coeffio(DEPTH),	-- Chain Input
			cdout => cdout,				-- Chain Output

		-- Data Output (Async without cclk and cena)
			addr => addr,
			dout => pedestal
-- das will er nicht machen:
--			dout(PED_SIZE-1 downto E_BIT+2) => open,
--			dout(E_BIT+1 downto 0) => pedestal
		);

		-- Assemble ROM Address
	--	process(ChSel, X)
		process(ChSel, gain)
		begin
			-- Default for driving possibly unused bits
			addr <= (others => '0');
			-- Re-assign relevant bits
	--		addr(log2ceil(CHANNELS)+1 downto 0) <= ChSel & X(0)(X_GAIN_BIT downto X_GAIN_BUT);
			addr(log2ceil(CHANNELS)+1 downto 0) <= ChSel & gain;
		end process;

	end block genpeds;

-- genInputs has to be generated upon DEPTH as genDSPs is already done

-- Compute A/B Inputs to DSP Slice from X Values
--	ai's are in reverse order: coeffio(i) initialises a(DEPTH-i)
	genInputs: for i in 0 to DEPTH-1 generate
		-- Current Coefficient Address and Value
		signal addr		: std_logic_vector(abits-1 downto 0);
		signal Coeff	: std_logic_vector(A_I_SIZE-1 downto 0);
		
		signal ChSel_delayed1 : std_logic_vector(log2ceil(CHANNELS)-1 downto 0);
		signal ChSel_delayed2 : std_logic_vector(log2ceil(CHANNELS)-1 downto 0);

	begin
		coefficients: chain_rom
		generic map (
			WIDTH  => A_I_SIZE,             -- Data Width
			ABITS  => abits              -- Address Bits
		)
		port map (
		-- Configuration
			cclk  => clk_calib,            -- Clock
			cena  => cena,           -- Enable: Shift cdin on cclk
			cdin  => coeffio(i),          -- Chain Input
			cdout => coeffio(i+1),         -- Chain Output

		-- Data Output (Async without cclk and cena)
			addr => addr,
			dout => Coeff
		);

		-- Assemble ROM Address
		process(ChSel_delayed2, X)
		begin
			-- Default for driving possibly unused bits
			addr <= (others => '0');
			-- Re-assign relevant bits
			addr(log2ceil(CHANNELS)+1 downto 0) <= ChSel_delayed2 & X(i)(X_GAIN_BIT downto X_GAIN_BUT);
		end process;

		process(clk)
		begin
			if rising_edge(clk) then
				if en = '1' then
					ChSel_delayed1 <= ChSel;
					ChSel_delayed2 <= ChSel_delayed1;
				end if;
			end if;
		end process;

		-- Second Factor (Coefficient)
		B(i) <= (17 downto 13 => X(i)(12)) & X(i)(12 downto 0);
--	should be replaced by:
--		B(i) <= (17 downto E_bit+2) => X(i)(E_bit+1)) & X(i)(E_bit+1 downto 0);

		A(i)  <= (29 downto Coeff'length => Coeff(Coeff'left)) & Coeff;

	end generate;
	
	genSingleDSP: if DEPTH = 1 generate
		DSP48E_inst : DSP48E
		generic map (
			ACASCREG => 1,       -- Number of pipeline registers between 
										 -- A/ACIN input and ACOUT output, 0, 1, or 2
			ALUMODEREG => 0,     -- Number of pipeline registers on ALUMODE input, 0 or 1
			AREG => 1,           -- Number of pipeline registers on the A input, 0, 1 or 2
			AUTORESET_PATTERN_DETECT => FALSE, -- Auto-reset upon pattern detect, TRUE or FALSE
			AUTORESET_PATTERN_DETECT_OPTINV => "MATCH", -- Reset if "MATCH" or "NOMATCH" 
			A_INPUT => "DIRECT", -- Selects A input used, "DIRECT" (A port) or "CASCADE" (ACIN port)
			BCASCREG => 1,       -- Number of pipeline registers between B/BCIN input and BCOUT output, 0, 1, or 2
			BREG => 1,           -- Number of pipeline registers on the B input, 0, 1 or 2
			B_INPUT => "DIRECT", -- Selects B input used, "DIRECT" (B port) or "CASCADE" (BCIN port)
			CARRYINREG => 0,     -- Number of pipeline registers for the CARRYIN input, 0 or 1
			CARRYINSELREG => 0,  -- Number of pipeline registers for the CARRYINSEL input, 0 or 1
			CREG => 0,           -- Number of pipeline registers on the C input, 0 or 1
			MASK => X"3FFFFFFFFFFF", -- 48-bit Mask value for pattern detect
			MREG => 1,           -- Number of multiplier pipeline registers, 0 or 1
			MULTCARRYINREG => 0, -- Number of pipeline registers for multiplier carry in bit, 0 or 1
			OPMODEREG => 0,      -- Number of pipeline registers on OPMODE input, 0 or 1
			PATTERN => X"000000000000", -- 48-bit Pattern match for pattern detect
			PREG => 1,           -- Number of pipeline registers on the P output, 0 or 1
			SIM_MODE => "SAFE", -- Simulation: "SAFE" vs "FAST", see "Synthesis and Simulation
										-- Design Guide" for details
			SEL_MASK => "MASK",  -- Select mask value between the "MASK" value or the value on the "C" port
			SEL_PATTERN => "PATTERN", -- Select pattern value between the "PATTERN" value or the value on the "C" port
			SEL_ROUNDING_MASK => "SEL_MASK", -- "SEL_MASK", "MODE1", "MODE2" 
			USE_MULT => "MULT_S", -- Select multiplier usage, "MULT" (MREG => 0), 
										  -- "MULT_S" (MREG => 1), "NONE" (not using multiplier)
			USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect, "PATDET", "NO_PATDET" 
			USE_SIMD => "ONE48" -- SIMD selection, "ONE48", "TWO24", "FOUR12"
		)
		port map (
			ACOUT => open,  -- 30-bit A port cascade output 
			BCOUT => open,  -- 18-bit B port cascade output
			CARRYCASCOUT => open, -- 1-bit cascade carry output
			CARRYOUT => open, -- 4-bit carry output
			MULTSIGNOUT => open, -- 1-bit multiplier sign cascade output
			OVERFLOW => open, -- 1-bit overflow in add/acc output
			P => P(0),       -- 48-bit output
			PATTERNBDETECT => open, -- 1-bit active high pattern bar detect output
			PATTERNDETECT => open, --  1-bit active high pattern detect output
			PCOUT => P(0),  -- 48-bit cascade output
			UNDERFLOW => open, -- 1-bit active high underflow in add/acc output
			A => A(0),          -- 30-bit A data input
			ACIN => (others => '0'),    -- 30-bit A cascade data input
			ALUMODE => "0000", -- 4-bit ALU control input
			B => B(0),          -- 18-bit B data input
			BCIN => (others => '0'),    -- 18-bit B cascade input
			C => (others => '0'),          -- 48-bit C data input
			CARRYCASCIN => '0', -- 1-bit cascade carry input
			CARRYIN => '0', -- 1-bit carry input signal
			CARRYINSEL => "000", -- 3-bit carry select input
			CEA1 => en,      -- 1-bit active high clock enable input for 1st stage A registers
			CEA2 => en,      -- 1-bit active high clock enable input for 2nd stage A registers
			CEALUMODE => en, -- 1-bit active high clock enable input for ALUMODE registers
			CEB1 => en,      -- 1-bit active high clock enable input for 1st stage B registers
			CEB2 => en,      -- 1-bit active high clock enable input for 2nd stage B registers
			CEC => en,      -- 1-bit active high clock enable input for C registers
			CECARRYIN => en, -- 1-bit active high clock enable input for CARRYIN register
			CECTRL => en, -- 1-bit active high clock enable input for OPMODE and carry registers
			CEM => en,       -- 1-bit active high clock enable input for multiplier registers
			CEMULTCARRYIN => en,       -- 1-bit active high clock enable for multiplier carry in register
			CEP => en,       -- 1-bit active high clock enable input for P registers
			CLK => CLK,       -- Clock input
			MULTSIGNIN => '0', -- 1-bit multiplier sign input
			OPMODE => "0000101", -- 7-bit operation mode input
			PCIN => (others => '0'),     -- 48-bit P cascade input 
			RSTA => rst,     -- 1-bit reset input for A pipeline registers
			RSTALLCARRYIN => rst, -- 1-bit reset input for carry pipeline registers
			RSTALUMODE => rst, -- 1-bit reset input for ALUMODE pipeline registers
			RSTB => rst,     -- 1-bit reset input for B pipeline registers
			RSTC => rst,     -- 1-bit reset input for C pipeline registers
			RSTCTRL => rst, -- 1-bit reset input for OPMODE pipeline registers
			RSTM => rst, -- 1-bit reset input for multiplier registers
			RSTP => rst  -- 1-bit reset input for P pipeline registers
		);
	end generate;
	
	genMultipleDSP: if DEPTH > 1 generate
		DSP48E_0 : DSP48E
		generic map (
			ACASCREG => 1,       -- Number of pipeline registers between 
									 -- A/ACIN input and ACOUT output, 0, 1, or 2
			ALUMODEREG => 0,     -- Number of pipeline registers on ALUMODE input, 0 or 1
			AREG => 1,           -- Number of pipeline registers on the A input, 0, 1 or 2
			AUTORESET_PATTERN_DETECT => FALSE, -- Auto-reset upon pattern detect, TRUE or FALSE
			AUTORESET_PATTERN_DETECT_OPTINV => "MATCH", -- Reset if "MATCH" or "NOMATCH" 
			A_INPUT => "DIRECT", -- Selects A input used, "DIRECT" (A port) or "CASCADE" (ACIN port)
			BCASCREG => 1,       -- Number of pipeline registers between B/BCIN input and BCOUT output, 0, 1, or 2
			BREG => 1,           -- Number of pipeline registers on the B input, 0, 1 or 2
			B_INPUT => "DIRECT", -- Selects B input used, "DIRECT" (B port) or "CASCADE" (BCIN port)
			CARRYINREG => 0,     -- Number of pipeline registers for the CARRYIN input, 0 or 1
			CARRYINSELREG => 0,  -- Number of pipeline registers for the CARRYINSEL input, 0 or 1
			CREG => 0,           -- Number of pipeline registers on the C input, 0 or 1
			MASK => X"3FFFFFFFFFFF", -- 48-bit Mask value for pattern detect
			MREG => 1,           -- Number of multiplier pipeline registers, 0 or 1
			MULTCARRYINREG => 0, -- Number of pipeline registers for multiplier carry in bit, 0 or 1
			OPMODEREG => 0,      -- Number of pipeline registers on OPMODE input, 0 or 1
			PATTERN => X"000000000000", -- 48-bit Pattern match for pattern detect
			PREG => 1,           -- Number of pipeline registers on the P output, 0 or 1
			SIM_MODE => "SAFE", -- Simulation: "SAFE" vs "FAST", see "Synthesis and Simulation
									-- Design Guide" for details
			SEL_MASK => "MASK",  -- Select mask value between the "MASK" value or the value on the "C" port
			SEL_PATTERN => "PATTERN", -- Select pattern value between the "PATTERN" value or the value on the "C" port
			SEL_ROUNDING_MASK => "SEL_MASK", -- "SEL_MASK", "MODE1", "MODE2" 
			USE_MULT => "MULT_S", -- Select multiplier usage, "MULT" (MREG => 0), 
									  -- "MULT_S" (MREG => 1), "NONE" (not using multiplier)
			USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect, "PATDET", "NO_PATDET" 
			USE_SIMD => "ONE48" -- SIMD selection, "ONE48", "TWO24", "FOUR12"
		)
		port map (
			ACOUT => open,  -- 30-bit A port cascade output 
			BCOUT => open,  -- 18-bit B port cascade output
			CARRYCASCOUT => open, -- 1-bit cascade carry output
			CARRYOUT => open, -- 4-bit carry output
			MULTSIGNOUT => open, -- 1-bit multiplier sign cascade output
			OVERFLOW => open, -- 1-bit overflow in add/acc output
			P => P(0),       -- 48-bit output
			PATTERNBDETECT => open, -- 1-bit active high pattern bar detect output
			PATTERNDETECT => open, --  1-bit active high pattern detect output
			PCOUT => open,  -- 48-bit cascade output
			UNDERFLOW => open, -- 1-bit active high underflow in add/acc output
			A => A(0),          -- 30-bit A data input
			ACIN => (others => '0'),    -- 30-bit A cascade data input
			ALUMODE => "0000", -- 4-bit ALU control input
			B => B(0),          -- 18-bit B data input
			BCIN => (others => '0'),    -- 18-bit B cascade input
			C => (others => '0'),          -- 48-bit C data input
			CARRYCASCIN => '0', -- 1-bit cascade carry input
			CARRYIN => '0', -- 1-bit carry input signal
			CARRYINSEL => "000", -- 3-bit carry select input
			CEA1 => en,      -- 1-bit active high clock enable input for 1st stage A registers
			CEA2 => en,      -- 1-bit active high clock enable input for 2nd stage A registers
			CEALUMODE => en, -- 1-bit active high clock enable input for ALUMODE registers
			CEB1 => en,      -- 1-bit active high clock enable input for 1st stage B registers
			CEB2 => en,      -- 1-bit active high clock enable input for 2nd stage B registers
			CEC => en,      -- 1-bit active high clock enable input for C registers
			CECARRYIN => en, -- 1-bit active high clock enable input for CARRYIN register
			CECTRL => en, -- 1-bit active high clock enable input for OPMODE and carry registers
			CEM => en,       -- 1-bit active high clock enable input for multiplier registers
			CEMULTCARRYIN => en,       -- 1-bit active high clock enable for multiplier carry in register
			CEP => en,       -- 1-bit active high clock enable input for P registers
			CLK => CLK,       -- Clock input
			MULTSIGNIN => '0', -- 1-bit multiplier sign input
			OPMODE => "0010101", -- 7-bit operation mode input
			PCIN => P(1),     -- 48-bit P cascade input 
			RSTA => rst,     -- 1-bit reset input for A pipeline registers
			RSTALLCARRYIN => rst, -- 1-bit reset input for carry pipeline registers
			RSTALUMODE => rst, -- 1-bit reset input for ALUMODE pipeline registers
			RSTB => rst,     -- 1-bit reset input for B pipeline registers
			RSTC => rst,     -- 1-bit reset input for C pipeline registers
			RSTCTRL => rst, -- 1-bit reset input for OPMODE pipeline registers
			RSTM => rst, -- 1-bit reset input for multiplier registers
			RSTP => rst  -- 1-bit reset input for P pipeline registers
		);

		DSP48E_last : DSP48E
		generic map (
			ACASCREG => 1,       -- Number of pipeline registers between 
									 -- A/ACIN input and ACOUT output, 0, 1, or 2
			ALUMODEREG => 0,     -- Number of pipeline registers on ALUMODE input, 0 or 1
			AREG => 1,           -- Number of pipeline registers on the A input, 0, 1 or 2
			AUTORESET_PATTERN_DETECT => FALSE, -- Auto-reset upon pattern detect, TRUE or FALSE
			AUTORESET_PATTERN_DETECT_OPTINV => "MATCH", -- Reset if "MATCH" or "NOMATCH" 
			A_INPUT => "DIRECT", -- Selects A input used, "DIRECT" (A port) or "CASCADE" (ACIN port)
			BCASCREG => 1,       -- Number of pipeline registers between B/BCIN input and BCOUT output, 0, 1, or 2
			BREG => 1,           -- Number of pipeline registers on the B input, 0, 1 or 2
			B_INPUT => "DIRECT", -- Selects B input used, "DIRECT" (B port) or "CASCADE" (BCIN port)
			CARRYINREG => 0,     -- Number of pipeline registers for the CARRYIN input, 0 or 1
			CARRYINSELREG => 0,  -- Number of pipeline registers for the CARRYINSEL input, 0 or 1
			CREG => 0,           -- Number of pipeline registers on the C input, 0 or 1
			MASK => X"3FFFFFFFFFFF", -- 48-bit Mask value for pattern detect
			MREG => 1,           -- Number of multiplier pipeline registers, 0 or 1
			MULTCARRYINREG => 0, -- Number of pipeline registers for multiplier carry in bit, 0 or 1
			OPMODEREG => 0,      -- Number of pipeline registers on OPMODE input, 0 or 1
			PATTERN => X"000000000000", -- 48-bit Pattern match for pattern detect
			PREG => 1,           -- Number of pipeline registers on the P output, 0 or 1
			SIM_MODE => "SAFE", -- Simulation: "SAFE" vs "FAST", see "Synthesis and Simulation
									-- Design Guide" for details
			SEL_MASK => "MASK",  -- Select mask value between the "MASK" value or the value on the "C" port
			SEL_PATTERN => "PATTERN", -- Select pattern value between the "PATTERN" value or the value on the "C" port
			SEL_ROUNDING_MASK => "SEL_MASK", -- "SEL_MASK", "MODE1", "MODE2" 
			USE_MULT => "MULT_S", -- Select multiplier usage, "MULT" (MREG => 0), 
									  -- "MULT_S" (MREG => 1), "NONE" (not using multiplier)
			USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect, "PATDET", "NO_PATDET" 
			USE_SIMD => "ONE48" -- SIMD selection, "ONE48", "TWO24", "FOUR12"
		)
		port map (
			ACOUT => open,  -- 30-bit A port cascade output 
			BCOUT => open,  -- 18-bit B port cascade output
			CARRYCASCOUT => open, -- 1-bit cascade carry output
			CARRYOUT => open, -- 4-bit carry output
			MULTSIGNOUT => open, -- 1-bit multiplier sign cascade output
			OVERFLOW => open, -- 1-bit overflow in add/acc output
			P => open,       -- 48-bit output
			PATTERNBDETECT => open, -- 1-bit active high pattern bar detect output
			PATTERNDETECT => open, --  1-bit active high pattern detect output
			PCOUT => P(DEPTH-1),  -- 48-bit cascade output
			UNDERFLOW => open, -- 1-bit active high underflow in add/acc output
			A => A(DEPTH-1),          -- 30-bit A data input
			ACIN => (others => '0'),    -- 30-bit A cascade data input
			ALUMODE => "0000", -- 4-bit ALU control input
			B => B(DEPTH-1),          -- 18-bit B data input
			BCIN => (others => '0'),    -- 18-bit B cascade input
			C => (others => '0'),          -- 48-bit C data input
			CARRYCASCIN => '0', -- 1-bit cascade carry input
			CARRYIN => '0', -- 1-bit carry input signal
			CARRYINSEL => "000", -- 3-bit carry select input
			CEA1 => en,      -- 1-bit active high clock enable input for 1st stage A registers
			CEA2 => en,      -- 1-bit active high clock enable input for 2nd stage A registers
			CEALUMODE => en, -- 1-bit active high clock enable input for ALUMODE registers
			CEB1 => en,      -- 1-bit active high clock enable input for 1st stage B registers
			CEB2 => en,      -- 1-bit active high clock enable input for 2nd stage B registers
			CEC => en,      -- 1-bit active high clock enable input for C registers
			CECARRYIN => en, -- 1-bit active high clock enable input for CARRYIN register
			CECTRL => en, -- 1-bit active high clock enable input for OPMODE and carry registers
			CEM => en,       -- 1-bit active high clock enable input for multiplier registers
			CEMULTCARRYIN => en,       -- 1-bit active high clock enable for multiplier carry in register
			CEP => en,       -- 1-bit active high clock enable input for P registers
			CLK => CLK,       -- Clock input
			MULTSIGNIN => '0', -- 1-bit multiplier sign input
			OPMODE => "0000101", -- 7-bit operation mode input
			PCIN => (others => '0'),     -- 48-bit P cascade input 
			RSTA => rst,     -- 1-bit reset input for A pipeline registers
			RSTALLCARRYIN => rst, -- 1-bit reset input for carry pipeline registers
			RSTALUMODE => rst, -- 1-bit reset input for ALUMODE pipeline registers
			RSTB => rst,     -- 1-bit reset input for B pipeline registers
			RSTC => rst,     -- 1-bit reset input for C pipeline registers
			RSTCTRL => rst, -- 1-bit reset input for OPMODE pipeline registers
			RSTM => rst, -- 1-bit reset input for multiplier registers
			RSTP => rst  -- 1-bit reset input for P pipeline registers
		);
	end generate;

	genMultiples: if DEPTH > 2 generate
		genStages: for i in 1 to DEPTH-2 generate
			DSP48E_inst : DSP48E
			generic map (
				ACASCREG => 1,       -- Number of pipeline registers between 
											 -- A/ACIN input and ACOUT output, 0, 1, or 2
				ALUMODEREG => 0,     -- Number of pipeline registers on ALUMODE input, 0 or 1
				AREG => 1,           -- Number of pipeline registers on the A input, 0, 1 or 2
				AUTORESET_PATTERN_DETECT => FALSE, -- Auto-reset upon pattern detect, TRUE or FALSE
				AUTORESET_PATTERN_DETECT_OPTINV => "MATCH", -- Reset if "MATCH" or "NOMATCH" 
				A_INPUT => "DIRECT", -- Selects A input used, "DIRECT" (A port) or "CASCADE" (ACIN port)
				BCASCREG => 1,       -- Number of pipeline registers between B/BCIN input and BCOUT output, 0, 1, or 2
				BREG => 1,           -- Number of pipeline registers on the B input, 0, 1 or 2
				B_INPUT => "DIRECT", -- Selects B input used, "DIRECT" (B port) or "CASCADE" (BCIN port)
				CARRYINREG => 0,     -- Number of pipeline registers for the CARRYIN input, 0 or 1
				CARRYINSELREG => 0,  -- Number of pipeline registers for the CARRYINSEL input, 0 or 1
				CREG => 0,           -- Number of pipeline registers on the C input, 0 or 1
				MASK => X"3FFFFFFFFFFF", -- 48-bit Mask value for pattern detect
				MREG => 1,           -- Number of multiplier pipeline registers, 0 or 1
				MULTCARRYINREG => 0, -- Number of pipeline registers for multiplier carry in bit, 0 or 1
				OPMODEREG => 0,      -- Number of pipeline registers on OPMODE input, 0 or 1
				PATTERN => X"000000000000", -- 48-bit Pattern match for pattern detect
				PREG => 1,           -- Number of pipeline registers on the P output, 0 or 1
				SIM_MODE => "SAFE", -- Simulation: "SAFE" vs "FAST", see "Synthesis and Simulation
											-- Design Guide" for details
				SEL_MASK => "MASK",  -- Select mask value between the "MASK" value or the value on the "C" port
				SEL_PATTERN => "PATTERN", -- Select pattern value between the "PATTERN" value or the value on the "C" port
				SEL_ROUNDING_MASK => "SEL_MASK", -- "SEL_MASK", "MODE1", "MODE2" 
				USE_MULT => "MULT_S", -- Select multiplier usage, "MULT" (MREG => 0), 
											  -- "MULT_S" (MREG => 1), "NONE" (not using multiplier)
				USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect, "PATDET", "NO_PATDET" 
				USE_SIMD => "ONE48" -- SIMD selection, "ONE48", "TWO24", "FOUR12"
			)
			port map (
				ACOUT => open,  -- 30-bit A port cascade output 
				BCOUT => open,  -- 18-bit B port cascade output
				CARRYCASCOUT => open, -- 1-bit cascade carry output
				CARRYOUT => open, -- 4-bit carry output
				MULTSIGNOUT => open, -- 1-bit multiplier sign cascade output
				OVERFLOW => open, -- 1-bit overflow in add/acc output
				P => open,       -- 48-bit output
				PATTERNBDETECT => open, -- 1-bit active high pattern bar detect output
				PATTERNDETECT => open, --  1-bit active high pattern detect output
				PCOUT => P(i),  -- 48-bit cascade output
				UNDERFLOW => open, -- 1-bit active high underflow in add/acc output
				A => A(i),          -- 30-bit A data input
				ACIN => (others => '0'),    -- 30-bit A cascade data input
				ALUMODE => "0000", -- 4-bit ALU control input
				B => B(i),          -- 18-bit B data input
				BCIN => (others => '0'),    -- 18-bit B cascade input
				C => (others => '0'),          -- 48-bit C data input
				CARRYCASCIN => '0', -- 1-bit cascade carry input
				CARRYIN => '0', -- 1-bit carry input signal
				CARRYINSEL => "000", -- 3-bit carry select input
				CEA1 => en,      -- 1-bit active high clock enable input for 1st stage A registers
				CEA2 => en,      -- 1-bit active high clock enable input for 2nd stage A registers
				CEALUMODE => en, -- 1-bit active high clock enable input for ALUMODE registers
				CEB1 => en,      -- 1-bit active high clock enable input for 1st stage B registers
				CEB2 => en,      -- 1-bit active high clock enable input for 2nd stage B registers
				CEC => en,      -- 1-bit active high clock enable input for C registers
				CECARRYIN => en, -- 1-bit active high clock enable input for CARRYIN register
				CECTRL => en, -- 1-bit active high clock enable input for OPMODE and carry registers
				CEM => en,       -- 1-bit active high clock enable input for multiplier registers
				CEMULTCARRYIN => en,       -- 1-bit active high clock enable for multiplier carry in register
				CEP => en,       -- 1-bit active high clock enable input for P registers
				CLK => CLK,       -- Clock input
				MULTSIGNIN => '0', -- 1-bit multiplier sign input
				OPMODE => "0010101", -- 7-bit operation mode input
				PCIN => P(i+1),     -- 48-bit P cascade input 
				RSTA => rst,     -- 1-bit reset input for A pipeline registers
				RSTALLCARRYIN => rst, -- 1-bit reset input for carry pipeline registers
				RSTALUMODE => rst, -- 1-bit reset input for ALUMODE pipeline registers
				RSTB => rst,     -- 1-bit reset input for B pipeline registers
				RSTC => rst,     -- 1-bit reset input for C pipeline registers
				RSTCTRL => rst, -- 1-bit reset input for OPMODE pipeline registers
				RSTM => rst, -- 1-bit reset input for multiplier registers
				RSTP => rst  -- 1-bit reset input for P pipeline registers
			);
		end generate;
	end generate;

end impl;
