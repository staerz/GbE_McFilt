----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- 
-- Create Date:		10:48:41 02/01/2010
--
-- Module Name:		LCD_driver_top - Behavioral
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		Basic LCD driver using the 4-bit interface of this board
--					Chars can be written easily at any position indicated
--					as well as strings (up to 16 chars length)
--
--					LCD can be displayed at LEDs (times might be to short to see)
--
-- Dependencies:	requires LCD.vhd as level-0-driver
--					see entity LCD_driver for I/O port discription
--					clk : clock with 50MHz required
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LCD_driver is
	port(
		reset	: in std_logic;	--	reset to clear display
		clk		: in std_logic;	--	clock signal: 50MHz required

		posx	: in integer range 0 to 16;	--	X-Position on the LCD
		posy	: in integer range 0 to 2;	--	Y-Position on the LCD
		text	: in string (1 to 16);	--	string to be displayed
		symbol	: in std_logic_vector(7 downto 0);--in character;	--	char to be displayed (low priority)
		flag	: in std_logic;	--	input flag to indicate new data transfer
		
		SF_D	: out std_logic_vector(3 downto 0);	--	output data to LCD
		LCD_E	: out std_logic;	--	control command to LCD
		LCD_RS	: out std_logic;	--	control command to LCD 
		LCD_RW	: out std_logic;	--	control command to LCD
		LED		: out std_logic_vector(7 downto 0)	--	visual effect to show LCD activity
	);

end LCD_driver;

architecture Behavioral of LCD_driver is

--	Level_0 LCD panel driver
component lcd is
	port(
		reset		: in std_logic;	--	reset to clear display
		clk			: in std_logic;	--	clock signal: 50MHz required
		symbol		: in std_logic_vector(7 downto 0);--character;	--	char to be displayed at LCD
		lcdx		: integer range 0 to 16;	--	X-Position on the LCD
		lcdy		: in integer range 0 to 2;	--	Y-Position on the LCD
		write_flag	: in std_logic;	--	flag for writing mode
		pos_flag	: in std_logic;	--	flag for positionning mode
		SF_D		: out std_logic_vector(3 downto 0);	--	output data to LCD
		LCD_E		: out std_logic;	--	control command to LCD
		LCD_RS		: out std_logic;	--	control command to LCD 
		LCD_RW		: out std_logic;	--	control command to LCD
		LED			: out std_logic_vector(7 downto 0)	--	visual effect to show LCD activity
	);
end component;

--	LCD panel transfer variables:
--	positionning
	signal save_posx	: integer range 0 to 16 := 0;
	signal lcdx			: integer range 0 to 16 := 0;
	signal save_posy	: integer range 0 to 2 := 0;
	signal lcdy			: integer range 0 to 2 := 0;
--	signal dispx		: integer range 0 to 17 := 0;
--	chars
	signal save_symbol	: std_logic_vector(7 downto 0) := x"00";
	signal char			: std_logic_vector(7 downto 0) := x"00";
	signal next_char	: std_logic_vector(7 downto 0) := x"00";
--	strings
	signal save_string	: string (1 to 16);
--	text postion within string
	signal text_pos		: integer range 0 to 16 := 0;
--	flags for LCD panel timing
	signal display_flag			: std_logic := '0';
	signal write_flag			: std_logic := '0';
	signal pre_write_flag		: std_logic := '0';
	signal pos_flag				: std_logic := '0';
	signal pre_pos_flag			: std_logic := '0';
--	signal reset_position_flag	: std_logic := '0';
	signal writing_mode			: std_logic := '0';
	signal next_writing_mode	: std_logic := '0';
--	timing variables and contants
	signal disptimer		: integer := 0;
	constant one_microsec	: integer := 50;
	--constant one_microsec : integer := 125;
	constant disptime		: integer := 45*one_microsec; -- display reaction time

	function char_to_byte(char: character) return std_logic_vector is
	begin
		return CONV_STD_LOGIC_vector(character'pos(char),8);
	end char_to_byte;
	
begin

	lcd_control : lcd
------------------------------  <-  80 chars  ->  --------------------------------
--                                                                              --
--         basic LCD driver module                                              --
--                                                                              --
--  Required Input:	reset, clock, textposition (lcdx, lcdy), text (symbol),     --
--                  write_flag: to write text (for the next comming char),      --
--                  pos_flag: to move cursor to pos (for the next comming time),--
--                                                                              --
--  Given Output:	sf_d(3 downto 0): data bits to LCD panel,                   --
--                  lcd_e, lcd_rs, lcd_rw: controlling bits to LCD panel,       --
--                  optional: LED to indicate display activity                  --
--                                                                              --
----------------------------------------------------------------------------------
	port map (
		reset				=> reset,
		clk					=> clk,
		symbol				=> char,
		lcdy				=> lcdy,
		lcdx				=> lcdx,
		write_flag			=> write_flag,
		pos_flag			=> pos_flag,
		SF_D(3 downto 0)	=> SF_D(3 downto 0),
		LCD_E				=> lcd_e,
		LCD_RS				=> lcd_rs,
		LCD_RW				=> lcd_rw,
--		LED					=> open
		LED					=> LED
	);

--	LED <= CONV_STD_LOGIC_VECTOR(dispx, 8);

----------------------------------------------------------------------------------
--	handling data transfer to LCD panel
----------------------------------------------------------------------------------
	writing: process (clk) is
	begin
----------------------------------------------------------------------------------
--	act on clock signal only:
----------------------------------------------------------------------------------
		if	(rising_edge(clk)) then
			if (reset = '1') then
--	reset all important timers, positions and flags
				disptimer <= 0;
				pre_write_flag <= '0';
				writing_mode <= '0';
				next_writing_mode <= '0';
			else
----------------------------------------------------------------------------------
--	load new data on input flag:
--		time to write next text
----------------------------------------------------------------------------------
				if flag = '1' then
					save_string <= text;
					save_posx <= posx;
					save_posy <= posy;
					save_symbol <= symbol;
--	activate writing mode
					writing_mode <= '1';
					next_writing_mode <= '1';
				else
--	deactivation is done after printing all symbols/text
				end if;

----------------------------------------------------------------------------------
--	reset text_pos when writing mode is off
--	note: text_pos indicates the position in strings to write
----------------------------------------------------------------------------------
				if writing_mode = '0' then
					text_pos <= 0;
				end if;

----------------------------------------------------------------------------------
--	let time pass (disptimer gives flags for new commands to display)
----------------------------------------------------------------------------------
				if disptimer > disptime then
----------------------------------------------------------------------------------
--	transmit new data to display
----------------------------------------------------------------------------------
					char <= next_char;
					lcdx <= save_posx;
					lcdy <= save_posy;
----------------------------------------------------------------------------------
--	increase text_position to select next char in strings
----------------------------------------------------------------------------------
					text_pos <= text_pos + 1;
					
					disptimer <= 0;
					
----------------------------------------------------------------------------------
--	set display_flag to know being in perfect timing
----------------------------------------------------------------------------------
					display_flag <= '1';
				else
----------------------------------------------------------------------------------
--	go on time counting
----------------------------------------------------------------------------------
					disptimer <= disptimer + 1;
					display_flag <= '0';
				end if;
			end if;

--			if (text_pos = 0) or (reset_position_flag = '1') then
			if (text_pos = 0) then
----------------------------------------------------------------------------------
--	for text_pos = 0 it's positionning mode:
--	select pre-positioning flag and pre-writing flag correspondingly
--		if you ever see this question mark, you have changed the basic driver...
----------------------------------------------------------------------------------
				next_char <= char_to_byte('?');

				if (save_posx = 0) or (save_posy = 0) then
----------------------------------------------------------------------------------
--	position needs to be specified other than 0 to locate it on the display!
----------------------------------------------------------------------------------
					pre_pos_flag <= '0';
				else
					pre_pos_flag <= '1';
				end if;
				pre_write_flag <= '0';
			elsif (text_pos > 0) and (text_pos <= 16) then
----------------------------------------------------------------------------------
--	now the main procedure handling char selection and positionning follows:
--
--	error discrimination: cursor needs to be within display
--
--	select what to write to display next (char or string)
--
--	first choice: string: if non NUL then print it
----------------------------------------------------------------------------------
				if save_string(1) /= NUL then
					if save_string(text_pos) /= NUL then
						next_char <= char_to_byte(save_string(text_pos));
					else
						next_writing_mode <= '0';
					end if;
				elsif (save_symbol /= char_to_byte(NUL)) and (text_pos = 1) then
----------------------------------------------------------------------------------
--	second choice: char: if non NUL then print it
----------------------------------------------------------------------------------
					next_char <= save_symbol;
				else
----------------------------------------------------------------------------------
--	otherwise: nothing to write
----------------------------------------------------------------------------------
					next_writing_mode <= '0';
				end if;
----------------------------------------------------------------------------------
--	for 0 < text_pos <= 16 it's writing mode:
--	select pre-positioning flag and pre-writing flag correspondingly
----------------------------------------------------------------------------------
				pre_pos_flag <= '0';
				pre_write_flag <= '1';
			else
				next_writing_mode <= '0';
				pre_pos_flag <= '0';
				pre_write_flag <= '0';
			end if;
----------------------------------------------------------------------------------
--	remeber writing mode for next cycle
----------------------------------------------------------------------------------
			writing_mode <= next_writing_mode;
----------------------------------------------------------------------------------
--	now here comes the ultimate confusion:
--	first you send the flag to the display, and on the next display-time only
--	the text (or new position coordinates) will be transmitted
--	(see LCD.vhd ("when done =>"))
----------------------------------------------------------------------------------
			write_flag <= next_writing_mode and pre_write_flag and display_flag;
			pos_flag <= next_writing_mode and pre_pos_flag and display_flag;
--			if pos_flag = '1' then
--				dispx <= save_posx;
----				reset_position_flag <= '0';
--			elsif write_flag = '1' then
--				dispx <= dispx + 1;
--				if dispx > 16 then
--					reset_position_flag <= '1';
--					if text_pos > 1 then-- >0 ?
--						text_pos <= text_pos-1;
--					end if;
--					if save_posy = 0 then
--						save_posy <= 1;
--					end if;
--					save_posx <= 1;
--					dispx <= 1;
--				else
--					reset_position_flag <= '0';
--				end if;
--			else
----				reset_position_flag <= '0';
--			end if;

		end if;
	end process writing;

end Behavioral;

