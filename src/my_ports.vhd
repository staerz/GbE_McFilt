library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package myports is
	-- external: calculate # of bits to encode number
	function log2ceilp(arg : positive) return natural;

-- RAM read/write constants
	-- write:
	constant w_bus_cnt		: positive := 2; -- # of concurrent writing modules
	constant w_addr_width	: positive := 8; -- write address bus width
	constant w_data_width	: positive := 64; -- write data bus width

	-- read:
	constant r_bus_cnt		: positive := 4; -- # of concurrent reading modules
	constant r_addr_width	: positive := 8; -- read address bus width
	constant r_data_width	: positive := 64; -- read data bus width

-- derived RAM manager port definition
	type t_w_addr is array(natural range <>) of std_logic_vector(w_addr_width-1 downto 0);
	type t_w_data is array(natural range <>) of std_logic_vector(w_data_width-1 downto 0);

	type t_r_addr is array(natural range <>) of std_logic_vector(r_addr_width-1 downto 0);

	constant write_cnt	: positive := w_bus_cnt;
	constant read_cnt	: positive := r_bus_cnt;

	constant E_bit_width : positive := 12;

-- L0 trigger sum ports (11 == E_bit)
	type t_E_bus is array(natural range <>) of std_logic_vector(E_bit_width-1 downto 0);

end myports;

package body myports is
--	calculate the number of bits required to encryp arg binarily
	function log2ceilp(arg : positive) return natural is
		variable tmp : positive;
		variable log : natural;
	begin
		tmp := 1;
		log := 0;
		-- satisfy synthesis tools: prevent warnings about loop iterations
		if arg > 1 then
			while arg > tmp loop
				tmp := tmp * 2;
				log := log + 1;
			end loop;
		end if;

		return log;
	end;
end package body;
