--------------------------------------------------------------------------------
-- Copyright (c) 2010                                                         --
-- Technische Universit�t Dresden, Dresden, Germany                           --
--                                                                            --
-- Author:			Steffen St�rz, IKTP TU Dresden, Germany                   --
--------------------------------------------------------------------------------
--
-- Create Date:		14:48:41 26/07/2010
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--	- clocked RAM manager controlling a 256 times 64bit dual port block RAM
--	- both read and write requests are dealt via priority list
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RAM_management is
	port (
--	controls
		clk				: in  std_logic;
		rst				: in  std_logic;

--	RAM requests of calibration: read only
		r_flag_calib	: in std_logic;
		r_addr_calib	: in std_logic_vector(7 downto 0);
		r_fine_calib	: out std_logic;

--	RAM read requests of UDP-module:
		r_flag_enet		: in std_logic;
		r_addr_enet		: in std_logic_vector(7 downto 0);
		r_fine_enet		: out std_logic;

--	RAM write requests of UDP-module:
		w_flag_enet		: in std_logic;
		w_addr_enet		: in std_logic_vector(7 downto 0);
		w_data_enet		: in std_logic_vector(63 downto 0);
		w_fine_enet		: out std_logic;

--	RAM read requests of McFilt:
		r_flag_mcfilt	: in std_logic;
		r_addr_mcfilt	: in std_logic_vector(7 downto 0);
		r_fine_mcfilt	: out std_logic;

--	RAM write requests of McFilt:
		w_flag_mcfilt	: in std_logic;
		w_addr_mcfilt	: in std_logic_vector(7 downto 0);
		w_data_mcfilt	: in std_logic_vector(63 downto 0);
		w_fine_mcfilt	: out std_logic;

--	RAM requests of LCD module: read only
		r_flag_lcd		: in std_logic;	-- in read demand
		r_addr_lcd		: in std_logic_vector(7 downto 0);	-- in read @
		r_fine_lcd		: out std_logic; -- out read accepted


--	RAM write requests of McFilt:
		w_flag_a	: in std_logic;
		w_addr_a	: in std_logic_vector(7 downto 0);
		w_data_a	: in std_logic_vector(63 downto 0);
		w_fine_a	: out std_logic;
--	RAM write requests of McFilt:
		w_flag_b	: in std_logic;
		w_addr_b	: in std_logic_vector(7 downto 0);
		w_data_b	: in std_logic_vector(63 downto 0);
		w_fine_b	: out std_logic;
--	RAM write requests of McFilt:
		w_flag_c	: in std_logic;
		w_addr_c	: in std_logic_vector(7 downto 0);
		w_data_c	: in std_logic_vector(63 downto 0);
		w_fine_c	: out std_logic;
--	RAM requests of LCD module: read only
		r_flag_c		: in std_logic;	-- in read demand
		r_addr_c		: in std_logic_vector(7 downto 0);	-- in read @
		r_fine_c		: out std_logic; -- out read accepted


--	RAM data output
		r_data			: out std_logic_vector(63 downto 0)
	);
end RAM_management;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of RAM_management is
----- SIMULATION START
--
----	simulation counter
--	signal global_count			: integer range 0 to 1000 := 0;
--
----	controls
--	signal clk				: std_logic;
--	signal rst				: std_logic;
--
----	RAM read requests of calibration
--	signal r_flag_calib	: std_logic;
--	signal r_addr_calib	: std_logic_vector(7 downto 0);
--	signal r_fine_calib	: std_logic;
--
----	RAM read requests of UDP module
--	signal r_flag_enet		: std_logic;
--	signal r_addr_enet		: std_logic_vector(7 downto 0);
--	signal r_fine_enet		: std_logic;
--
----	RAM write requests of UDP module
--	signal w_flag_enet		: std_logic;
--	signal w_addr_enet		: std_logic_vector(7 downto 0);
--	signal w_data_enet		: std_logic_vector(63 downto 0);
--	signal w_fine_enet		: std_logic;
--
----	RAM read requests of McFilt module
--	signal r_flag_mcfilt	: std_logic;
--	signal r_addr_mcfilt	: std_logic_vector(7 downto 0);
--	signal r_fine_mcfilt	: std_logic;
--
----	RAM write requests of McFilt module
--	signal w_flag_mcfilt	: std_logic;
--	signal w_addr_mcfilt	: std_logic_vector(7 downto 0);
--	signal w_data_mcfilt	: std_logic_vector(63 downto 0);
--	signal w_fine_mcfilt	: std_logic;
--
----	RAM read requests of LCD module
--	signal r_flag_lcd		: std_logic;	-- in read demand
--	signal r_addr_lcd		: std_logic_vector(7 downto 0);	-- in read @
--	signal r_fine_lcd		: std_logic; -- out read accepted
--
----	RAM read data output
--	signal r_data			: std_logic_vector(63 downto 0);
--
----- SIMULATION END

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--      RAM Block managing signals                                            --
--                                                                            --
--------------------------------------------------------------------------------

	constant RAM_reg	: positive := 1;--2 for RAM with registers, 1 without

	type t_r_selector is (McFilt, Enet, calib, LCD, none, c);
	type t_r_selector_sr is array(1 to RAM_reg) of t_r_selector;
	
--	direct RAM access:
	-- write:
	signal w_flag		: std_logic := '0';
	signal w_ena_flag	: std_logic_vector(0 downto 0) := "0";
	signal w_addr		: std_logic_vector(7 downto 0) := x"00";
	signal w_data		: std_logic_vector(63 downto 0) := x"00_00_00_00_00_00_00_00";
	-- read:
	signal r_flag		: std_logic := '0';
	signal r_addr		: std_logic_vector(7 downto 0) := x"00";
--	muxing requests
	signal in_selector	: t_r_selector := none;
	signal r_selector	: t_r_selector_sr := (others => none);

	type t_w_selector is (Enet, McFilt, none, a, b, c);

	signal out_selector	: t_w_selector := none;
	signal w_selector	: t_w_selector := none;

	component blk_mem_gen_v3_3_no_reg is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         256x 64 bit black box RAM module (see Xilinx documentation)        --
--                                                                            --
--------------------------------------------------------------------------------
	port (
		clka	: IN std_logic;
		ena		: IN std_logic;
		wea		: IN std_logic_vector(0 downto 0);
		addra	: IN std_logic_vector(7 downto 0);
		dina	: IN std_logic_vector(63 downto 0);
		clkb	: IN std_logic;
--		rstb	: IN std_logic;
		enb		: IN std_logic;
		addrb	: IN std_logic_vector(7 downto 0);
		doutb	: OUT std_logic_vector(63 downto 0)
	);
	end component blk_mem_gen_v3_3_no_reg;

begin

	memory: blk_mem_gen_v3_3_no_reg
	port map (
		clka	=> clk,
		ena		=> w_flag,
		wea		=> w_ena_flag,
		addra	=> w_addr,
		dina	=> w_data,
		clkb	=> clk,
--		rstb	=> rst,
		enb		=> r_flag,
		addrb	=> r_addr,
		doutb	=> r_data
	);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          RAM write requests are treated here                               --
--                                                                            --
--------------------------------------------------------------------------------

--  a registered response is introduced for back checking of modules
	RAM_write_selection: process(clk)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				w_selector <= none;--(others => none);
			else
				w_selector <= out_selector;-- & w_selector(0 to RAM_reg-1);
			end if;
		end if;
	end process;

--	priorised choice list of write requests
	out_selector <=
		McFilt	when w_flag_mcfilt	= '1' else
		Enet	when w_flag_enet	= '1' else
		a	when w_flag_a	= '1' else
		b	when w_flag_b	= '1' else
		c	when w_flag_c	= '1' else
		none;

--	generate feedback to outputs
	w_fine_mcfilt	<= '1' when w_selector = mcfilt	else '0';
	w_fine_enet		<= '1' when w_selector = enet	else '0';
	w_fine_a		<= '1' when w_selector = a	else '0';
	w_fine_b		<= '1' when w_selector = b	else '0';
	w_fine_c		<= '1' when w_selector = c	else '0';
	
--	set ram write address accordingly
	with out_selector select w_addr <=
		w_addr_mcfilt	when mcfilt,
		w_addr_enet		when enet,
		w_addr_a		when a,
		w_addr_b		when b,
		w_addr_c		when c,
		(others => '0')	when none;

--	set ram write data accordingly
	with out_selector select w_data <=
		w_data_mcfilt	when mcfilt,
		w_data_enet		when enet,
		w_data_a		when a,
		w_data_b		when b,
		w_data_c		when c,
		(others => '0')	when none;

--	set actual ram write flag accordingly
	with out_selector select w_flag <=
		'0' when none,
		'1' when others;
	
--	set actual ram write enable flag accordingly
	with w_flag select w_ena_flag <=
		"1" when '1',
		"0" when others;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          RAM read requests are treated here                                --
--                                                                            --
--------------------------------------------------------------------------------

--  a delay of 2 clock cycles is introduced due to pipelined RAM reading
	single_reg: if RAM_reg = 1 generate
	begin
		RAM_read_selection: process(clk)
		begin
			if rising_edge(clk) then
				if (rst = '1') then
					r_selector <= (others => none);
				else
					r_selector(1) <= in_selector;
				end if;
			end if;
		end process;
	end generate;
	
	multiple_reg: if RAM_REG > 1 generate
	begin
		RAM_read_selection: process(clk)
		begin
			if rising_edge(clk) then
				if (rst = '1') then
					r_selector <= (others => none);
				else
					r_selector <= in_selector & r_selector(1 to RAM_reg-1);
				end if;
			end if;
		end process;
	end generate;

--	priorised choice list of read requests
	in_selector <=
		mcfilt	when r_flag_mcfilt	= '1' else
		enet	when r_flag_enet	= '1' else
		calib	when r_flag_calib	= '1' else
		lcd		when r_flag_lcd		= '1' else
		c		when r_flag_c		= '1' else
		none;

--	generate feedback to outputs
	r_fine_mcfilt	<= '1' when r_selector(RAM_reg) = mcfilt	else '0';
	r_fine_enet		<= '1' when r_selector(RAM_reg) = enet	else '0';
	r_fine_calib	<= '1' when r_selector(RAM_reg) = calib	else '0';
	r_fine_lcd		<= '1' when r_selector(RAM_reg) = lcd		else '0';
	r_fine_c		<= '1' when r_selector(RAM_reg) = c		else '0';

--	set actual ram read enable flag accordingly
	with in_selector select	r_flag <=
		'0' when none,
		'1' when others;
--r_flag <= '1';
			
--	set actual ram read address accordingly
	with in_selector select	r_addr <= 
		r_addr_mcfilt	when mcfilt,
		r_addr_enet		when enet,
		r_addr_calib	when calib,
		r_addr_lcd		when lcd,
		r_addr_c		when c,
		(others => '0')	when none;

----- BEGIN SIMULATION
---- Clock process definitions
--clk_n_count_process :process 
--begin
--	clk <= '1';
--	global_count <= global_count + 1;
--	wait for 4ns;
--	clk <= '0';
--	wait for 4ns;
--end process;
--
--with global_count select rst <=
--	'1' when 1,
--	'0' when others;
--
--with global_count select r_flag_mcfilt <=
--	'1' when 9 | 26 | 27 | 49 | 50 | 72 | 77,
--	'0' when others;
--   
--with global_count select r_addr_mcfilt <=
--	x"12" when 9 | 26 | 27 | 49 | 50 | 72 | 77,
--	x"00" when others;
--   
--with global_count select r_flag_lcd <=
--	'1' when 9 | 13 | 20 | 22,
--	'0' when others;
--   
--with global_count select r_addr_lcd <=
--	x"f4" when 9 | 13 | 20 | 22,
--	x"00" when others;
--
--with global_count select r_flag_calib <=
--	'1' when 9 | 15 | 25,
--	'0' when others;
--   
--with global_count select r_addr_calib <=
--	x"f1" when 9 | 15 | 25,
--	x"00" when others;
--
--with global_count select r_flag_enet <=
--	'1' when 12 | 16 | 20 | 23,
--	'0' when others;
--   
--with global_count select r_addr_enet <=
--	x"41" when 12 | 16 | 20 | 23,
--	x"00" when others;
--
---- init data: write data to RAM
--with global_count select w_addr_mcfilt <=
--	x"71" when 6,
--	x"f4" when 8,
--	x"41" when 7,
--	x"00" when others;
--
--with global_count select w_data_mcfilt <=
--	x"1234_1234_1234_1234" when 6,
--	x"7890_7890_7890_7890" when 8,
--	x"ffff_ffff_ffff_ffff" when 7,
--	x"0000_0000_0000_0000" when others;
--
--with global_count select w_flag_mcfilt <=
--	'1' when 6 to 8,
--	'0' when others;
--
---- test enet write
--with global_count select w_addr_enet <=
--	x"12" when 2,
--	x"45" when 3,
--	x"23" when 6,
--	x"00" when others;
--
--with global_count select w_data_enet <=
--	x"4567_4567_4567_4567" when 2,
--	x"1234_5678_90ab_cdef" when 3,
--	x"1234_5678_90ab_cdef" when 6,
--	x"0000_0000_0000_0000" when others;
--
--with global_count select w_flag_enet <=
--	'1' when 2 | 3 | 6,
--	'0' when others;
--
----- END SIMULATION

end Behavioral;