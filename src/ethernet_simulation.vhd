library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ethernet_simulation is
end ethernet_simulation;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

library my_definitions;
use my_definitions.mytypes.all;

architecture Behavioral of ethernet_simulation is

	signal clk_reset	: std_logic := '0';	-- DCM reset signal
	signal clk			: std_logic;
	signal rtx_clk		: std_logic;
	signal clk_50MHz	: std_logic;
	signal clk_mcfilt	: std_logic;
	signal clk_locked	: std_logic := '0';	-- high = clocking runs

	COMPONENT clocking
	port (
		CLKIN1_IN	: in  std_logic; 
		RST_IN		: in  std_logic; 
		CLKOUT0_OUT	: out std_logic; 
		CLKOUT1_OUT	: out std_logic; 
		CLKOUT2_OUT	: out std_logic; 
		LOCKED_OUT	: out std_logic
	);
	END COMPONENT;

	component ethernet_data_module is
	port (
--	global user reset
		user_reset			: out std_logic; -- global user reset

--	CLOCKING:
	--	clock running
		clk_locked			: in std_logic; -- high = clock running

	--	Ethernet clocking
		rtx_clk				: in  std_logic; -- Input CLK from TRIMAC Reciever
		rtx_rst				: in  std_logic; -- Synchronous reset signal

	--	LCD clocking
		clk_50MHz			: in  std_logic; -- Input CLK 50MHz

	--	calculation clocking
		clk_mcfilt			: in  std_logic; -- Input high speed CLK (approx. 400MHz)

--	Ethernet data + controls
		rx_ll_data_in		: in  std_logic_vector(7 downto 0); -- Input data
		rx_ll_sof_in_n		: in  std_logic; -- Input start of frame
		rx_ll_eof_in_n		: in  std_logic; -- Input end of frame
		rx_ll_src_rdy_in_n 	: in  std_logic; -- Input source ready
		tx_ll_data_out		: out std_logic_vector(7 downto 0); -- Modified output data
		tx_ll_sof_out_n		: out std_logic; -- Output start of frame						
		tx_ll_eof_out_n		: out std_logic; -- Output end of frame							
		tx_ll_src_rdy_out_n	: out std_logic; -- Output source ready
		tx_ll_dst_rdy_in_n	: in  std_logic; -- Input destination ready	-- check

--	LCD interface
		lcd_e				: out std_logic;	-- LCD enable
		lcd_rs				: out std_logic;	-- LCD control
		lcd_rw				: out std_logic;	-- LCD control
		lcd_data			: inout std_logic_vector(7 downto 4);	-- LCD data

--	LEDs
		leds_nwces			: out std_logic_vector(4 downto 0);
		leds8				: out std_logic_vector(0 to 7);

--	user push buttons
		switches_nwces		: in std_logic_vector(4 downto 0)
	);
	end component;

	signal rtx_rst				: std_logic;

--	Ethernet data + controls
	signal rx_ll_data_in		: std_logic_vector(7 downto 0); -- Input data
	signal rx_ll_sof_in_n		: std_logic; -- Input start of frame
	signal rx_ll_eof_in_n		: std_logic; -- Input end of frame
	signal rx_ll_src_rdy_in_n 	: std_logic; -- Input source ready
	signal tx_ll_data_out		: std_logic_vector(7 downto 0); -- Modified output data
	signal tx_ll_sof_out_n		: std_logic; -- Output start of frame						
	signal tx_ll_eof_out_n		: std_logic; -- Output end of frame							
	signal tx_ll_src_rdy_out_n	: std_logic; -- Output source ready
	signal tx_ll_dst_rdy_in_n	: std_logic; -- Input destination ready	-- check

----	RAM requests from LCD module: read only
--	signal r_flag_lcd			: std_logic;	-- in read demand
--	signal r_addr_lcd			: std_logic_vector(7 downto 0);	-- in read @
--	signal r_fine_lcd			: std_logic; -- out read permission
--	signal r_data_lcd			: std_logic_vector(63 downto 0);	-- out read data
--		
----	communication ports LCD <-> Enet module
--	signal capture_source_flag	: std_logic;	-- SRC MAC @ was found
--	signal capture_dest_flag	: std_logic;	-- DST MAC @ was found
--	signal mac_readout_flag		: std_logic;	-- MACs have been read out
		
--	external stimuli
	signal user_reset			: std_logic;
--	LEDs
	signal leds_nwces			: std_logic_vector(4 downto 0);
	signal leds8				: std_logic_vector(0 to 7);

--	user push buttons
	signal switches_nwces		: std_logic_vector(4 downto 0);

--	SIMULATION counter
	signal global_count			: unsigned(20 downto 0):= (others => '0');
	signal fast_count			: unsigned(31 downto 0):= (others => '0');
	signal count_100			: unsigned(31 downto 0):= (others => '0');

--constant local_ram_coeff_bytes : integer := 16;--544;
constant calistart : integer := 16;
constant caliend : integer := calistart+to_integer(ram_coeff_bytes);
constant datainstart : integer := 4500; --220
constant datainend : integer := datainstart + 4*10*2;
constant outputstart : integer := datainstart + 100;

constant ABITS : positive := 5;

---	signals for LCD_top_driver:
	signal lcd_e		: std_logic;
	signal lcd_rs		: std_logic;
	signal lcd_rw		: std_logic;
	signal lcd_data		: std_logic_vector(7 downto 4);
	
--	signal count		: unsigned(20 downto 0) := (others => '0');

begin

	Clocking_inst: clocking
	PORT MAP(
		CLKIN1_IN	=> clk,
		RST_IN		=> clk_reset,
		CLKOUT0_OUT	=> rtx_clk,--GTX_CLK_0,--clk_125MHz,
		CLKOUT1_OUT	=> clk_50MHz,
		CLKOUT2_OUT	=> clk_mcfilt,
		LOCKED_OUT	=> clk_locked
	);

	simulation: ethernet_data_module
	port map (
--	global user reset
		user_reset			=> user_reset, -- global user reset

--	CLOCKING:
	--	clock running
		clk_locked			=> clk_locked, -- high = clock running

	--	Ethernet clocking
		rtx_clk				=> rtx_clk,
		rtx_rst				=> rtx_rst,

	--	LCD clocking
		clk_50MHz			=> clk_50MHz,

	--	calculation clocking
		clk_mcfilt			=> clk_mcfilt,

--	Ethernet data + controls
		rx_ll_data_in		=> rx_ll_data_in,
		rx_ll_sof_in_n		=> rx_ll_sof_in_n,
		rx_ll_eof_in_n		=> rx_ll_eof_in_n,
		rx_ll_src_rdy_in_n 	=> rx_ll_src_rdy_in_n,
		tx_ll_data_out		=> tx_ll_data_out,
		tx_ll_sof_out_n		=> tx_ll_sof_out_n,
		tx_ll_eof_out_n		=> tx_ll_eof_out_n,
		tx_ll_src_rdy_out_n	=> tx_ll_src_rdy_out_n,
		tx_ll_dst_rdy_in_n	=> tx_ll_dst_rdy_in_n,

--	LCD interface
		lcd_e				=> lcd_e,
		lcd_rs				=> lcd_rs,
		lcd_rw				=> lcd_rw,
		lcd_data			=> lcd_data,

--	LEDs
		leds_nwces			=> leds_nwces,
		leds8				=> leds8,

--	user push buttons
		switches_nwces		=> switches_nwces
    );

	rtx_rst <= user_reset;
	switches_nwces(0) <= '0';
	switches_nwces(1) <= '0';
	switches_nwces(3) <= '0';
	
--	switches_nwces(2) <= rst;
--	switches_nwces(4) <= send_frame_flag;

gen_rx_in: process(rtx_clk)
--gen_rx_in: process(clk)
variable count : integer := 0;
variable channel : integer := 0;
variable step : integer := 1;
variable gain : integer := 1;
variable the_depth : integer := 0;
begin
	if rising_edge(rtx_clk) then
--	if rising_edge(clk) then
		rx_ll_data_in <= (others => '0');
		if (rtx_rst = '1') then
--		if (rst = '1') then
			count := 0;
			channel := 0;
			step := 1;
		else
-- default:
			rx_ll_data_in <= x"00";
-- reset on start of each packet
			if (global_count = calistart) then
				count := 0;
				channel := 0;
				step := 1;
			end if;
-- calib header
			if (calistart <= global_count) and (global_count < calistart+42) then
				count := count + 1;
				rx_ll_data_in <= IPheader_calib(count);
			end if;
-- local reset after calib header
			if (global_count = calistart+42) then
				count := 0;
				gain := 1;
				step := 0;
--				channel := 4;
				the_depth := 6;
			end if;
-- calib data: attention: only valid for special testing circumstances!
			if (calistart + 42 <= global_count) and (global_count < calistart+to_integer(ram_coeff_bytes)+42) then
				count := count + 1;
				-- fill a5 downto a1:
				if (0 < the_depth) and (the_depth < 6) then
					if count mod 4 = 3 then
						step := step + 1;
					end if;
					case count mod 4 is
						when 1 =>
							rx_ll_data_in <= x"00";
						when 2 =>
							rx_ll_data_in <= x"00";
						when 3 =>
						-- ch 3 -> CHANNELS - 3 = 1
							rx_ll_data_in(7) <= coeff_init_values((1+the_depth-1) mod 4)(3)(the_DEPTH*A_I_SIZE - step);--gain 3
							rx_ll_data_in(6) <= coeff_init_values((1+the_depth-1) mod 4)(2)(the_DEPTH*A_I_SIZE - step);--gain 2
							rx_ll_data_in(5) <= coeff_init_values((1+the_depth-1) mod 4)(1)(the_DEPTH*A_I_SIZE - step);--gain 1
							rx_ll_data_in(4) <= '0';-- gain 0
						-- ch 2 -> CHANNELS - 2 = 2
							rx_ll_data_in(3) <= coeff_init_values((2+the_depth-1) mod 4)(3)(the_DEPTH*A_I_SIZE - step);--gain 3
							rx_ll_data_in(2) <= coeff_init_values((2+the_depth-1) mod 4)(2)(the_DEPTH*A_I_SIZE - step);--gain 2
							rx_ll_data_in(1) <= coeff_init_values((2+the_depth-1) mod 4)(1)(the_DEPTH*A_I_SIZE - step);--gain 1
							rx_ll_data_in(0) <= '0';-- gain 0
						when 0 =>
						-- ch 1 -> others => '1' = 3
							rx_ll_data_in(7) <= coeff_init_values((3+the_depth-1) mod 4)(3)(the_DEPTH*A_I_SIZE - step);--gain 3
							rx_ll_data_in(6) <= coeff_init_values((3+the_depth-1) mod 4)(2)(the_DEPTH*A_I_SIZE - step);--gain 2
							rx_ll_data_in(5) <= coeff_init_values((3+the_depth-1) mod 4)(1)(the_DEPTH*A_I_SIZE - step);--gain 1
							rx_ll_data_in(4) <= '0';-- gain 0
						-- ch 0 -> others => '0' = 0
							rx_ll_data_in(3) <= coeff_init_values((0+the_depth-1) mod 4)(3)(the_DEPTH*A_I_SIZE - step);--gain 3
							rx_ll_data_in(2) <= coeff_init_values((0+the_depth-1) mod 4)(2)(the_DEPTH*A_I_SIZE - step);--gain 2
							rx_ll_data_in(1) <= coeff_init_values((0+the_depth-1) mod 4)(1)(the_DEPTH*A_I_SIZE - step);--gain 1
							rx_ll_data_in(0) <= '0';-- gain 0
					end case;
				end if;
				if (the_depth = 6) then
					if count mod 4 = 3 then
						step := step + 1;
					end if;
					case count mod 4 is
						when 1 =>
							rx_ll_data_in <= x"00";
						when 2 =>
							rx_ll_data_in <= x"00";
						when 3 =>
						-- ch 3 -> CHANNELS - 3 = 1
							rx_ll_data_in(7) <= coeff_init_values(1)(3)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 3
							rx_ll_data_in(6) <= coeff_init_values(1)(2)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 2
							rx_ll_data_in(5) <= coeff_init_values(1)(1)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 1
							rx_ll_data_in(4) <= '0';-- gain 0
						-- ch 2 -> CHANNELS - 2 = 2
							rx_ll_data_in(3) <= coeff_init_values(2)(3)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 3
							rx_ll_data_in(2) <= coeff_init_values(2)(2)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 2
							rx_ll_data_in(1) <= coeff_init_values(2)(1)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 1
							rx_ll_data_in(0) <= '0';-- gain 0
						when 0 =>
						-- ch 1 -> others => '1' = 3
							rx_ll_data_in(7) <= coeff_init_values(3)(3)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 3
							rx_ll_data_in(6) <= coeff_init_values(3)(2)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 2
							rx_ll_data_in(5) <= coeff_init_values(3)(1)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 1
							rx_ll_data_in(4) <= '0';-- gain 0
						-- ch 0 -> others => '0' = 0
							rx_ll_data_in(3) <= coeff_init_values(0)(3)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 3
							rx_ll_data_in(2) <= coeff_init_values(0)(2)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 2
							rx_ll_data_in(1) <= coeff_init_values(0)(1)(DEPTH*A_I_SIZE+PED_SIZE - step);--gain 1
							rx_ll_data_in(0) <= '0';-- gain 0
					end case;
				end if;
				if (count = 2* PED_SIZE/8 * CHANNELS * GAIN_COUNT) and (the_depth = 6) then
					count := 0;
					the_depth := the_depth - 1;
					step := 0;
				end if;
				if (count = 2* A_I_SIZE/8 * CHANNELS * GAIN_COUNT) then
					count := 0;
					the_depth := the_depth - 1;
					step := 0;
				end if;
			end if;
-- reset on start of each packet
			if (global_count = datainstart) or (global_count = datainstart+42)then
				count := 0;
				channel := 0;
				step := 1;
			end if;
-- data header
			if (datainstart <= global_count) and (global_count < datainstart+42) then
				count := count + 1;
				rx_ll_data_in <= IPheader_data(count);
			end if;
-- data
			if (datainstart+42 <= global_count) and (global_count < datainend+42) then
				count := count + 1;
				rx_ll_data_in <= DATA_init_values(channel)(step)((count mod 2) *8 + 7 downto (count mod 2) * 8);
				if count mod 2 = 0 then
					if channel < 3 then	channel := channel + 1; else channel := 0; end if;
--					if channel > 0 then	channel := channel - 1; else channel := 3; end if;
				end if;
				if count mod 8 = 0 then
					step := step + 1;
				end if;
			end if;
		end if;
	end if;
end process;

-- always ready to send and receive
tx_ll_dst_rdy_in_n <= '0';

-- src_rdy:
with to_integer(global_count) select rx_ll_src_rdy_in_n <=
	'0' when calistart+1 to 42+caliend,
	'0' when datainstart+1 to 42+datainend,
	'1' when others;

-- start receive packet at:
with to_integer(global_count) select rx_ll_sof_in_n <=
	'0' when calistart+1,-- calibration
	'0' when datainstart+1, -- xx: E-data
	'1' when others;

-- end receive packet at:
with to_integer(global_count) select rx_ll_eof_in_n <=
	'0' when 42+caliend,-- 10: calibration
	'0' when 42+datainend,
	'1' when others;

-- global reset on boot for 5 cycles to start clocking
with to_integer(count_100) select clk_reset <=
	'1' when 1 to 5,
	'0' when others;

-- local reset on boot for all other instances
with to_integer(count_100) select switches_nwces(2) <=
	'1' when 27 to 30,
	'0' when others;

-- send out (calculated) packet
with to_integer(global_count) select switches_nwces(4) <=
	'1' when outputstart-4 to outputstart,
	'0' when others;

fast_counting: process(clk_mcfilt)
begin
	if rising_edge(clk_mcfilt) then
		if (user_reset = '1') then
			fast_count <= (others => '0');
		else
			fast_count <= fast_count + 1;
		end if;
	end if;
end process;

slow_counting: process(rtx_clk)
begin
	if rising_edge(rtx_clk) then
		if rtx_rst = '1' then
			global_count <= (others => '0');
		elsif global_count < 5000 then
			global_count <= global_count + 1;
		else
			global_count <= to_unsigned(2,21);
		end if;
	end if;
end process;

-- Clock process definitions
clk_n_count_process: process 
begin
	clk <= '1';
	count_100 <= count_100 + 1;
	wait for 5ns;
	clk <= '0';
	wait for 5ns;
end process;

end Behavioral;

