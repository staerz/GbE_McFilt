----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz
-- Company:			Technical University of Dresden, Germany
-- 					Institute of Physics of Nuclei and Particle Physics
-- 
-- Create Date:		15:59:00 14/04/2010 
--
-- Module Name:		Adder_16bit - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Description:		16 Bit Full Adder with Carry Out
--
-- Dependencies:	Using Adder_4bit
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder_16bit is
	port(
		a_in	: in std_logic_vector (15 downto 0);
		b_in	: in std_logic_vector (15 downto 0);
		c_in	: in std_logic;
		s_out	: out std_logic_vector (15 downto 0);
		c_out	: out std_logic
	);
end FullAdder_16bit;

architecture Behavioral of FullAdder_16bit is

	component Adder_4bit is
		port(
			a_in	: in std_logic_vector (3 downto 0);
			b_in	: in std_logic_vector (3 downto 0);
			c_in	: in std_logic;
			s_out	: out std_logic_vector (3 downto 0);
			c_out	: out std_logic
		);
	end component;

	signal carry : std_logic_vector (3 downto 1);

begin

--	composition of 16-Bit-Adder using 4 4-Bit-FullAdders
	bits_3_downto_0 : Adder_4bit
	port map (
		a_in(3 downto 0), b_in(3 downto 0), c_in, s_out(3 downto 0), carry(1)
	);
	
	bits_7_downto_4 : Adder_4bit
	port map (
		a_in(7 downto 4), b_in(7 downto 4), carry(1), s_out(7 downto 4), carry(2)
	);

	bits_11_downto_8 : Adder_4bit
	port map (
		a_in(11 downto 8), b_in(11 downto 8), carry(2), s_out(11 downto 8), carry(3)
	);

	bits_15_downto_12 : Adder_4bit
	port map (
		a_in(15 downto 12), b_in(15 downto 12), carry(3), s_out(15 downto 12), c_out
	);


end Behavioral;