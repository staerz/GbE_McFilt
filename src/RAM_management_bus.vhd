--------------------------------------------------------------------------------
-- Copyright (c) 2010                                                         --
-- Technische Universit�t Dresden, Dresden, Germany                           --
--                                                                            --
-- Author:			Steffen St�rz, IKTP TU Dresden, Germany                   --
--------------------------------------------------------------------------------
--
-- Create Date:		14:48:41 26/07/2010
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--	- clocked RAM manager controlling a dual port block RAM
--	- r/w requests are dealt via priority list r_flag_bus and w_flag_bus
--	- number of concurrent reading and writing modules flexible via generics
--	
--	- r_flag_bus: most right bit (lsb) has highest priority
--	- w_flag_bus: most right bit (lsb) has highest priority
--
--	- r_fine_bus indicates validity of r_data for each module
--	- w_fine_bus indicates which w_data_bus element has been stored to RAM
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library my_definitions;
use my_definitions.myports.all;

entity RAM_management_bus is
	generic (
-- number of concurrent writing modules to manage
		write_cnt	: positive := w_bus_cnt;
-- number of concurrent reading modules to manage
		read_cnt	: positive := w_bus_cnt
	);
	port (
--	controls
		clk			: in  std_logic;
		rst			: in  std_logic;

--	write ports
		w_flag_bus	: in  std_logic_vector(write_cnt-1 downto 0);
		w_addr_bus	: in		  t_w_addr(write_cnt-1 downto 0);
		w_data_bus	: in		  t_w_data(write_cnt-1 downto 0);
		w_fine_bus	: out std_logic_vector(write_cnt-1 downto 0);

--	read ports
		r_flag_bus	: in  std_logic_vector(read_cnt-1 downto 0);
		r_addr_bus	: in		  t_r_addr(read_cnt-1 downto 0);
		r_fine_bus	: out std_logic_vector(read_cnt-1 downto 0);

--	RAM data output
		r_data		: out std_logic_vector(r_data_width-1 downto 0)
	);
end RAM_management_bus;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of RAM_management_bus is
----- SIMULATION START
--
----	simulation counter
--	signal global_count	: integer range 0 to 1000 := 0;
--
----	controls
--	signal clk			: std_logic;
--	signal rst			: std_logic;
--
----	write ports
--	signal w_flag_bus	:	std_logic_vector(write_cnt-1 downto 0);
--	signal w_addr_bus	:			t_w_addr(write_cnt-1 downto 0);
--	signal w_data_bus	:			t_w_data(write_cnt-1 downto 0);
--	signal w_fine_bus	:	std_logic_vector(write_cnt-1 downto 0);
--
----	read ports
--	signal r_flag_bus	:	std_logic_vector(read_cnt-1 downto 0);
--	signal r_addr_bus	:			t_r_addr(read_cnt-1 downto 0);
--	signal r_fine_bus	:	std_logic_vector(read_cnt-1 downto 0);
--
----	RAM data output
--	signal r_data		:	std_logic_vector(r_data_width-1 downto 0);
----- SIMULATION END

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--      RAM Block managing signals                                            --
--                                                                            --
--------------------------------------------------------------------------------

	constant RAM_reg	: positive := 1;--2 for RAM with additional register, 1 without

--	muxing requests
	signal w_flag_reg	: std_logic_vector(write_cnt-1 downto 0);

	type t_r_flag_reg is array(1 to RAM_reg) of std_logic_vector(read_cnt-1 downto 0);
	signal r_flag_reg	: t_r_flag_reg;

--	direct RAM access:
	-- write:
	signal w_flag		: std_logic := '0';
	signal w_ena_flag	: std_logic_vector(0 downto 0) := "0";
	signal w_addr		: std_logic_vector(w_addr_width-1 downto 0);
	signal w_data		: std_logic_vector(w_data_width-1 downto 0);
	-- read:
	signal r_flag		: std_logic := '0';
	signal r_addr		: std_logic_vector(r_addr_width-1 downto 0);

	component blk_mem_gen_v3_3_no_reg is
------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--        black box dual port RAM module (see Xilinx documentation)           --
--                                                                            --
--        write port: 2**w_addr_width x w_data_width bit                      --
--                                                                            --
--        read port:  2**r_addr_width x r_data_width bit                      --
--                                                                            --
--------------------------------------------------------------------------------
	port (
		clka	:  IN std_logic;
		ena		:  IN std_logic;
		wea		:  IN std_logic_vector(0 downto 0);
		addra	:  IN std_logic_vector(w_addr_width-1 downto 0);
		dina	:  IN std_logic_vector(w_data_width-1 downto 0);
		clkb	:  IN std_logic;
		enb		:  IN std_logic;
		addrb	:  IN std_logic_vector(r_addr_width-1 downto 0);
		doutb	: OUT std_logic_vector(r_data_width-1 downto 0)
	);
	end component blk_mem_gen_v3_3_no_reg;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--         priority list management: lsb equals port to be taken              --
--                                                                            --
--------------------------------------------------------------------------------

-- Least-Significant Set Bit (lssb):
-- Computes a vector of the same length as the argument with
-- at most one bit set at the rightmost '1' found in arg.
--
-- @synthesis supported
-- @author    Thomas B. Preusser
--
	function lssb(arg : std_logic_vector) return std_logic_vector is
	begin
		return  arg and std_logic_vector(unsigned(not arg)+1);
	end;

-- Returns the position of the least-significant set bit assigning
-- the rightmost position an index of zero (0).
--
-- The returned vector is of length 1+log2ceil(arg'length) coding
-- the result position in a two's complement binary. If its additional
-- leftmost bit is set, all elements of the argument vector were
-- zero (0).
--
-- @synthesis supported
-- @author    Thomas B. Preusser
--
-- @editor	Steffen St�rz
--  edit: return value modified to log2ceil(arg'length) only.
--
	function lssb_idx(arg : std_logic_vector) return std_logic_vector is
	variable hot : std_logic_vector(arg'length             downto 0);
	variable res : std_logic_vector(log2ceil(arg'length)-1 downto 0);
	begin
		hot := lssb('1' & arg);
		res := (others => '0');
		for i in 0 to arg'length-1 loop
			if hot(i) = '1' then
				res := res or std_logic_vector(to_unsigned(i, res'length));
			end if;
		end loop;
--		return  hot(arg'length) & res;
		return  res;
	end;

begin

	memory: blk_mem_gen_v3_3_no_reg
	port map (
		clka	=> clk,
		ena		=> w_flag,
		wea		=> w_ena_flag,
		addra	=> w_addr,
		dina	=> w_data,
		clkb	=> clk,
--		rstb	=> rst,
		enb		=> r_flag,
		addrb	=> r_addr,
		doutb	=> r_data
	);

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          RAM write requests are treated here                               --
--                                                                            --
--------------------------------------------------------------------------------

-- set write/write enable flag accordingly
	w_flag		<= '0' when signed(w_flag_bus) = 0 else '1';
	w_ena_flag	<= "0" when signed(w_flag_bus) = 0 else "1";

-- select addr and data to be taken from bus
	w_addr <= w_addr_bus(to_integer(unsigned(lssb_idx(w_flag_bus))));
	w_data <= w_data_bus(to_integer(unsigned(lssb_idx(w_flag_bus))));

--  a registered response is introduced for back checking of modules
	-- mark the choice based on priority list
	w_flag_reg <= lssb(w_flag_bus);

	-- create output
	RAM_write_selection: process(clk)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				w_fine_bus <= (others => '0');
			else
				w_fine_bus <= w_flag_reg;
			end if;
		end if;
	end process;

------------------------------  <-  80 chars  ->  ------------------------------
--                                                                            --
--          RAM read requests are treated here                                --
--                                                                            --
--------------------------------------------------------------------------------

-- set read flag accordingly
	r_flag <= '0' when signed(r_flag_bus) = 0 else '1';

-- select addr to be taken from bus
	r_addr <= r_addr_bus(to_integer(unsigned(lssb_idx(r_flag_bus))));

--  a delay of 1 clock cycle is introduced due to pipelined RAM reading
	-- mark the choice based on priority list
	r_flag_reg(1) <= lssb(r_flag_bus);

	-- create output
	RAM_read_selection: process(clk)
	begin
		if rising_edge(clk) then
			if (rst = '1') then
				r_fine_bus <= (others => '0');
			else
				r_fine_bus <= r_flag_reg(RAM_REG);
			end if;
		end if;
	end process;
	
	-- if further delay is required
	multiple_reg: for i in 1 to RAM_REG-1 generate
	begin
		RAM_read_selection: process(clk)
		begin
			if rising_edge(clk) then
				if (rst = '1') then
					r_flag_reg(i+1) <= (others => '0');
				else
					r_flag_reg(i+1) <= r_flag_reg(i);
				end if;
			end if;
		end process;
	end generate;

----- BEGIN SIMULATION
---- Clock process definitions
--clk_n_count_process : process 
--begin
--	clk <= '1';
--	global_count <= global_count + 1;
--	wait for 4ns;
--	clk <= '0';
--	wait for 4ns;
--end process;
--
--with global_count select rst <=
--	'1' when 1,
--	'0' when others;
--
--with global_count select r_flag_bus(0) <=
--	'1' when 9 | 26 | 27 | 49 | 50 | 72 | 77,
--	'0' when others;
--   
--with global_count select r_addr_bus(0) <=
--	x"12" when 9 | 26 | 27 | 49 | 50 | 72 | 77,
--	x"00" when others;
--   
--with global_count select r_flag_bus(3) <=
--	'1' when 9 | 13 | 20 | 22,
--	'0' when others;
--   
--with global_count select r_addr_bus(3) <=
--	x"f4" when 9 | 13 | 20 | 22,
--	x"00" when others;
--
--with global_count select r_flag_bus(2) <=
--	'1' when 9 | 15 | 25,
--	'0' when others;
--   
--with global_count select r_addr_bus(2) <=
--	x"f1" when 9 | 15 | 25,
--	x"00" when others;
--
--with global_count select r_flag_bus(1) <=
--	'1' when 12 | 16 | 20 | 23,
--	'0' when others;
--   
--with global_count select r_addr_bus(1) <=
--	x"41" when 12 | 16 | 20 | 23,
--	x"00" when others;
--
---- init data: write data to RAM
--with global_count select w_addr_bus(0) <=
--	x"71" when 6,
--	x"f4" when 8,
--	x"41" when 7,
--	x"00" when others;
--
--with global_count select w_data_bus(0) <=
--	x"1234_1234_1234_1234" when 6,
--	x"7890_7890_7890_7890" when 8,
--	x"ffff_ffff_ffff_ffff" when 7,
--	x"0000_0000_0000_0000" when others;
--
--with global_count select w_flag_bus(0) <=
--	'1' when 6 to 8,
--	'0' when others;
--
---- test enet write
--with global_count select w_addr_bus(1) <=
--	x"12" when 2,
--	x"45" when 3,
--	x"23" when 6,
--	x"00" when others;
--
--with global_count select w_data_bus(1) <=
--	x"4567_4567_4567_4567" when 2,
--	x"1234_5678_90ab_cdef" when 3,
--	x"1234_5678_90ab_cdef" when 6,
--	x"0000_0000_0000_0000" when others;
--
--with global_count select w_flag_bus(1) <=
--	'1' when 2 | 3 | 6,
--	'0' when others;
--
----- END SIMULATION

end Behavioral;