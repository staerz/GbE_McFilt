#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "summation.h"

// works temprarily but needs to be put into header-file and nicely c-done
#define MINCHANNELS 8
#define CHANNELS 4
#define GAIN_COUNT 4
#define PED_SIZE 16
#define A_I_SIZE 24
#define DEPTH 5

struct Tudplist
{
	char value;				/* data byte (8bit value) */ 
	struct Tudplist *prev;	/* pointer to previous byte */
	struct Tudplist *next;	/* pointer to next byte */
};

// here in 32 bit signed (there is no 24 bit std type)
int coefficients[CHANNELS][GAIN_COUNT-1][DEPTH+1] = {
	{
//		   pedestal,     a1,         a2,     a3,     a4,     a5
		{0xffffFFFF, 0x0004, 0xffffFFFF, 0x0012, 0x0055, 0x0083},//channel 0, gain 1
		{0xffffFFFE, 0x0003, 0xffffFFFD, 0x00d2, 0x0035, 0x00c3},//channel 0, gain 2
		{0xffffFFFD, 0x0002, 0xffffFFFE, 0x0072, 0x0015, 0x0013} //channel 0, gain 3
	},
	{
		{0x0000, 0x0006, 0xffffFFCF, 0x0042, 0x0025, 0x0021},//channel 1, gain 1
		{0x0001, 0x0005, 0xffffFFAF, 0x0012, 0x0045, 0x0023},//channel 1, gain 2
		{0x0002, 0x0001, 0xffffFFBF, 0x0008, 0x0035, 0x0013} //channel 1, gain 3
	},
	{
		{0x0003, 0x0045, 0xffffFF1F, 0x0012, 0x00c5, 0x002e},//channel 2, gain 1
		{0x0042, 0x0034, 0xffffFF2F, 0x0023, 0x00a5, 0x002d},//channel 2, gain 2
		{0x0004, 0x0023, 0xffffFF3F, 0x0021, 0x00b5, 0x002b} //channel 2, gain 3
	},
	{
		{0x0046, 0x0045, 0xffffFF4F, 0x0066, 0x0012, 0x001a},//channel 3, gain 1
		{0x0076, 0x0024, 0xffffFFC3, 0x0052, 0x0035, 0x001b},//channel 3, gain 2
		{0x0023, 0x0011, 0xffffFF26, 0x0041, 0x0045, 0x001c} //channel 3, gain 3
	}
};

//----------------------------------------------------------------------------\\
//----------------------------------------------------------------------------\\
//	Converts coefficient table to byte data stream to be sent via UDP         \\
//                                                                            \\
//  The serial bit order of the data stream is as follows:                    \\
//                                                                            \\
//   MSB,   addr Max-1             (input first)                              \\
//   MSB,   addr Max-2                                                        \\
//     ...                                                                    \\
//   MSB,   addr 1                                                            \\
//   MSB,   addr 0                                                            \\
//                                                                            \\
//   MSB-1, addr Max-1                                                        \\
//   MSB-1, addr Max-2                                                        \\
//     ...                                                                    \\
//   MSB-1, addr 1                                                            \\
//   MSB-1, addr 0                                                            \\
//                                                                            \\
//     ...                                                                    \\
//   LSB,   addr Max-1                                                        \\
//   LSB,   addr Max-2                                                        \\
//     ...                                                                    \\
//   LSB,   addr 1                                                            \\
//   LSB,   addr 0                  (input last)                              \\
//                                                                            \\
//  where addr Max = 2^log2ceil(channel) but at least 32 (= 5 bit)            \\
//                                                                            \\
//  addr is made up of 2 parts: addr = channel-part & gain                    \\
//    upper bits to encrypt channel = {0, 1, ... , CHANNELS-1}                \\
//    lower 2 bits to encrypt gain = {0, 1, 2, 3}                             \\
//                                                                            \\
//----------------------------------------------------------------------------\\
//                                                                            \\
//  The conversion of channel <-> addr (channel-part) is as follows:          \\
//                                                                            \\
//     channel 0                      -> addr 0;                              \\
//     channel 1                      -> addr 2^log2ceil(CHANNELS)-1;         \\
//     channel others                 -> CHANNELS-channel;                    \\
//                                                                            \\
//  explicitely:                                                              \\
//                                                                            \\
//  for 1 up to 4 channels (0, 1, 2, 3):                                      \\
//    address :         76543210                                              \\
//    channel :         -------0 for 1 channel                                \\ 
//    channel :         ----1--0 for 2 channels                               \\
//    channel :         ----1-20 for 3 channels                               \\
//    channel :         ----1230 for 4 channels                               \\
//  for 5 up to 8 channels (0, 1, 2, 3):                                      \\
//    address :         76543210                                              \\
//    channel :         1---2340 for 5 channels                               \\
//    channel :         1--23450 for 6 channels                               \\
//    channel :         1-234560 for 7 channels                               \\
//    channel :         12345670 for 8 channels                               \\
//  for 9 up to 16 channels (0, 1, 2, 3):                                     \\
//    address : fedcba9876543210                                              \\
//    channel : 1-------23456780 for 9 channels                               \\
//    channel : 1-----23456789a0 for 11 channels                              \\
//    channel : 1----23456789ab0 for 12 channels                              \\
//    channel : 1---23456789abc0 for 13 channels                              \\
//    channel : 1--23456789abcd0 for 14 channels                              \\
//    channel : 1-23456789abcde0 for 15 channels                              \\
//    channel : 123456789abcdef0 for 16 channels                              \\
//                                                                            \\
//----------------------------------------------------------------------------\\
//                                                                            \\
//  additionally the channels have to be shifted for each cefficient:         \\
//     channel -> (channel+(DEPTH-the_depth))%CHANNELS                        \\
//                                                                            \\
//  exemplarily given for 4 channels:                                         \\
//  a1: shifted 4x    (the_depth==1)                                          \\
//    address :         76543210                                              \\
//    channel :         ----1230                                              \\
//  a2: shifted 3x    (the_depth==2)                                          \\
//    address :         76543210                                              \\
//    channel :         ----0123                                              \\
//  a3: shifted 2x    (the_depth==3)                                          \\
//    address :         76543210                                              \\
//    channel :         ----3012                                              \\
//  a4: shifted 1x    (the_depth==4)                                          \\
//    address :         76543210                                              \\
//    channel :         ----2301                                              \\
//	a5: unshifted     (the_depth==5)                                          \\
//    address :         76543210                                              \\
//    channel :         ----1230                                              \\
//  pedestal: unshifted                                                       \\
//    address :         76543210                                              \\
//    channel :         ----1230                                              \\
//                                                                            \\
//  exemplarily given for 6 channels:                                         \\
//  a1: shifted 4x                                                            \\
//    address :         76543210                                              \\
//    channel :         5--01234                                              \\
//  a2: shifted 3x                                                            \\
//    address :         76543210                                              \\
//    channel :         4--50123                                              \\
//  a3: shifted 2x                                                            \\
//    address :         76543210                                              \\
//    channel :         3--45012                                              \\
//  a4: shifted 1x                                                            \\
//    address :         76543210                                              \\
//    channel :         2--34501                                              \\
//	a5: unshifted                                                             \\
//    address :         76543210                                              \\
//    channel :         1--23450                                              \\
//  pedestal: unshifted                                                       \\
//    address :         76543210                                              \\
//    channel :         1--23450                                              \\
//----------------------------------------------------------------------------\\
//----------------------------------------------------------------------------\\

struct Tudplist* root;

//	calculates minimal number of bits required to encrypt arg
short log2ceil(int arg){
	int tmp, log;
	for(tmp = 1,log = 0; arg > tmp; tmp *= 2, log += 1);
	return log;
}

//	implementation of 2^exp via bit shift
int powerof2(short exp){ return(1<<exp); }

// create output stream
struct Tudplist* textoutput(struct Tudplist *root){
	struct Tudplist *temp;
	short tmp;
	short cnt;
	
//	loop through list
	for(temp=root, cnt = 0;
		temp;
		temp=temp->next, cnt++)
	{
//	use tmp to get rid of leading ff's for "negative" interpreted values
		tmp = temp->value & 0xff;
//	formatting of text output
		if (cnt%2 == 0) printf(" ");
		if (cnt%16 == 0) printf("\n");
		printf("%02x ",tmp);
	}
	printf("\n");
	return(root);	   
}

//	create file output
struct Tudplist* fileoutput(struct Tudplist *root, char* argv[])
{
	FILE *fptxt, *fphex;
	struct Tudplist *temp;
	short cnt;
	short tmp;
	
	fptxt = fopen(argv[1],"w+b");
	fphex = fopen(argv[2],"w+b");

//	loop through list
	for(temp=root, cnt = 0;
		temp;
		temp=temp->next, cnt++)
	{
//	use tmp to get rid of leading ff's for "negative" interpreted values
		tmp = temp->value & 0xff;
//	format hex data in text file with 2 positions and leading 0
		fprintf(fptxt,"%02x ",tmp);
		fprintf(fphex,"%c",temp->value);
		if (cnt%2 == 1) fprintf(fptxt," ");
	}
	fclose(fptxt);
	fclose(fphex);

	return(root);	   
}

//	calculate channel from address
short grab_channel(short addr){
	if (addr == 0) return (0);
	if (addr == powerof2(log2ceil(CHANNELS))-1) return(1);
	if (CHANNELS-addr > 0) return(CHANNELS-addr);
	return(CHANNELS); //if address is out of range return invalid channel
}

//	calculate address from channel: inverse grab_channel (actually not needed)
short grab_addr(short channel){
	switch(channel){
		case 0: return(0);
		case 1: return(powerof2(log2ceil(CHANNELS))-1);//all bits 1
		default: return(CHANNELS-channel);
	}
}

//	maximum of 2 values
short max(short a, short b){
	if (a > b) return(a);
	return(b);
}

//	bitwise construction of data stream: byte by byte
struct Tudplist* init(void){
	struct Tudplist *root, *temp;
	short byte, bit;
	short addr, channel, gain, the_depth;
	char value;
	
//	loop over all coefficient depths (start with pedestals <-> the_depth == 0)
	for(the_depth = 0, value = 0,
		root=(struct Tudplist*)malloc(sizeof(struct Tudplist)),
		root->prev=NULL, temp=root;
		the_depth < 6;
		byte = 0, the_depth++)
	{
//		printf("\nthe_depth = %d\n",the_depth);
//		distinguis bit width of peds and coefficients
		for(bit = (the_depth == 0)? PED_SIZE-1 : A_I_SIZE-1;
			bit >= 0;
			bit--)
		{	
//			loop over all addresses to be set
			for(addr = powerof2(log2ceil(max(CHANNELS, MINCHANNELS)))-1;// 
				addr >= 0;
				addr --)
			{
//				select corresponding channel for address
				channel = grab_channel(addr);
//				set value only for valid channels
				if (channel < CHANNELS){
					if (the_depth == 0)	// pedestals
//						respect all 4 gain options
						for(gain = 3; gain > 0; gain--)
//	coefficients[channel][gain-1][0]: selects coefficient/pedestal
//  >> bit: shifts required bit to most right positition
//  & 1: masks most right bit
//  << gain: shifts bit back to its position according to address part for gain
//  + (addr%2)*4: shifts upper byte part (1st 4bits) to its position
							value += ((coefficients[channel][gain-1][0]>>bit) 
								& 1)<<(gain + (addr%2)*4);
					else				// coefficients
						for(gain = 3; gain > 0; gain--)
							value += ((coefficients
								[(channel+(5-the_depth))%CHANNELS]
								[gain-1][the_depth]
								>>bit) & 1)<<(gain + ( addr%2)*4);
				}
//				2 times 4 bits gives full byte: create new byte as list entry
				if (addr%2 == 0){
					temp->next=(struct Tudplist*)malloc(sizeof(struct Tudplist));
					temp->next->prev=temp;
					temp->value = value;
					temp=temp->next;
//					reset value for next run
					value = 0;
				}
			}
		}
	}
	temp=temp->prev;
	free(temp->next);
	temp->next=NULL;
	return(root);	   
}

int main(int argc, char* argv[])
{
	printf("Usage: calibration.exe <filename.txt> <filename.hex> [options]\n\noptions: -l >> list data\n");
//	require 2 filenames to store data to
  	if(argc<2){
		printf("\nAbort due to too few parameters! Two filenames required!");
		exit(0);  	
	}
//	do bit arrangement
	root = init();
//	list output if specified by user by option -l
	if( argc==4 && (!(strcmp(argv[3],"-l"))))
		root = textoutput(root);
//	save output to files
	root = fileoutput(root, argv);
	printf("\nOperation successful, two files created:\n  - data in hex: %s,\n  - data in txt: %s",argv[1],argv[2]);
	printf("\n\nPress any key to terminate programme.\n");
//	wait for the any key
	getch();
	return(0);
}
