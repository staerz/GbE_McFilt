----------------------------------------------------------------------------------
-- Engineer:		Steffen St�rz, IKTP TU Dresden, Germany
-- 
-- Create Date:		15:31:48 08/27/2010 
--
-- Module Name:		McFilt_manager - Behavioral 
--
-- Target Devices:	virtex-5 (board ml507)
-- Tool versions:	ISE 11.2
--
-- Dependencies:	None
--
-- Revision 0.01 - File Created
--
-- Comments:		Using 4 spaces for tab
--
-- Description:
--		On calc_start = '1' the manager will initialise the calculation process
--		by filling in a 64-to-16 bit cross clock fifo with RAM data. The 16 bit
--		fifo output will then be given to McFilt. It's result is thus stored in
--		a 16-to-64 bit cross clock fifo with final storage to 64bit RAM.
--		Several registers are introduced between the fifos and McFilt to achieve
--		a maximum clock frequency and thus performance.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity McFilt_manager is
	generic (
--	number of E samples to be calculated (multiple of 4)
		E_samples		: positive;
		LOWESTBIT		: positive
	);
	port (
--	clocking
		clk_125MHz		: in  std_logic;
		clk_mcfilt		: in  std_logic;
		rst				: in  std_logic;

--	communication McFilt-manager -> McFilt
		mcfilt_enable	: out std_logic;
		mcfilt_reset	: out std_logic;

--	RAM read requests of McFilt manager:
		r_flag_mcfilt	: out std_logic;
		r_addr_mcfilt	: out std_logic_vector(7 downto 0);
		r_fine_mcfilt	: in  std_logic;
		r_data			: in  std_logic_vector(63 downto 0);

--	RAM write requests of McFilt manager:
		w_flag_mcfilt	: out std_logic;
		w_addr_mcfilt	: out std_logic_vector(7 downto 0);
		w_data_mcfilt	: out std_logic_vector(63 downto 0);
		w_fine_mcfilt	: in  std_logic;

--	external stimuli
		send_frame_flag	: out std_logic;

--	communication McFilt manager <-> UDP module
		calc_start		: in  std_logic;
		calc_enable		: out std_logic;

--	Energy to be put into McFilt
		E				: out std_logic_vector(15 downto 0);
--	Result from McFilt
		S				: in  std_logic_vector(47 downto LOWESTBIT);
		inval			: in  std_logic;
		c_valid			: in  std_logic
	);
end McFilt_manager;

library my_definitions;
use my_definitions.mytypes.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library IEEE;
use IEEE.NUMERIC_STD.ALL;

architecture Behavioral of McFilt_manager is
--	state machine (so far there is no difference between "none" and "done")
	type t_state is (none, reset, count, delay, ready, done);
	signal state 			: t_state := none;
	signal mcfilt_reset_i	: std_logic;
	
--	FIFO INSIDE
	signal fifo_in_done		: std_logic;
	-- fifo state
	signal fifo_in_valid	: std_logic;
	signal fifo_in_afull	: std_logic;

	component fifo_mcfilt_inside
	port (
		rst			: IN  std_logic;
		wr_clk		: IN  std_logic;
		rd_clk		: IN  std_logic;
		din			: IN  std_logic_VECTOR(63 downto 0);
		wr_en		: IN  std_logic;
		rd_en		: IN  std_logic;
		dout		: OUT std_logic_VECTOR(15 downto 0);
		full		: OUT std_logic;
		almost_full	: OUT std_logic;
		empty		: OUT std_logic;
		valid		: OUT std_logic
	);
	end component;

--	ranging

--	Result from calculation
	signal S_out		: std_logic_vector(15 downto 0);
	signal range_valid	: std_logic := '0';

	component ranging_slow is
	port (
--	controls
		clk 	: in  std_logic;
		rst 	: in  std_logic;
		en  	: in  std_logic;

--	unranged input
		Input	: in  std_logic_vector(47 downto LOWESTBIT);
--	invalidity flag of input
		inval	: in  std_logic;
--	ranged output
		Output	: out std_logic_vector(15 downto 0)
	);
	end component;

--	FIFO OUTSIDE
	signal fifo_out_done	: std_logic;
	-- fifo state
	signal fifo_out_valid	: std_logic;

	component fifo_mcfilt_outside
	port (
		rst		: IN  std_logic;
		wr_clk	: IN  std_logic;
		rd_clk	: IN  std_logic;
		din		: IN  std_logic_VECTOR(15 downto 0);
		wr_en	: IN  std_logic;
		rd_en	: IN  std_logic;
		dout	: OUT std_logic_VECTOR(63 downto 0);
		full	: OUT std_logic;
		empty	: OUT std_logic;
		valid	: OUT std_logic
	);
	end component;

begin
--	error protection: require multiple of 4 for E_samples due to fifo ratio
	assert E_samples mod 4 = 0
	report "E_samples must be a multiple of 4 due to fifo ratio!"
	severity failure;

--	ranging module: insert a register for high clock frequency and short paths
	delay_ranging: block
	signal S_nxt	: std_logic_vector(47 downto LOWESTBIT);
	signal i_nxt	: std_logic;
	begin
		process (clk_mcfilt)
		begin
			if rising_edge(clk_mcfilt) then
				if mcfilt_reset_i = '1' then
					range_valid <= '0';
				else
					range_valid <= c_valid;
				end if;
				S_nxt <= S;
				i_nxt <= inval;
			end if;
		end process;
		
		makeranging: ranging_slow
		port map (
	--	controls
			clk		=> clk_mcfilt,
			rst		=> mcfilt_reset_i,
			en 		=> '1',

	--	unranged input
			Input	=> S_nxt,
	--	invalidity flag of input
			inval	=> i_nxt,
	--	ranged output
			Output	=> S_out
		);
	end block;

	state_machine: process(clk_125MHz) is
	begin
		if rising_edge(clk_125MHz) then
			if (rst = '1') then
				state <= none;
			else
				case state is
					when none =>
						if calc_start = '1' then
							-- initialise
							state <= reset;
						else
							-- standby
							state <= none;
						end if;
					when reset =>
						-- fill fifo_in
						state <= count;
					when count =>
						if fifo_in_done = '1' then
							-- wait for fifo_out empty
							state <= delay;
						else
							-- keep filling fifo_in
							state <= count;
						end if;
					when delay =>
						if fifo_out_done = '1' then
							-- indicate readyness
							state <= ready;
						else
							-- keep waiting for fifo out
							state <= delay;
						end if;
					when ready =>
						-- standy
						state <= done;
					when done =>
						if calc_start = '1' then
							-- initialise
							state <= reset;
						else
							-- standby
							state <= done;
						end if;
				end case;
			end if;
		end if;
	end process;

--	indicate active module to outer world
	with state select calc_enable <=
		'0' when none | done,
		'1' when others;

--	indicate finished activity to outer world
	with state select send_frame_flag <=
		'1' when ready,
		'0' when others;
	
	fifo_in_counting: block
	-- sample counting
	signal fifo_in_count	: signed(log2ceil(E_samples) downto 0);
	begin
		process(clk_125MHz) is
		begin
			if rising_edge(clk_125MHz) then
				case state is
					-- initialise counter
					when reset	=>	fifo_in_count <= to_signed(E_samples - 2, fifo_in_count'length);
					when count	=>
						if fifo_in_afull = '0' then
							-- go on counting when fifo not yet (almost) full
							fifo_in_count <= fifo_in_count - to_signed(1, fifo_in_count'length);
						else
							-- hold counting on (almost) full fifo
							fifo_in_count <= fifo_in_count;
						end if;
					-- don't care otherwise
					when others	=> fifo_in_count <= (others => '-');
				end case;
			end if;
		end process;

	--	sign flip indicates done
		fifo_in_done <= fifo_in_count(fifo_in_count'left);
	end block;

--	same for outside fifo
	fifo_out_counting: block
	-- sample counting
	signal fifo_out_count	: signed(log2ceil(E_samples) downto 0);
	begin
		process(clk_125MHz) is
		begin
			if rising_edge(clk_125MHz) then
				case state is
					when reset => fifo_out_count <= to_signed(E_samples - 2, fifo_out_count'length);
					when count | delay => -- here it's 2 states!
						if fifo_out_valid = '1' then
							fifo_out_count <= fifo_out_count - to_signed(1, fifo_out_count'length);
						else
							fifo_out_count <= fifo_out_count;
						end if;
					when others => fifo_out_count <= (others => '0');
				end case;
			end if;
		end process;

		fifo_out_done <= fifo_out_count(fifo_out_count'left);
	end block;

--	set r_addr and read flag accordingly to state
	fifo_in_fill: block
	signal r_addr	: unsigned(7 downto 0);
	begin
		process(clk_125MHz)
		begin
			if rising_edge(clk_125MHz) then
				case state is
					when reset =>
						-- initial values
						r_addr <= ram_data_start - 1;
						r_flag_mcfilt <= '0';
					when count =>
						if fifo_in_afull = '0' then
							-- while not (almost) full: keep reading
							r_flag_mcfilt <= '1';
							r_addr <= r_addr + 1;
						else
							-- (almost) full: hold
							r_flag_mcfilt <= '0';
							r_addr <= r_addr;
						end if;
					when others =>
						-- don't read if not specified
						r_addr <= (others => '-');
						r_flag_mcfilt <= '0';
				end case;
			end if;
		end process;
		
		r_addr_mcfilt <= std_logic_vector(r_addr);
	end block;

--	set w_addr accordingly to state
	fifo_out_write: block
	signal w_addr			: unsigned(7 downto 0);
	begin
		process(clk_125MHz)
		begin
			if rising_edge(clk_125MHz) then
				case state is
					when reset =>
						-- initial value
						w_addr <= ram_data_start;
					when count | delay =>
						if fifo_out_valid = '1' then
							-- while output valid: keep writing
							w_addr <= w_addr + 1;
						end if;
					when others =>
						-- don't care for others
						w_addr <= (others => '-');
				end case;
			end if;
		end process;
		
		w_addr_mcfilt <= std_logic_vector(w_addr);
	end block;

--	handle clock domain switch of reset: use 2 sequential registers
	manage_reset: block
	signal do_mcfilt_reset		: std_logic; -- clock domain of state machine
	signal sync_mcfilt_reset	: std_logic; -- clock domain of mcfilt
	begin
		process(clk_125MHz)
		begin
			if rising_edge(clk_125MHz) then
				case state is
					when reset => do_mcfilt_reset <= '1';
					when others => do_mcfilt_reset <= '0';
				end case;
			end if;
		end process;

		process (clk_mcfilt)
		begin
			if rising_edge(clk_mcfilt)  then
				sync_mcfilt_reset <= do_mcfilt_reset;
				mcfilt_reset_i <= sync_mcfilt_reset;
			end if;
		end process;
	end block;

	mcfilt_reset <= mcfilt_reset_i;

--	fifo for McFilt input: use supplementary register for optimal clk speed
	fifo_in_manage: block
	type t_E_reg is array(1 to E_delay) of std_logic_vector(E_BIT downto 0);
	type t_g_reg is array(1 to E_delay) of std_logic_vector(GAIN_BIT downto GAIN_BUT);
	signal E_reg		: t_E_reg;
	signal g_reg		: t_g_reg;
	signal E_valid_reg	: std_logic_vector(1 to E_delay) := (others => '0');
	signal E_out		: std_logic_vector(15 downto 0);
	begin
		-- get E_out out of fifo on fifo_in_valid
		manage_fifo_inside: fifo_mcfilt_inside
		port map (
			rst			=> rst,
			wr_clk		=> clk_125MHz,
			rd_clk		=> clk_mcfilt,
			din			=> r_data,
			wr_en		=> r_fine_mcfilt,
			rd_en		=> fifo_in_valid,
			dout		=> E_out,
			full		=> open,
			almost_full	=> fifo_in_afull,
			empty		=> open,
			valid		=> fifo_in_valid
		);
		
		-- store E_out (and fifo_in_valid) in register
		single_E_reg: if E_delay = 1 generate
		begin
			process (clk_mcfilt)
			begin
				if rising_edge(clk_mcfilt) then
					if mcfilt_reset_i = '1' then
						E_valid_reg <= (others => '0');
					else
						E_valid_reg(1) <= fifo_in_valid;
					end if;
					E_reg(1)<= E_out(E_BIT downto 0);
					g_reg(1)<= E_out(GAIN_BIT downto GAIN_BUT);
				end if;
			end process;
		end generate;

		multiple_e_reg: if E_delay > 1 generate
		begin
			process (clk_mcfilt)
			begin
				if rising_edge(clk_mcfilt) then
					if mcfilt_reset_i = '1' then
						E_valid_reg <= (others => '0');
					else
						E_valid_reg <= fifo_in_valid & E_valid_reg(E_reg'left to E_reg'right-1);
					end if;
					E_reg	<= E_out(E_BIT downto 0) & E_reg(E_reg'left to E_reg'right-1);
					g_reg	<= E_out(GAIN_BIT downto GAIN_BUT) & g_reg(E_reg'left to E_reg'right-1);
				end if;
			end process;
		end generate;

		-- assign register to E input of McFilt
		process (clk_mcfilt)
		begin
			if rising_edge(clk_mcfilt) then
				E <= (others => '0');
				E(E_BIT downto 0)			<= E_reg(E_reg'right);
				E(GAIN_BIT downto GAIN_BUT)	<= g_reg(E_reg'right);
				mcfilt_enable <= E_valid_reg(E_reg'right);
			end if;
		end process;
	end block;
	
--	fifo for McFilt output: use supplementary register for optimal clk speed
	manage_fifo_out : block
	type t_S_reg is array(1 to S_delay) of std_logic_vector(15 downto 0);
	signal S_reg		: t_S_reg;
	signal S_valid_reg	: std_logic_vector(1 to S_delay) := (others => '0');
	begin
		-- store S_out (and range_valid) in register

		singe_S_reg: if S_delay = 1 generate
		begin
			process (clk_mcfilt)
			begin
				if rising_edge(clk_mcfilt) then
					if mcfilt_reset_i = '1' then
						S_valid_reg <= (others => '0');
					else
						S_valid_reg(1) <= range_valid;
					end if;
					S_reg(1) <= S_out;
				end if;
			end process;
		end generate;
		
		multiple_S_reg: if S_delay > 1 generate
		begin
			process (clk_mcfilt)
			begin
				if rising_edge(clk_mcfilt) then
					if mcfilt_reset_i = '1' then
						S_valid_reg <= (others => '0');
					else
						S_valid_reg <= range_valid & S_valid_reg(S_reg'left to S_reg'right-1);
					end if;
					S_reg <= S_out & S_reg(S_reg'left to S_reg'right-1);
				end if;
			end process;
		end generate;

		-- put S_reg into fifo on S_reg_valid
		manage_fifo_outside : fifo_mcfilt_outside
		port map (
			rst		=> rst,
			wr_clk	=> clk_mcfilt,
			rd_clk	=> clk_125MHz,
			din		=> S_reg(S_reg'right),
			wr_en	=> S_valid_reg(S_reg'right),
			rd_en	=> fifo_out_valid,
			dout	=> w_data_mcfilt,
			full	=> open,
			empty	=> open,
			valid	=> fifo_out_valid
		);

	end block;
	w_flag_mcfilt <= fifo_out_valid;

end Behavioral;

