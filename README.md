# Overview
This repository contains the VHDL source code of my Diploma Thesis project alias **McFilt**.

It is a DSP implementation with RAM and 1 GbE connectivity and was designed on a Xinlinx Virtex-5 ML507 development kit.

[This is an overview block diagram.](./GbE_Layout_2.pdf)

# Disclaimer
No guarantee it works out of the box, it is a 10 year old project.

This repo is a restoration (from a backup) of the source code for my Diploma Thesis, some source files might be missing (e.g Xinlinx IP cores).

# Documentation

See [my diploma thesis CERN-THESIS-2010-174](https://cds.cern.ch/record/1316494?ln=en).

The top level is [ethernet_data_module.vhd](./src/ethernet_data_module.vhd).

See [the `documentation` directory](./documentation) for additional files.
